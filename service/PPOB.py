__author__ = 'wahyudi@popbox.asia'

import requests
import json
import logging
from pprint import pprint
import sys
import ClientTools
import Configurator
import os
from PyQt5.QtCore import QObject, pyqtSignal
from box.service import BoxService
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class PPOBSignalHandler(QObject):
    __qualname__ = 'PPOBSignalHandler'
    start_get_sim_product_signal = pyqtSignal(str)
    start_check_addon_signal = pyqtSignal(str)
    start_book_transaction_signal = pyqtSignal(str)
    start_confirm_transaction_signal = pyqtSignal(str)
    start_callback_signal = pyqtSignal(str)
    start_create_transaction_signal = pyqtSignal(str)
    start_add_payment_signal = pyqtSignal(str)
    start_get_payment_signal = pyqtSignal(str)
    start_get_transaction_signal = pyqtSignal(str)
    start_flag_emoney_signal = pyqtSignal(str)
    start_get_sepulsa_signal = pyqtSignal(str)
    start_get_ppob_button_signal = pyqtSignal(str)
    start_check_member_signal = pyqtSignal(str)

_PPOB_ = PPOBSignalHandler()

_LOG_ = logging.getLogger()
img_path = sys.path[0] + '/video_capture/'
NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet'}
SSL_VERIFY = False
PPOB_TOKEN = '5jLoDSzRiNWMajzMsHIAsc66hl8XsKbR3DvfD8DqW8lEE9KagK80l5E3XN8igoixGRgbfmiE2J3OVRXbrzfCLJXLpDBNJpXyc1XJAHs0EAvhyBKnaARAyeEPXuwnvmlu'
PPOB_URL = 'http://servicedev.popbox.asia/'
PPOB_URL_PAYMENT = 'https://payment.popbox.asia/'
PPOB_TOKEN_PAYMENT = 'PHVVH4KXB9QJUK6NZ6EKGQNFQOHLVGVFX87RUYBV'
# PPOB_URL_PAYMENT = "http://paymentdev.popbox.asia/"
# PPOB_TOKEN_PAYMENT = "ZNZNEONSYG6SUAFEPYNACR1H2AMJ29DS7D3630AH"

BOX = BoxService.get_box()
GLOBAL_URL = Configurator.get_value('ClientInfo', 'serveraddress')
READER_TID = Configurator.get_value('popbox', 'terminalid')
ORDER_NO = Configurator.get_value('ClientInfo', 'OrderNo')
HEADERS = {
    'content-type': "application/json",
    'orderNo': ORDER_NO
}

##################################################################


def global_request(url_param, param=None, headers=HEADERS):
    global SSL_VERIFY
    r_json = NOT_INTERNET_
    r_status = -1
    r = None
    try:
        if 'https' in url_param:
            if param is None:
                r = requests.get(url_param, json=None, timeout=60, headers=headers, verify=SSL_VERIFY)
            else:
                r = requests.post(url_param, json=param, timeout=60, headers=headers, verify=SSL_VERIFY)
        else:
            if param is None:
                r = requests.get(url_param, json=None, timeout=60, headers=headers)
            else:
                r = requests.post(url_param, json=param, timeout=60, headers=headers)
    except requests.RequestException as e:
        _LOG_.warning((url_param, e, r_status))

    try:
        if r is not None:
            r_json = r.json()
            r_status = r.status_code
        else:
            _LOG_.warning((url_param, r_json, r_status))
    except ValueError as e:
        _LOG_.warning((url_param, e, r_status))

    _LOG_.info('<URL>:' + str(url_param) + ' || <POST>: ' + str(param) + ' || <RESPONSE>: ' + str(r_json) +
                ' || <STATUS>: ' + str(r_status))

    print('[live-debugger] <URL>: ' + str(url_param) + ' || <POST>: ' + str(param) + ' || <RESPONSE>: ' + str(r_json) + ' || <STATUS>: '
          + str(r_status))

    return r_json, r_status

##################################################################


SIM_PRODUCTS = []


def start_get_sim_product(operator, t_product):
    ClientTools.get_global_pool().apply_async(get_sim_product, (operator, t_product,))


def get_sim_product(operator, t_product):
    global SIM_PRODUCTS
    try:
        SIM_PRODUCTS = []
        post_param = {
            'token': PPOB_TOKEN,
            'operator': operator,
            'type': t_product
        }
        query_url = PPOB_URL + 'api/starterPack/getList'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            # m_data = []
            # m_data.extend((response['data']) * 10)
            response_param = {
                'data': response['data'],
                # 'data': m_data,
                'operator': operator,
                'type': t_product,
                'total': len(response['data'])
            }
            SIM_PRODUCTS = response_param
            _PPOB_.start_get_sim_product_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_get_sim_product_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_get_sim_product_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_sim_product : ', str(e)))


def start_check_addon(pid):
    ClientTools.get_global_pool().apply_async(check_addon, (pid,))


def check_addon(pid):
    try:
        post_param = {
            'token': PPOB_TOKEN,
            'product_id': pid
        }
        query_url = PPOB_URL + 'api/starterPack/getAvailableAddOn'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'product_id': pid,
                'total': len(response['data'])
            }
            _PPOB_.start_check_addon_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_check_addon_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_check_addon_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] check_addon : ', str(e)))


def start_book_transaction(package):
    ClientTools.get_global_pool().apply_async(book_transaction, (package,))


def book_transaction(package):
    try:
        pack = json.loads(package)
        post_param = {
            'token': PPOB_TOKEN,
            'product_id': pack['product_id'],
            'addon_id': pack['addon_id'],
            'location_type': 'locker',
            'location_id': BOX['id'],
            'location_name': BOX['name'],
            'customer_phone': pack['customer_phone'],
            'customer_email': pack['customer_email'],
            'customer_name': pack['customer_name']
        }
        query_url = PPOB_URL + 'api/starterPack/bookTransaction'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'product_id': pack['product_id'],
                'addon_id': pack['addon_id'],
                'total': len(response['data'])
            }
            _PPOB_.start_book_transaction_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_book_transaction_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_book_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] book_transaction : ', str(e)))


def start_confirm_transaction(invoice_id):
    ClientTools.get_global_pool().apply_async(confirm_transaction, (invoice_id,))


def confirm_transaction(invoice_id):
    try:
        post_param = {
            'token': PPOB_TOKEN,
            'invoice_id': invoice_id
        }
        query_url = PPOB_URL + 'api/starterPack/confirmTransaction'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'invoice_id': invoice_id,
                'total': len(response['data'])
            }
            _PPOB_.start_confirm_transaction_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_confirm_transaction_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_confirm_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] confirm_transaction : ', str(e)))


def start_callback_transaction(package):
    ClientTools.get_global_pool().apply_async(callback_transaction, (package,))


def callback_transaction(package):
    try:
        pack = json.loads(package)
        post_param = {
            'token': PPOB_TOKEN,
            'transaction_id': pack['transaction_id'],
            'payment_id': pack['payment_id'],
            'paid_amount': pack['paid_amount'],
            'status': 'PAID',
            'payment_channel_code': pack['payment_method']
        }
        query_url = PPOB_URL + 'api/locker/payment/callbackFixed'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['response']['data']) > 0:
            response_param = {
                'data': response['response']['data'],
                'transaction_id': pack['transaction_id'],
                'payment_id': pack['payment_id'],
                'paid_amount': pack['paid_amount'],
                'status': 'PAID',
                'total': len(response['response']['data'])
            }
            _PPOB_.start_callback_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_callback_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_callback_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] callback_transaction : ', str(e)))


def start_create_transaction(type_of, package):
    ClientTools.get_global_pool().apply_async(create_transaction, (type_of, package,))


def create_transaction(type_of, package):
    # type_of = mobile/starter-pack
    try:
        pack = json.loads(package)
        post_param = {
            'token': PPOB_TOKEN,
            'phone': pack['phone'],
            'type': type_of,
            'locker_id': BOX['id'],
            'locker_name': BOX['name'],
            'customer_phone': pack['customer_phone'],
            'customer_email': pack['customer_email'],
            'customer_name': pack['customer_name']
        }
        if type_of == 'mobile':
            post_param['product_id'] = pack['product_id'][0]
        if type_of == 'starter-pack':
            post_param['product_id'] = pack['product_id']
            post_param['addon_id'] = pack['addon_id']
        query_url = PPOB_URL + 'api/locker/transaction/create'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'product_id': post_param['product_id'],
                'type': type_of,
                'total': len(response['data'])
            }
            _PPOB_.start_create_transaction_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_create_transaction_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_create_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] create_transaction : ', str(e)))


def start_add_payment(package):
    ClientTools.get_global_pool().apply_async(add_payment, (package,))


def add_payment(package):
    try:
        pack = json.loads(package)
        post_param = {
            'token': PPOB_TOKEN,
            'country': 'ID',
            'transaction_reference': pack['transaction'],
            'payment_method': pack['payment']
        }
        query_url = PPOB_URL + 'api/locker/transaction/addPaymentMethod'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'country': 'ID',
                'transaction_reference': pack['transaction'],
                'payment_method': pack['payment'],
                'total': len(response['data'])
            }
            _PPOB_.start_add_payment_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_add_payment_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_add_payment_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] add_payment : ', str(e)))

def start_get_payment_offline():
    ClientTools.get_global_pool().apply_async(get_payment_offline)

def get_payment_offline():
    try:
        session_id = get_payment_session()
        post_param = {
            'token': PPOB_TOKEN_PAYMENT,
            'country': 'ID',
            'session_id': session_id
        }
        _LOG_.debug(('payment_param_available : ', post_param))

        if session_id is None: 
            param_offline = [{
                        'type': 'EM',
                        'name': 'Mandiri-Emoney',
                        'code': 'MANDIRI-EMONEY',
                        'text': 'MANDIRI-EMONEY',
                        'description': 'Pay with Mandiri Emoney',
                        'long_description': 'null',
                        'image_url': 'null',
                        'status': 'OPEN'
            }]
            response = {}
            response['data'] = param_offline
            response_param = {
                'data': response['data'],
                'country': 'ID',
                'total': len(response['data'])
            }
            json_save = json.dumps(response_param, indent=4, sort_keys=True)
            with open('service/method.json', 'w') as jw:  
                jw.write(json_save)
            # _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))

        else:
            query_url = PPOB_URL_PAYMENT + 'api/v1/payment/getPaymentMethod'
            response, status = global_request(query_url, post_param)
            _LOG_.info("Payment Paramx : " + str(post_param) + str(query_url))
            if status == 200 and len(response['data']) > 0:
                response_param = {
                    'data': response['data'],
                    'country': 'ID',
                    'total': len(response['data'])
                }
                json_save = json.dumps(response_param, indent=4, sort_keys=True)
                with open('service/method.json', 'w') as jw:  
                    jw.write(json_save)

                _LOG_.info("Payment Paramx response : " + str(response_param))
                _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))
            elif response['statusCode'] == -1:
                param_offline = [{
                            'type': 'EM',
                            'name': 'Mandiri-Emoney',
                            'code': 'MANDIRI-EMONEY',
                            'text': 'MANDIRI-EMONEY',
                            'description': 'Pay with Mandiri Emoney',
                            'long_description': 'null',
                            'image_url': 'null',
                            'status': 'OPEN'
                }]
                response = {}
                response['data'] = param_offline
                response_param = {
                    'data': response['data'],
                    'country': 'ID',
                    'total': len(response['data'])
                }
                json_save = json.dumps(response_param, indent=4, sort_keys=True)
                with open('service/method.json', 'w') as jw:  
                    jw.write(json_save)
                # _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))

            else:
                _PPOB_.start_get_payment_signal.emit('NO_DATA')
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_payment : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _PPOB_.start_get_payment_signal.emit('ERROR')
        # _LOG_.warning(('[ERROR] get_payment : ', str(e)))

def start_get_payment():
    ClientTools.get_global_pool().apply_async(get_payment)


def get_payment():
    try:
        response = ""
        with open('service/method.json') as json_file:
            response = json.load(json_file)

        if (len(response['data'])==1):
            start_get_payment_offline()
            response_param = {
                'data': response['data'],
                'country': 'ID',
                'total': len(response['data'])
            }
            _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))
        else:
            response_param = {
                'data': response['data'],
                'country': 'ID',
                'total': len(response['data'])
            }
            _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))
            start_get_payment_offline()
        # response_param = {
        #     'data': response['data'],
        #     'country': 'ID',
        #     'total': len(response['data'])
        # }
        # _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] get_payment : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _PPOB_.start_get_payment_signal.emit('ERROR')    
    # try:
    #     session_id = get_payment_session()
    #     post_param = {
    #         'token': PPOB_TOKEN_PAYMENT,
    #         'country': 'ID',
    #         'session_id': session_id
    #     }
    #     _LOG_.debug(('payment_param_available : ', post_param))

    #     if session_id is None: 
    #         # param_offline = [{
    #         #             'type': 'EM',
    #         #             'name': 'Mandiri-Emoney',
    #         #             'code': 'MANDIRI-EMONEY',
    #         #             'text': 'MANDIRI-EMONEY',
    #         #             'description': 'Pay with Mandiri Emoney',
    #         #             'long_description': 'null',
    #         #             'image_url': 'null',
    #         #             'status': 'OPEN'
    #         # }]
    #         # response = {}
    #         # response['data'] = param_offline
    #         response = ""
    #         with open('service/method.json') as json_file:
    #             response = json.load(json_file)
    #         response_param = {
    #             'data': response['data'],
    #             'country': 'ID',
    #             'total': len(response['data'])
    #         }
    #         _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))

    #     else:
    #         query_url = PPOB_URL_PAYMENT + 'api/v1/payment/getPaymentMethod'
    #         response, status = global_request(query_url, post_param)
    #         _LOG_.info("Payment Paramx : " + str(post_param) + str(query_url))
    #         # param = {'id_payment': 1}
    #         # getpay = ExpressDao.local_get_payment(param)
    #         # empl_data = ""
    #         # getc = ""
    #         # getpay = json.dumps(getpay)
    #         # getpay_ = {}
    #         # for row in getpay[0]['parameter']:
    #         #     empl_data = empl_data + row
    #         # empl_data = str(empl_data)
    #         # getpay_ = json.loads(json.dumps(empl_data)) 
    #         # getpay_ = str(getpay[0]['parameter'])
    #         # getpay_ = getpay_['parameter']['data']
            
    #         # with open('service/data.json') as json_file:
    #         #     data = json.load(json_file)
    #         #     for p in data['data']:
    #         #         _LOG_.info('Name Payment Local : ' + p['code'])

    #         # for x in empl_data:
    #         #     _LOG_.info(("Payment Local : "+ x['data'])) 
    #         # _LOG_.info(("Payment Local : "+ empl_data['data'])) s
    #         # do something with empl_data
    #         # getpay__ = json.loads(getpay_)
    #         # getpay_ = getpay_['parameter']
    #         # getpay_ = str(getpay_)

    #         # getx = json.loads(json.dumps(getpay_))
    #         # _LOG_.info(("Payment Local : "+ empl_data))   
    #         # getpay_ = "{"+ getpay_ +"}"
    #         # getpay_ = json.loads(getpay_)
    #         # getpay = getpay.decode("utf-8")
    #         # getment = json.loads(json.dumps(getpay_))
    #         # getment = { 'param': str(getpay) }
    #         # getpay = json.dumps(getpay)
    #         # cgetpay = str(getpay)
    #         # cget = getpay['data']
    #         # cgetpay = getpay.decode("UTF-8")
    #         # xparam = {'data': [{'text': None, 'long_description': None, 'code': 'BNI-YAP', 'name': 'BNI Yap!', 'image_url': None, 'status': 'OPEN', 'type': None, 'description': None}, {'text': 'TCASH BIll Payment', 'long_description': None, 'code': 'TCASH', 'name': 'TCASH Bill Payment', 'image_url': None, 'status': 'OPEN', 'type': None, 'description': 'Pay with tcash QR'}, {'text': 'OttoPay', 'long_description': None, 'code': 'OTTO-QR', 'name': 'OttoPay', 'image_url': None, 'status': 'OPEN', 'type': None, 'description': 'Pay with Otto Pay'}, {'text': 'MANDIRI-EMONEY', 'long_description': None, 'code': 'MANDIRI-EMONEY', 'name': 'Mandiri-Emoney', 'image_url': None, 'status': 'OPEN', 'type': 'EM', 'description': 'Pay with Mandiri Emoney'}, {'text': 'MIDTRANS GOPAY', 'long_description': None, 'code': 'MID-GOPAY', 'name': 'MIDTRANS-GOPAY', 'image_url': 'https://payment.popbox.asia/files/channel/MID-GOPAY.png', 'status': 'OPEN', 'type': 'EW', 'description': 'Pay with gopay wallet'}], 'response': {'code': 200, 'message': None, 'latency': 0.06601405143737793}}
    #         # scode = json.dumps(xparam)
    #         # _LOG_.info(("Payment Local : "+ str(scode)))
    #         # dpar = xparam['data']
    #         # ron = "" 
    #         # for ron in dpar:
    #         #     _LOG_.info(("Payment Local : "+ ron['code']))

    #         if status == 200 and len(response['data']) > 0:
    #             response_param = {
    #                 'data': response['data'],
    #                 'country': 'ID',
    #                 'total': len(response['data'])
    #             }
    #             json_save = json.dumps(response_param, indent=4, sort_keys=True)
    #             with open('service/method.json', 'w') as f:  
    #                 f.write(json_save)

    #             _LOG_.info("Payment Paramx response : " + str(response_param))
    #             _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))
    #             # return response_param
    #         elif response['statusCode'] == -1:
    #             # param_offline = [{
    #             #             'type': 'EM',
    #             #             'name': 'Mandiri-Emoney',
    #             #             'code': 'MANDIRI-EMONEY',
    #             #             'text': 'MANDIRI-EMONEY',
    #             #             'description': 'Pay with Mandiri Emoney',
    #             #             'long_description': 'null',
    #             #             'image_url': 'null',
    #             #             'status': 'OPEN'
    #             # }]
    #             # response = {}
    #             # response['data'] = param_offline
    #             response = ""
    #             with open('service/method.json') as json_file:
    #                 response = json.load(json_file)
    #                 # for json_offline in json_data['data']:
    #                 #     _LOG_.info('Name Payment Local : ' + p['code'])
    #             response_param = {
    #                 'data': response['data'],
    #                 'country': 'ID',
    #                 'total': len(response['data'])
    #             }
    #             _PPOB_.start_get_payment_signal.emit(json.dumps(response_param))

    #         else:
    #             _PPOB_.start_get_payment_signal.emit('NO_DATA')
    # except Exception as e:
    #     exc_type, exc_obj, exc_tb = sys.exc_info()
    #     fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    #     _LOG_.warning(('[ERROR] get_payment : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
    #     _PPOB_.start_get_payment_signal.emit('ERROR')
    #     # _LOG_.warning(('[ERROR] get_payment : ', str(e)))

def get_payment_session(clientid="001"):
    try:
        param = {
            "token": PPOB_TOKEN_PAYMENT,
            "client_id": clientid
        }
        url = PPOB_URL_PAYMENT + "api/v1/createSession"
        response, status = global_request(url, param)
        _LOG_.debug(('get_payment_session : ', url, response))
        if status == 200 and response['data'][0]['session_id'] is not None:
            return response['data'][0]['session_id']
        elif response['statusCode'] == -1:
            return None
        else:
            return None
    except Exception as e:
        _LOG_.warning(('[ERROR] get_payment_session : ', str(e)))
        return None


def start_get_transaction(trx_id):
    ClientTools.get_global_pool().apply_async(get_transaction, (trx_id,))


def get_transaction(trx_id):
    try:
        post_param = {
            'token': PPOB_TOKEN,
            'transaction_reference': trx_id
        }
        query_url = PPOB_URL + 'api/locker/transaction/getTransaction'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'transaction_reference': trx_id,
                'total': len(response['data'])
            }
            _PPOB_.start_get_transaction_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_get_transaction_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_get_transaction_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_transaction : ', str(e)))


def start_flag_emoney(package):
    ClientTools.get_global_pool().apply_async(flag_emoney, (package,))


def flag_emoney(package):
    try:
        pack = json.loads(package)
        post_param = {
            'token': PPOB_TOKEN,
            'transaction_reference': pack['transaction'],
            'card_number': pack['card_no'],
            'last_balance': pack['last_balance'],
            'reader_id': READER_TID,
            'merchant_id': '009',
            'bank': 'mandiri',
            'paid_amount': pack['amount']
        }
        query_url = PPOB_URL + 'api/locker/transaction/paidEMoneyTransaction'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response['data']) > 0:
            response_param = {
                'data': response['data'],
                'transaction_reference': pack['transaction'],
                'card_number': pack['card_no'],
                'last_balance': pack['last_balance'],
                'reader_id': READER_TID,
                'paid_amount': pack['amount'],
                'total': len(response['data'])
            }
            _PPOB_.start_flag_emoney_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_flag_emoney_signal.emit('NO_DATA')
    except Exception as e:
        _PPOB_.start_flag_emoney_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] flag_emoney : ', str(e)))


def start_get_sepulsa(phone):
    ClientTools.get_global_pool().apply_async(get_sepulsa, (phone,))


def get_sepulsa(phone):
    try:
        post_param = {
            'token': 'f1830924a4cad06778a73d38db55a69e510d0452',
            'phone': phone
        }
        query_url = 'http://lockerapi.popbox.asia/service/sepulsa/getPulsaProduct'
        response, status = global_request(query_url, post_param)
        if status == 200 and len(response["data"]) != 0:
            response_param = {
                'data': response['data']['pulsa_list'],
                'operator': response['data']['operator'],
                'avail_prod': len(response['data'])
            }
            _PPOB_.start_get_sepulsa_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_get_sepulsa_signal.emit('ERROR')
    except Exception as e:
        _PPOB_.start_get_sepulsa_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] get_sepulsa : ', str(e)))


def start_get_ppob_button():
    ClientTools.get_global_pool().apply_async(get_ppob_button)


def get_ppob_button():
    try:
        query_url = GLOBAL_URL + 'box/ppobButton'
        response, status = global_request(query_url)
        if status == 200 and len(response["data"]) != 0:
            response_param = {
                'data': response['data'],
                'total': len(response['data'])
            }
            storing = local_button('store', json.dumps(response['data']))
            _LOG_.debug(('writing ppob_button : ', str(storing)))
            _PPOB_.start_get_ppob_button_signal.emit(json.dumps(response_param))
        else:
            prev_buttons = local_button('read')
            if prev_buttons is not False:
                buttons = json.loads(prev_buttons)
                response_param = {
                'data': buttons,
                'total': len(buttons)
                }
                _PPOB_.start_get_ppob_button_signal.emit(json.dumps(response_param))
            else:
                _PPOB_.start_get_ppob_button_signal.emit('ERROR')
    except Exception as e:
        if 'Invalid argument' in str(e):
            prev_buttons = local_button('read')
            if prev_buttons is not False:
                buttons = json.loads(prev_buttons)
                response_param = {
                'data': buttons,
                'total': len(buttons)
                }
                _PPOB_.start_get_ppob_button_signal.emit(json.dumps(response_param))
            else:
                _PPOB_.start_get_ppob_button_signal.emit('ERROR')
        else:
            _PPOB_.start_get_ppob_button_signal.emit('ERROR')
            _LOG_.warning(('[ERROR] get_ppob_button : ', str(e)))


def local_button(m, d=None):
    path_file = os.path.join(os.getcwd(), 'qml', 'button_payment.js')
    if m=='store':
        try:
            with open(path_file, 'w') as f:
                f.write(d)
                f.close()
            return d
        except Exception as e:
            _LOG_.warning(str(e))
            return False             
    elif m=='read':
        try:
            d = open(path_file, 'r').read()
            _LOG_.info(('read previous buttons :', str(d)))
            return d
        except Exception as e:
            _LOG_.warning(str(e))
            return False              
    else:
        return False      


def start_filter_sim_data(filtering):
    ClientTools.get_global_pool().apply_async(filter_sim_data, (filtering,))


def filter_sim_data(filtering):
    print('filter_sim_data : ', filtering)
    # pprint(SIM_PRODUCTS)
    filter_data = []
    try:
        sims = SIM_PRODUCTS['data']
    except Exception as e:
        sims = []
        _LOG_.warning(('[ERROR] failed to parse SIM_PRODUCTS', e))

    if len(sims) == 0:
        _PPOB_.start_get_sim_product_signal.emit('NO_DATA')
        return

    if filtering == 'RESET':
        response_param = {
            'data': sims,
            'operator': SIM_PRODUCTS['operator'],
            'type': SIM_PRODUCTS['type'],
            'total': len(sims)
        }
        _PPOB_.start_get_sim_product_signal.emit(json.dumps(response_param))
    else:
        for sim in sims:
            if filtering in sim['number']:
                filter_data.append(sim)
        if len(filter_data) != 0:
            response_param = {
                'data': filter_data,
                'operator': SIM_PRODUCTS['operator'],
                'type': SIM_PRODUCTS['type'],
                'total': len(filter_data)
            }
            _PPOB_.start_get_sim_product_signal.emit(json.dumps(response_param))
        else:
            _PPOB_.start_get_sim_product_signal.emit('NO_DATA')
        # pprint(str(filter_data))


def start_check_member(phone):
    ClientTools.get_global_pool().apply_async(check_member, (phone,))


def check_member(phone):
    __response = {
        "name": "User (" + phone + ")",
        "phone" : phone,
        "email" : "N/A"
    }
    if phone is None:
        _PPOB_.start_check_member_signal.emit(json.dumps(__response))
        return
    try:
        param = {
            "token": "4EI5COIOWBVPF6JVXTFPP4TMC8LZYPZ5J5HYUA1SKXEQQXA10Q",
            "phone": phone,
            "email": "a@a.com"
        }
        query_url = "http://popsendv2.popbox.asia/" + 'user/check'
        response, status = global_request(query_url, param)
        if status == 200 and len(response["data"]) != 0:
            _PPOB_.start_check_member_signal.emit(json.dumps(response["data"][0]))
        else:
            _PPOB_.start_check_member_signal.emit(json.dumps(__response))
    except Exception as e:
        _PPOB_.start_check_member_signal.emit(json.dumps(__response))
        _LOG_.warning(("[ERROR] post_activity : ", str(e)))
