import requests
import json
import logging
import os
import datetime
import time
import base64
import glob
import random
import sys
import ClientTools
import Configurator
import box.service.BoxService as BoxService
import express.repository.ExpressDao as ExpressDao
import company.service.CompanyService as CompanyService
import express.service.ExpressService as ExpressService
from PyQt5.QtCore import QObject, pyqtSignal
from network import HttpClient
from device import QP3000S
from device import Scanner
# from device import Detector
from database import ClientDatabase
import sys, os

__author__ = 'wahyudi@popbox.asia'


class ApSignalHandler(QObject):
    __qualname__ = 'ApSignalHandler'
    start_store_apexpress_signal = pyqtSignal(str)
    start_update_apexpress_signal = pyqtSignal(str)


_AP_ = ApSignalHandler()
_LOG_ = logging.getLogger()
img_path = sys.path[0] + '/video_capture/'
NOT_INTERNET_ = {
    'statusCode': -1,
    'statusMessage': 'Not Internet'}

# Get Locker Information
locker_info = BoxService.get_box()
SSL_VERIFY = True

##################################################################
###SUPPORTING_FUNCTIONS###
##################################################################
def global_request(url, param=None):
    global SSL_VERIFY
    headers = {'content-type': "application/json"}
    _LOG_.debug(('param ', str(param)))
    r_status = ''
    try:
        if 'https' in url:
            r = requests.post(url, json=param, timeout=60, headers=headers, verify=SSL_VERIFY)
        else:
            r = requests.post(url, json=param, timeout=60, headers=headers)
    except requests.RequestException:
        _LOG_.debug((NOT_INTERNET_, -1))
        return NOT_INTERNET_, -1
    try:
        r_json = r.json()
        r_status = r_json["response"]["code"]
    except ValueError:
        _LOG_.debug(('ValueError on ', url))
        return NOT_INTERNET_, r_status
    # logger.info((r_json, r_status))
    _LOG_.debug('***<URL> : ' + str(param) + ' | <STATUS> : ' + str(r_status) + ' | <POST> : ' + str(param) + ' | <RESP> : ' + str(r_json))
    return r_json, r_status
##################################################################
def rename_file(filename, list, x):
    for char in list:
        filename = filename.replace(char, x)
    return filename
##################################################################
def random_char(length):
    while True:
        chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
        random_ = ''
        i = 0
        while i < int(length):
            random_ += random.choice(chars)
            i += 1
        return random_
##################################################################
def force_rename(file1, file2):
    from shutil import move
    try:
        move(file1, file2)
        return True
    except:
        return False
##################################################################
POST_APEXPRESS_FLAG = True


def start_store_apexpress(phone, hour,  transactionRecord, paymentMethod, paymentAmount, lockerSize, paymentParam):
    global POST_APEXPRESS_FLAG
    if POST_APEXPRESS_FLAG is True:
        ClientTools.get_global_pool().apply_async(store_apexpress, (phone, hour, transactionRecord, paymentMethod, paymentAmount, lockerSize, paymentParam))
        POST_APEXPRESS_FLAG = False


def store_apexpress(phone, hour, transactionRecord, paymentMethod, paymentAmount, lockerSize, paymentParam):
    global POST_APEXPRESS_FLAG
    try:
        time.sleep(1)
        if phone is None or phone == "":
            _AP_.start_store_apexpress_signal.emit('ERROR')
            _LOG_.warning('[ERROR] start_store_apexpress phone is NULL')
            return
        if hour is None or hour == "":
            _AP_.start_store_apexpress_signal.emit('ERROR')
            _LOG_.warning('[ERROR] start_store_apexpress hour is NULL')
            return
        mouth_result = BoxService.mouth
        _LOG_.debug(('mouth_result:', mouth_result))
        box_result = BoxService.get_box()
        if not box_result:
            _AP_.start_store_apexpress_signal.emit('ERROR')
            _LOG_.warning('[ERROR] start_store_apexpress box_result is NULL')
            return
        overdue_time = get_overdue_hours(h=int(hour))
        # Default value user_drop for APEXPRESS
        user_result = {
            'id': '1a508842c1983236747ee1aaada4472d',
            'company': {'id': '161e5ed1140f11e5bdbd0242ac110001'}
        }        
        ap_param = {'id': ClientTools.get_uuid(),
                    'expressNumber': get_apexpress_number(),
                    'expressType': 'COURIER_STORE',
                    'overdueTime': overdue_time,
                    'status': 'IN_STORE',
                    'storeTime': ClientTools.now(),
                    'syncFlag': 0,
                    'takeUserPhoneNumber': phone,
                    'validateCode': ExpressService.random_validate(box_result['validateType'], chars='0123456789'),
                    'version': 0,
                    'box_id': box_result['id'],
                    'logisticsCompany_id': user_result['company']['id'],
                    'mouth_id': mouth_result['id'],
                    'operator_id': box_result['operator_id'],
                    'storeUser_id': user_result['id'],
                    'groupName': 'AP_EXPRESS',
                    'payment_param': json.dumps(paymentParam) }
        _LOG_.debug(('[ap_param]:', ap_param))
        
        ExpressDao.save_express(ap_param)
        mouth_param = {'id': mouth_result['id'],
                       'express_id': ap_param['id'],
                       'status': 'USED'}
        BoxService.use_mouth(mouth_param)
        ap_param['box'] = {'id': ap_param['box_id']}
        ap_param.pop('box_id')
        ap_param['logisticsCompany'] = {'id': ap_param['logisticsCompany_id']}
        ap_param.pop('logisticsCompany_id')
        ap_param['mouth'] = {'id': ap_param['mouth_id']}
        ap_param.pop('mouth_id')
        ap_param['operator'] = {'id': ap_param['operator_id']}
        ap_param.pop('operator_id')
        ap_param['storeUser'] = {'id': ap_param['storeUser_id']}
        ap_param.pop('storeUser_id')
        result_param = json.dumps({
            'result': 'SUCCESS',
            'pin_code': ap_param['validateCode'],
            'express': ap_param['expressNumber'],
            'phone_number': phone,
            'door_no': mouth_result['number'],
            'date': time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(overdue_time/1000))
        })
        _AP_.start_store_apexpress_signal.emit(result_param)

        ap_param['transactionRecord'] = transactionRecord
        ap_param['paymentMethod'] = paymentMethod
        ap_param['paymentAmount'] = paymentAmount
        ap_param['lockerSize'] = "L"
        ap_param['lockerNo'] = str(mouth_result['number'])

        
        _LOG_.info(('[SUCCESS] ap_param_new', ap_param))

        message, status_code = HttpClient.post_message('express/staffStoreExpress', ap_param)
        if status_code == 200 and message['id'] == ap_param['id']:
            ExpressDao.mark_sync_success(ap_param)
        # Sending WA Notif direct from locker -> Disabled on 23-10-2018
        # send_wa_notif(phone, get_content_wa(ap_param['validateCode'], overdue_time, box_result['name']))
        ap_param['output'] = result_param
        store_ap_transaction(ap_param)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] customer_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _AP_.start_store_apexpress_signal.emit('ERROR')
        _LOG_.warning(('[ERROR] start_store_apexpress', str(e)))
    finally:
        POST_APEXPRESS_FLAG = True


def get_content_wa(validate, overdue, locker):
    return '[ *PopBox Asia Notification* ]\nKode Pin pengambilan titipan Anda *' + validate + '* di PopBox @ ' + \
           locker + ', valid sampai *' + time.strftime('%d/%m/%Y %H:%M', time.localtime(overdue/1000))\
           + '* untuk durasi 8 jam pertama. Unduh Aplikasi PopBox di bit.do/getpopbox'


def start_store_ap_transaction(param):
    param = json.loads(param)
    ClientTools.get_global_pool().apply_async(store_ap_transaction, (param,))


def store_ap_transaction(param):
    transaction = {
        'id': ClientTools.get_uuid(),
        'paymentType': param['validateCode'],
        'transactionType': param['output'],
        'amount': param['paymentAmount'],
        'express_id': param['id'],
        'mouth_id': param['mouth']['id'],
        'createTime': ClientTools.now()
    }
    ExpressDao.save_record(transaction)


def get_overdue_hours(h):
    try:
        start_time = datetime.datetime.now()
        end_time = start_time + datetime.timedelta(hours=int(h))
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        _LOG_.debug(('get_overdue_hours', str(e)))


def get_apexpress_number():
    return 'APXID-' + random_char(5)


def start_update_apexpress(validateCode, paymentOverdue):
    ClientTools.get_global_pool().apply_async(update_apexpress, (validateCode,paymentOverdue,))


def update_apexpress(validateCode, paymentOverdue):
    global UPDATE_APEXRESS_FLAG
    overdue = json.dumps(paymentOverdue)
    apexpress_param = {
        'overdueTime': get_overdue_hours(8),
        'syncFlag': 0,
        'validateCode': validateCode,
        'status': 'IN_STORE',
        'paymentOverdue': overdue
    }
    ExpressDao.update_apexpress_by_pin(apexpress_param)
    # record_data_extend(extendData, paymentMethod,transactionRecord)
    _AP_.start_update_apexpress_signal.emit('Success')


def record_data_extend(param_extend, paymentMethod,transactionRecord):
    try:
        data_param = json.loads(param_extend)
        data_overdue = json.loads(data_param['overdue_data'])
        data_transaction = json.loads(data_param['transactionType'])
        token = Configurator.get_value('ClientInfo', 'Token')
        overduetime = data_overdue['date_overdue']
        ap_param_extend = {
            'box_id': data_param['mouth_id'],
            'expressNumber': data_transaction['express'],
            'id': data_param['id'],
            'lockerNo': data_transaction['door_no'],
            'lockerSize': "L",
            'overdueTime': overduetime,
            'paymentAmount': data_overdue['cost_overdue'],
            'paymentMethod': paymentMethod,
            'storeTime': data_param['createTime'],
            'duration': data_overdue['parcel_duration'],
            'extendTimes': data_overdue['extend_overdue'],
            'takeUserPhoneNumber': data_transaction['phone_number'],
            'transactionRecord': transactionRecord,
            'token': token,
            'validateCode': data_transaction['pin_code']
        }
        _LOG_.info(('[PARAM_EXTEND_POPSEND]', ap_param_extend))

        message, status_code = HttpClient.post_message('aps/extend', ap_param_extend)
        if status_code == 200 and message['invoice_code'] == ap_param_extend['expressNumber']:
            _LOG_.info(('[EXTEND_SUCCESS]', message))         
    except Exception as e:
        _LOG_.warning(('[EXTEND_FAILED] ', e))

def send_wa_notif(phone_no, content):
    try:
        param_send = {
            "token": "p0pb0x4514",
            "channel": "POPBOX",
            "recipient": phone_no,
            "content": content
        }
        url = 'http://wa-notif.popbox.asia/wa/push'
        resp, stat = global_request(url, param_send)
        if stat == 200 and resp['response']['code'] == 200:
            _LOG_.info(('WhatsApp Notification is Sent to ', phone_no))
        else:
            _LOG_.warning(('WhatsApp Notification is Failure to ', phone_no, resp))
    except Exception as e:
        _LOG_.warning(('WhatsApp Notification is Failure to ', phone_no, e))
