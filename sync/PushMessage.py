import time
import json
import logging
import ClientTools
import box.service.BoxService as BoxService
import express.service.ExpressService as ExpressService
# from device import UPSMachine
from network import HttpClient

__author__ = 'wahyudi@popbox.asia'
start_flag = True
_LOG_ = logging.getLogger()


def sync_express():
    try:
        express_list = ExpressService.get_scan_not_sync_express_list()
        _LOG_.info(("sync_express_list_RESULT : ", express_list))
        if len(express_list) == 0:
            return
        for express_ in express_list:
            if not ClientTools.get_value('storeTime', express_, default_value=0) > ClientTools.now() - 30000:
                _LOG_.info(("DATA_SYNC_LIST : ", express_))                
                if ClientTools.get_value('takeTime', express_, default_value=0) > ClientTools.now() - 30000:
                    continue
                express_['storeUser'] = {'id': ClientTools.get_value('storeUser_id', express_, None)}
                express_['mouth'] = {'id': express_['mouth_id']}
                express_['logisticsCompany'] = {'id': ClientTools.get_value('logisticsCompany_id', express_, None)}
                transaction_record = ExpressService.get_scan_sync_express_transaction_record(
                    {'express_id': express_['id']})
                express_['locker_identity'] = BoxService.get_locker_identity({'mouth_id': express_['mouth_id']})
                if len(transaction_record) != 0:
                    express_['transactionRecords'] = transaction_record
                if 'groupName' in express_.keys():
                    express_['groupName'] = express_['groupName']
                if 'staffTakenUser_id' in express_.keys():
                    express_['staffTakenUser'] = {'id': express_['staffTakenUser_id']}
                _LOG_.info(("sync_express : ", express_))
                message, status_code = HttpClient.post_message('express/syncExpress', express_)
                if status_code == 200 and message['id'] == express_['id']:
                    _LOG_.info(("sync_express RESULT : ", message))
                    ExpressService.mark_sync_success(express_)
                else:
                    _LOG_.debug(("sync_express FAILED : ", message))
    except Exception as e:
        _LOG_.debug(('sync_express ERROR :', e))


def sync_mouth():
    try:
        mouth_list = BoxService.get_scan_not_sync_mouth_list()
        _LOG_.info(("sync_mout_list : ", mouth_list))
        for mouth in mouth_list:
            message, status_code = HttpClient.post_message('box/mouth/sync', mouth)
            if status_code == 200 and message['id'] == mouth['id']:
                BoxService.mark_sync_success(mouth)
            else:
                _LOG_.debug(("sync_mouth : ", message))
    except Exception as e:
        _LOG_.debug(('sync_mouth ERROR :', e))


def sync_box():
    try:
        box_info = BoxService.get_box()
        _LOG_.debug(("sync_box_info : ", box_info))        
        if not box_info and box_info['syncFlag'] == 0:
            message, status_code = HttpClient.post_message('box/sync', box_info)
            if status_code == 200:
                BoxService.mark_box_sync_success()
            else:
                _LOG_.debug(("sync_box : ", message))
        else:
            _LOG_.info(("locker_info : ", box_info))
    except Exception as e:
        _LOG_.debug(('sync_box ERROR :', e))


# def check_ups():
#     try:
#         UPSMachine.get_ups_status()
#         time.sleep(2)
#         ups_status = UPSMachine.ups_status_result
#         _LOG_.info(('ups_status_result is :', ups_status))
#     except Exception as e:
#         _LOG_.debug(('check_ups ERROR :', e))


# def sync_ups():
#     try:
#         box_id = BoxService.get_box()
#         ups_warn_record = UPSMachine.check_ups_status()
#         for record in ups_warn_record:
#             if record['syncFlag'] == 0:
#                 record['box'] = box_id
#                 record['operator'] = {'id': box_id['operator_id']}
#                 message, status_code = HttpClient.post_message('alert/create', record)
#                 if status_code == 200:
#                     UPSMachine.syn_ups_warn(record)
#                 else:
#                     _LOG_.debug(message)
#                     continue
#
#     except Exception as e:
#         _LOG_.debug(('sync_ups ERROR :', e))


def sync_message():
    global start_flag
    while start_flag:
        sync_express()
        sync_mouth()
        sync_box()
        # check_ups()
        # sync_ups()
        time.sleep(60)


def set_start_flag(flag):
    global start_flag
    start_flag = flag


def start_sync_message():
    ClientTools.get_global_pool().apply_async(sync_message)
