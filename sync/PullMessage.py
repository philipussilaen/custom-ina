import logging
import time
import ClientTools
import Configurator
import json
import box.repository.BoxDao as BoxDao
import box.service.BoxService as BoxService
import express.service.ExpressService as ExpressService
import express.popbox.PopboxService as PopBoxService
import alert.service.AlertService as AlertService
# import device.Detector as Detector

from network import HttpClient

__author__ = 'wahyudi@popbox.asia'
_LOG_ = logging.getLogger()

IDLE_MODE = True


def start_idle_mode():
    _start_ = 'START'
    ClientTools.get_global_pool().apply_async(idle_mode, (_start_, ))


def stop_idle_mode():
    _stop_ = 'STOP'
    ClientTools.get_global_pool().apply_async(idle_mode, (_stop_, ))


def idle_mode(_c_):
    global IDLE_MODE
    if _c_ == 'START':
        IDLE_MODE = True
    elif _c_ == 'STOP':
        IDLE_MODE = False
    print('pyt: Set IDLE_MODE into '+str(IDLE_MODE)+' at '+ClientTools.time_str())


def start_pull_message():
    ClientTools.get_global_pool().apply_async(pull_message)


def pull_message():
    attempt = 0
    while True:
        attempt += 1
        try:
            # Detector.setting["status"] = True if (
            #         Configurator.get_value('panel', 'detector') == "enabled") else False
            # Detector.setting["max_count"] = 1
            # status, response = Detector.start_detection()
            # self_count = response['count'] if (status is True and 'error' not in response) else 0
            counting = 0
            # if attempt % 2 == 0:
            #     counting = Detector.people_count
            #     Detector.people_count = attempt = 0
            if IDLE_MODE is True:
                comp_stat = PopBoxService.get_comp_stats()
                stat_param = {
                    "cpu": comp_stat["cpu_temp"],
                    "disk": comp_stat["disk_space"],
                    "memory": comp_stat["memory_space"],
                    "people_count": counting
                }
                message, status_code = HttpClient.post_message('box/pull', stat_param)
            else:
                message, status_code = HttpClient.get_message('box/pull')
            if status_code == 200:
                parsing_message(message)
            time.sleep(60)
        except Exception as e:
            _LOG_.debug(('pull_message ERROR :', e))


# Can Do Multiple Task
def parsing_message(push_message_list):
    if len(push_message_list) == 0:
        return
    for message_result in push_message_list:
        if message_result['pushMessageType'] == 'BOX_START_TIME_CHANGE':
            if not BoxService.update_free_time(message_result['value']):
                continue
            message_result['value'] = message_result['value']['id']
            HttpClient.post_message('box/finish', message_result)
        if message_result['pushMessageType'] == 'MOUTH_STATUS_CHANGE':
            BoxDao.update_mouth_status(message_result['value'])
            message_result['value'] = message_result['value']['id']
            HttpClient.post_message('box/finish', message_result)
        if message_result['pushMessageType'] == 'INIT_CLIENT':
            BoxService.start_init_client()
            message_result['value'] = message_result['value']['id']
            HttpClient.post_message('box/finish', message_result)
        if message_result['pushMessageType'] == 'STORE_EXPRESS':
            ExpressService.start_service_pull_store_express(message_result['value'], message_result['timestamp'])
            message_result['value'] = message_result['value']['id']
            HttpClient.post_message('box/finish', message_result)
        if message_result['pushMessageType'] == 'BACKEND_REBOOT':
            post_result = {'id': message_result['value']['id'], 'value': message_result['value'],
                           'pushMessageType': message_result['pushMessageType']}
            response, status = HttpClient.post_message('box/finish', post_result)
            AlertService.system_reboot_remotely(value_id=post_result['id'], event='system_reboot_remotely')
            if (status == "200" or status == 200) and response is not None:
                import os
                os.system('shutdown -r -f -t 0')
        if message_result['pushMessageType'] == 'FORCE_RESYNC_ALL':
            post_result = {'id': message_result['value']['id'], 'value': message_result['value'],
                           'pushMessageType': message_result['pushMessageType']}
            response, status = HttpClient.post_message('box/finish', post_result)
            AlertService.system_reboot_remotely(value_id=post_result['id'], event='force_resync_all')
            if (status == "200" or status == 200) and response is not None:
                PopBoxService.force_resync_all()

        if message_result['pushMessageType'] == 'REMOTE_COMMAND':
            post_result = {'id': message_result['value']['id'], 'value': message_result['value'],
                           'pushMessageType': message_result['pushMessageType']}
            response, status = HttpClient.post_message('box/finish', post_result)
            AlertService.system_reboot_remotely(value_id=post_result['id'], event='force_remote_command')
            if (status == "200" or status == 200) and response is not None:
                PopBoxService.execute_command(message_result['value']['taskType'])

        if message_result['pushMessageType'] == 'ASYNC_TASK':
            timeout = int(ClientTools.get_value('timeout', message_result['value']))
            if message_result['value']['taskType'] == 'REMOTE_UNLOCK':
                if timeout is not None and timeout < ClientTools.now():
                    result = {'id': message_result['value']['id'], 'result': 'TIMEOUT', 'statusType': 'ERROR'}
                    HttpClient.post_message('task/finish', result)
                    continue
                else:
                    BoxService.pull_open_mouth(message_result['value'])
            if message_result['value']['taskType'] == 'RESET_EXPRESS':
                ExpressService.start_reset_express(message_result['value'])
            if message_result['value']['taskType'] == 'UPDATE_BOX':
                BoxService.start_update_box(message_result['value'])
            if message_result['value']['taskType'] == 'MOUTH_STATUS_CHANGE':
                BoxService.start_update_mouth_status(message_result['value'])
            else:
                continue


# To Do Single Task Only
def doing_task(message_result):
    if not message_result:
        return
    if message_result['pushMessageType'] == 'BOX_START_TIME_CHANGE':
        if not BoxService.update_free_time(message_result['value']):
            message_result['value'] = message_result['value']['id']
            HttpClient.post_message('box/finish', message_result)
    if message_result['pushMessageType'] == 'MOUTH_STATUS_CHANGE':
        BoxDao.update_mouth_status(message_result['value'])
        message_result['value'] = message_result['value']['id']
        HttpClient.post_message('box/finish', message_result)
    if message_result['pushMessageType'] == 'INIT_CLIENT':
        BoxService.start_init_client()
        message_result['value'] = message_result['value']['id']
        HttpClient.post_message('box/finish', message_result)
    if message_result['pushMessageType'] == 'STORE_EXPRESS':
        ExpressService.start_service_pull_store_express(message_result['value'], message_result['timestamp'])
        message_result['value'] = message_result['value']['id']
        HttpClient.post_message('box/finish', message_result)
    if message_result['pushMessageType'] == 'ASYNC_TASK':
        timeout = int(ClientTools.get_value('timeout', message_result['value']))
        if message_result['value']['taskType'] == 'REMOTE_UNLOCK':
            if timeout is not None and timeout < ClientTools.now():
                result = {'id': message_result['value']['id'], 'result': 'TIMEOUT', 'statusType': 'ERROR'}
                HttpClient.post_message('task/finish', result)
            else:
                BoxService.pull_open_mouth(message_result['value'])
        if message_result['value']['taskType'] == 'RESET_EXPRESS':
            ExpressService.start_reset_express(message_result['value'])
        if message_result['value']['taskType'] == 'UPDATE_BOX':
            BoxService.start_update_box(message_result['value'])
        if message_result['value']['taskType'] == 'MOUTH_STATUS_CHANGE':
            BoxService.start_update_mouth_status(message_result['value'])
