import cv2
from time import gmtime, strftime
import sys
import logging as log

input("""
Press 'C' - To Capture Image
Press 'X' - To Exit Application
Press ANY Key to Start
""")

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
log.basicConfig(filename='webcam.log', level=log.INFO)

webcam = cv2.VideoCapture(0)
x = 0
anterior = 0

while True:
    x = x + 1
    check, frame = webcam.read()
    frame = cv2.flip(frame, 180)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    current_time = strftime("%Y%m%d_%H%M%S", gmtime())

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    if anterior != len(faces):
        anterior = len(faces)
        log.info("face detected : " + str(len(faces)) + " at " + current_time)

    # print(check)
    # print(frame)
    cv2.imshow('WebCam Viewer', frame)
    key = cv2.waitKey(1)

    if key == ord('c'):
        filename = sys.path[0] + '/' + current_time + '_frame.jpg'
        cv2.imwrite(filename, frame)
        print(filename)

    del frame

    if key == ord('x'):
        break

# print(x)
webcam.release()
cv2.destroyAllWindows()
