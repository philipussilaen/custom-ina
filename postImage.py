#import Configurator
import sys
import datetime
import os
import base64
import glob
import time
import pprint
#from box.service.BoxService import *
import requests

def grab_image():
    global latest_img
    try:
        #file_path = sys.path[0] + '/video_capture/'
        latest_img = max(glob.iglob('video_capture/*.jpg'), key=os.path.getctime)
        print(latest_img)
    except:
        return 'Failed'
            
def convert_image():
    global b64_image
    
    if grab_image() != 'Failed':
        try:
            with open(latest_img, 'rb') as imageFile:
                b64_image = base64.b64encode(imageFile.read())
                print(b64_image.decode('ascii'))
        except:
            print('Convert Failed')
    else:
        print('Abort')
        

def set_param():
    global photo_param
    
    photo_param = dict()
    photo_param['base64_image'] = b64_image.decode('ascii')
    photo_param['datetime'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    photo_param['event_name'] = 'Python Test 999'
    photo_param['locker_name'] = 'Jakarta TestStation'
    photo_param['token'] = 'f1830924a4cad06778a73d38db55a69e510d0452'
    
    #photo_param['token'] = Configurator.get_value('popbox', 'tokenphoto')
    #box_info = get_box()
    #photo_param['locker_name'] = box_info['name']
    
    pprint.pprint(photo_param)


def popbox_get_message(url, param):
    try:
        r = requests.post(url, json = param, timeout = 50)
    except requests.RequestException:        
        return 'Failed Post Data' 
    try:
        r_json = r.json()
    except ValueError:        
        return 'Failed Get Response'
    return r_json


def post_image():
    #photo_url = Configurator.get_value('popbox', 'photoserver')
    photo_url = 'http://api-dev.popbox.asia/locker/photo'
    
    try:
        response_server = popbox_get_message(photo_url, photo_param)
        if response_server['response']['code'] == '200' and response_server['response']['message'] == 'OK':
            print('Post Image is Success')
        else:
            print('Check Parameter Post')
    except:
        print('Check Module Connection')   
               
    
def main():
    grab_image()
    convert_image()
    set_param()
    post_image()


if '__init__' == '__main__':
    main()
