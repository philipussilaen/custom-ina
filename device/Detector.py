import cv2
from time import gmtime, strftime, sleep
import Configurator
import logging
import sys
from imutils.object_detection import non_max_suppression
import numpy as np
import imutils


__author__ = "wahyudi"
_LOG_ = logging.getLogger()
# rstp = "rtsp://admin:popbox12345@192.168.5.88:554/Streaming/Channels/201"

try:
    cascPath = sys.path[0] + "/device/" + "face_weight.xml"
except Exception as e:
    _LOG_.warning(("get_face_weight ERROR : ", e))
    cascPath = None

GLOBAL_SETTING = {
    "status": False,
    "face_detector": True,
    "face_weight": cascPath,
    "motion_detector": True,
    "printable": True,
    "rstp": Configurator.get_value('panel', 'rstp'),
    "max_count": 10,
    "fps": 1,
    "prompt": False,
    "non_max_suppression": False,
    "less_padding": False
}

if GLOBAL_SETTING['rstp'] is None:
    video_source = 0
else:
    video_source = GLOBAL_SETTING['rstp']

# ===============
modulus = 5
# ===============
people_count = 0
# ===============

def inside(r, q):
    rx, ry, rw, rh = r
    qx, qy, qw, qh = q
    return rx > qx and ry > qy and rx + rw < qx + qw and ry + rh < qy + qh


def draw_detections(img, rects, color=(0, 255, 0), thickness=1):
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        pad_w, pad_h = int(0.15 * w), int(0.05 * h)
        cv2.rectangle(img, (x + pad_w, y + pad_h), (x + w - pad_w, y + h - pad_h), color, thickness)


START_DETECTION = False


def start_detection():
    global people_count, START_DETECTION

    START_DETECTION = True
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    try:
        video_capture = cv2.VideoCapture(video_source)
        video_capture.set(6, GLOBAL_SETTING["fps"])
    except Exception as e:
        _LOG_.warning(("start_detection ERROR : ", e))
        return True, {"error": str(e)}

    # ret, frame = video_capture.read()
    # height, width, layers = frame.shape
    # fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    # out = self.video_output
    # video = cv2.VideoWriter(out, fourcc, fps, (width, height), 1)

    count_skip = 0
    res_face = []
    res_motion = []
    anterior_face = 0
    anterior_motion = 0
    limit_modulus = 0

    while GLOBAL_SETTING['status'] is True:
        count_skip += 1
        limit_modulus += 1
        if count_skip < modulus:
            continue
        ret, frame = video_capture.read()
        frame = imutils.resize(frame, width=min(400, frame.shape[1]))
        if frame is None:
            break
        count_skip = 0
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        current_time = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
        faces = None

        if GLOBAL_SETTING['face_weight'] is not None and GLOBAL_SETTING["face_detector"] is True:
            faceCascade = cv2.CascadeClassifier(cascPath)
            faces = faceCascade.detectMultiScale(
                gray,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(30, 30)
            )

            draw_detections(img=frame, rects=faces, color=(255, 255, 255), thickness=2)

            if len(faces) > 0:
                face_found = "face(s) [" + str(len(faces)) + "] : " + current_time
                anterior_face += 1
                if anterior_face % modulus == 0:
                    res_face.append(face_found)
                    people_count += len(faces)
                if GLOBAL_SETTING['printable'] is True:
                    print(face_found)

        motion_ = None
        motions_ = []

        if GLOBAL_SETTING["motion_detector"] is True:
            if GLOBAL_SETTING["less_padding"] is True:
                motion_, weight = hog.detectMultiScale(frame, winStride=(4, 4), padding=(8, 8), scale=1.05)
            else:
                motion_, weight = hog.detectMultiScale(frame, winStride=(8, 8), padding=(16, 16), scale=1.05)
            # for ri, r in enumerate(motion_):
            #     for qi, q in enumerate(motion_):
            #         if ri != qi and inside(r, q):
            #             break
            #         else:
            #             motions_.append(r)
            if GLOBAL_SETTING["non_max_suppression"] is True:
                motion_ = np.array([[x, y, x + w, y + h] for (x, y, w, h) in motion_])
                motion_ = non_max_suppression(motion_, probs=None, overlapThresh=0.65)

            draw_detections(img=frame, rects=motion_, color=(0, 255, 0), thickness=2)
            # draw_detections(img=frame, rects=motions_, color=(0, 255, 0), thickness=3)

            if len(motion_) > 0:
                motion_found = "motion(s) [" + str(len(motion_)) + "] : " + current_time
                anterior_motion += 1
                if GLOBAL_SETTING['prompt'] is True:
                    res_motion.append(motion_found)
                    people_count += len(motion_)
                else:
                    if anterior_motion % modulus == 0:
                        res_motion.append(motion_found)
                        people_count += len(motion_)
                if GLOBAL_SETTING['printable'] is True:
                    print(motion_found)

        # video.write(frame)
        # cv2.imshow('Video', frame)
        # if cv2.waitKey(1) & 0xFF == ord('q') or len(result) >= 50:

        #to get prompt response even calculation is still ongoing
        if (len(res_face) + len(res_motion)) > 0 and GLOBAL_SETTING['prompt'] is True:
            return GLOBAL_SETTING["status"], {"result_face": len(res_face),
                               "result_motion": len(res_motion),
                               "count": len(res_motion) + len(res_face)
                                              }

        if (len(res_face) + len(res_motion)) >= GLOBAL_SETTING['max_count']:
            del frame
            del faces
            del gray
            del motion_
            del motions_
            START_DETECTION = False
            break

    # cv2.destroyAllWindows()
    video_capture.release()
    return GLOBAL_SETTING["status"], {"result_face": len(res_face),
                               "result_motion": len(res_motion),
                               "count": GLOBAL_SETTING["max_count"]
                                      }


# video.release()

'''
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

# loop over the image paths
imagePaths = list(paths.list_images(args["images"]))

for imagePath in imagePaths:
	# load the image and resize it to (1) reduce detection time
	# and (2) improve detection accuracy
	image = cv2.imread(imagePath)
	image = imutils.resize(image, width=min(400, image.shape[1]))
	orig = image.copy()

	# detect people in the image
	(rects, weights) = hog.detectMultiScale(image, winStride=(4, 4),
		padding=(8, 8), scale=1.05)

	# draw the original bounding boxes
	for (x, y, w, h) in rects:
		cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)

	# apply non-maxima suppression to the bounding boxes using a
	# fairly large overlap threshold to try to maintain overlapping
	# boxes that are still people
	rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
	pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

	# draw the final bounding boxes
	for (xA, yA, xB, yB) in pick:
		cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)
'''
