import os
import pygame
import pygame.camera
import time
import sys
import logging
import ClientTools

cam = None
_LOG_ = logging.getLogger()
file_path = sys.path[0] + '/video_capture/'

use_pygame = False


def init_camera():
    global file_path
    global cam
    global use_pygame
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    try:
        pygame.camera.init()
        pygame.camera.list_cameras()
        cam = pygame.camera.Camera(0, (640, 480))
        use_pygame = True
    except Exception as e:
        cam = None
        _LOG_.warning(('[ERROR] init_camera Pygame:', str(e)))
        init_opencv()


cv2 = None


def init_opencv():
    global cam, use_pygame, cv2
    try:
        import cv2 as cv2
        cam = cv2.VideoCapture(0)
        cam.set(3, 800)
        cam.set(4, 600)
        use_pygame = False
        cam.release()
    except Exception as e:
        cam = None
        _LOG_.warning(('[ERROR] init_camera OpenCV:', str(e)))


def start_video_capture(filename):
    ClientTools.get_global_pool().apply_async(video_capture, (filename,))


def video_capture(filename):
    if cam is None or filename is None:
        return
    try:
        delete_capture_file()
        cam_time = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
        cam_name = file_path + cam_time + '_' + filename + '.jpg'
        if use_pygame:
            cam.start()
            img = cam.get_image()
            pygame.image.save(img, cam_name)
            del img
            cam.stop()
        else:
            if not cam.isOpened():
                cam.open(0)
            time.sleep(1.5)
            status, image = cam.read()
            if status:
                cv2.imwrite(cam_name, image)
            del image
            cam.release()
    except Exception as e:
        _LOG_.warning(('[ERROR] video_capture :', str(e)))


def delete_capture_file():
    try:
        tady_time = ClientTools.today_time()
        capture_file = os.listdir(file_path)
        for _capture_file in capture_file:
            if 'no_cam' not in _capture_file:
                file_date = _capture_file.split('_')[0]
                _capture_date = file_date[0:4] + '-' + file_date[4:6] + '-' + file_date[6:8]
                _capture_time = int(time.mktime(time.strptime(_capture_date, '%Y-%m-%d')))
                if tady_time - _capture_time >= 5184000:
                    os.remove(os.path.join(file_path, _capture_file))
                continue
    except Exception as e:
        _LOG_.warning(("[ERROR] delete_capture_file : ", str(e)))
