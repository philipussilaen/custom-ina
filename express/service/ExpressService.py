import json
import logging
import math
import random
import datetime
import time
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import Configurator
import box.repository.BoxDao as BoxDao
import company.service.CompanyService as CompanyService
import company.repository.CompanyDao as CompanyDao
import express.repository.ExpressDao as ExpressDao
import box.service.BoxService
import user.service.UserService as UserService
from network import HttpClient
from device import Scanner
from device import Camera
from device import QP3000S
from array import array
import sys, os

__author__ = 'wahyudi@popbox.asia'


class ExpressSignalHandler(QObject):
    customer_take_express_signal = pyqtSignal(str)
    # overdue_apservice_signal = pyqtSignal(str)
    overdue_cost_signal = pyqtSignal(str)
    barcode_signal = pyqtSignal(str)
    store_express_signal = pyqtSignal(str)
    phone_number_signal = pyqtSignal(str)
    paid_amount_signal = pyqtSignal(str)
    customer_store_express_signal = pyqtSignal(str)
    overdue_express_list_signal = pyqtSignal(str)
    overdue_express_count_signal = pyqtSignal(int)
    staff_take_overdue_express_signal = pyqtSignal(str)
    load_express_list_signal = pyqtSignal(str)
    customer_store_express_cost_signal = pyqtSignal(str)
    store_customer_express_result_signal = pyqtSignal(str)
    customer_express_cost_insert_coin_signal = pyqtSignal(str)
    send_express_list_signal = pyqtSignal(str)
    send_express_count_signal = pyqtSignal(int)
    staff_take_send_express_signal = pyqtSignal(str)
    imported_express_result_signal = pyqtSignal(str)
    customer_reject_express_signal = pyqtSignal(str)
    reject_express_signal = pyqtSignal(str)
    reject_express_list_signal = pyqtSignal(str)
    product_file_signal = pyqtSignal(str)
    start_get_express_info_result_signal = pyqtSignal(str)
    start_get_pakpobox_express_info_signal = pyqtSignal(str)
    start_get_customer_info_by_card_signal = pyqtSignal(int)
    start_payment_by_card_signal = pyqtSignal(str)
    start_get_cod_status_signal = pyqtSignal(str)
    local_user_get_signal = pyqtSignal(str)


_LOG_ = logging.getLogger()
_EXPRESS_ = ExpressSignalHandler()
barcode_run_flag = False
barcode_start = False
overdue_cost = 0
server = Configurator.get_or_set_value('ClientInfo', 'prox^prefix', 'pr0x')


# SET OVERDUE
overdueTime = float(Configurator.get_value('OverdueTime', 'hours'))

if server not in Configurator.get_value('ClientInfo', 'serveraddress'):
    still_ebox = True
else:
    still_ebox = False


def init_express(param, mouth, box_param):
    express = {'id': param['id'],
               'expressType': param['expressType'],
               'status': param['status'],
               'storeTime': param['storeTime'],
               'syncFlag': 1,
               'version': param['version'],
               'box_id': mouth['box_id'],
               'logisticsCompany_id': ClientTools.get_value('id', ClientTools.get_value('logisticsCompany', param)),
               'operator_id': box_param['operator_id'],
               'mouth_id': mouth['id'],
               'storeUser_id': ClientTools.get_value('id', ClientTools.get_value('storeUser', param)),
               'groupName': load_param_or_default('groupName', param),
               'expressNumber': load_param_or_default('expressNumber', param),
               'customerStoreNumber': load_param_or_default('customerStoreNumber', param),
               'overdueTime': load_param_or_default('overdueTime', param),
               'takeUserPhoneNumber': load_param_or_default('takeUserPhoneNumber', param),
               'storeUserPhoneNumber': ClientTools.get_value('phoneNumber', ClientTools.get_value('storeUser', param)),
               'validateCode': load_param_or_default('validateCode', param),
               'designationSize': load_param_or_default('designationSize', param),
               'chargeType': load_param_or_default('chargeType', param),
               'continuedHeavy': load_param_or_default('continuedHeavy', param),
               'endAddress': load_param_or_default('endAddress', param),
               'continuedPrice': load_param_or_default('continuedPrice', param),
               'startAddress': load_param_or_default('startAddress', param),
               'firstHeavy': load_param_or_default('firstHeavy', param),
               'firstPrice': load_param_or_default('firstPrice', param),
               'payOfAmount': load_param_or_default('payOfAmount', param),
               'electronicCommerce_id': ClientTools.get_value('id', ClientTools.get_value('electronicCommerce', param)),
               'takeUser_id': ClientTools.get_value('id', ClientTools.get_value('takeUser', param))}
    ExpressDao.init_express(express)
    if express['electronicCommerce_id'] is not None:
        company_list = CompanyDao.get_company_by_id({'id': express['electronicCommerce_id']})
        electronic_commerce = {'id': express['electronicCommerce_id'],
                               'companyType': 'ELECTRONIC_COMMERCE',
                               'name': param['electronicCommerce']['name'],
                               'deleteFlag': 0,
                               'parentCompany_id': ClientTools.get_value('parentCompany_id',
                                                                         param['electronicCommerce'], None)}
        if len(company_list) == 0:
            CompanyDao.insert_company(electronic_commerce)
        else:
            CompanyDao.update_company(electronic_commerce)


def load_param_or_default(key, dict_, default_value=None):
    if key in dict_.keys():
        return dict_[key]
    return default_value


def start_barcode_take_express():
    global barcode_start
    if barcode_start:
        return
    barcode_start = True
    Scanner._SCANNER_.barcode_result.connect(get_express_barcode_text)
    Scanner.start_get_text_info()


def stop_barcode_take_express():
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    Scanner._SCANNER_.barcode_result.disconnect(get_express_barcode_text)
    Scanner.start_stop_scanner()


def start_customer_take_express(validate_code):
    ClientTools.get_global_pool().apply_async(customer_take_express, (validate_code,))
    
def start_operator_taken(op_param):
    ClientTools.get_global_pool().apply_async(operator_take_express, (op_param,))


AP_EXPRESS = None
AP_OVERDUE = None

def operator_take_express(param_op):
    op_param = json.loads(param_op)
    express_id = op_param['express_id']
    _LOG_.info(("[express_id_operator_taken]", express_id))
    if express_id == '':
        _LOG_.warning(("[operator_taken] express_id null"))        
    else:
        try:
            express = {'takeTime': ClientTools.now(),
               'status': 'OPERATOR_TAKEN',
               'syncFlag': 0,
               'version': 1,
               'id': express_id,
               'staffTakenUser_id': None}
            
            data_operator = {
                'dataOperator': json.dumps(param_op),
                'id': express_id
            }
            ExpressDao.take_express(express)
            ExpressDao.save_data_operator_taken(data_operator)
            mouth_param = box.service.BoxService.get_mouth(express)
            box.service.BoxService.free_mouth(mouth_param)
            box.service.BoxService.open_mouth(mouth_param['id'])
            param = {
                'id': express_id,
            }
            data_express = ExpressDao.getdata_express_operator_taken(param)
            _LOG_.info(("[data_express_operator_taken]", data_express))

            try:
                express['expressNumber'] = data_express['expressNumber']
                express['takeUserPhoneNumber'] = data_express['takeUserPhoneNumber']
                express['overdueTime'] = data_express['overdueTime']
                express['storeTime'] = data_express['storeTime']
                express['validateCode'] = data_express['validateCode']
                express['box_id'] = data_express['box_id']
                express['logisticsCompany_id'] = data_express['logisticsCompany_id']
                express['mouth_id'] = data_express['mouth_id']
                express['operator_id'] = data_express['operator_id']
                express['storeUser_id'] = data_express['storeUser_id']
                express['dataOperator'] = data_express['dataOperator']
                express['isExtend'] = False

            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                _LOG_.warning(('[ERROR] operator_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
            
            _LOG_.warning(("[operator_taken] SEND DATA TO PROX : ", express))
            result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
            if status_code == 200 and result['id'] == express['id']:
                ExpressDao.mark_sync_success(express)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            _LOG_.warning(('[ERROR] operator_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
            # _EXPRESS_.operator_take_express_signal.emit('Error')


def customer_take_express(data_take):
    global AP_EXPRESS
    try:
        checkIsExtend = False
        dataExtend = ""
        checkDataExist = False

        _LOG_.info(('DATA_PARAM_VALIDATECODE', data_take))
        tempValidateCode = data_take.split('||')
        checkIsJson = is_json(tempValidateCode[0])
        _LOG_.info(('check_is_json', checkIsJson, tempValidateCode[0]))

        if checkIsJson==True:
            getValidateCode = json.loads(tempValidateCode[0])
            _LOG_.info(('check_is_json', checkIsJson))
            validate_code = getValidateCode['extendPincode']
            dataExtend = validate_code[2]
            checkIsExtend = True
        else:
            validate_code = tempValidateCode[0]

        # dataExtend = validate_code[2]
        param = {'validateCode': validate_code,
                 'status': 'IN_STORE'}
        _LOG_.info(('DATA_PARAM_TAKE', param))
        _LOG_.info(('TEMP_VALIDATE_CODE', len(tempValidateCode)))			

        expresses = ExpressDao.get_express_by_validate(param)
        if len(tempValidateCode) == 2:
            try:
                check_express = ExpressDao.get_record_appexress(param)
                _LOG_.warning(('customer_take_express for AP_EXPRESS check_express', param))
                _LOG_.info(('RESULT_CHECK_EXPRESS', check_express))
                _LOG_.info(('LONG_CHECK_EXPRESS', len(check_express)))

                if len(check_express) != 0:
                    AP_EXPRESS = check_express[0]
                    checkDataExist = True
                    _LOG_.info(('DATA_AP_EXPRESS_FROM_CHECKING', check_express[0]))
                else:
                    _LOG_.info(('RESULT_CHECK_EXPRESS_RETRY', check_express))
                    if len(expresses) > 0:
                         checkDataExist = True 
                    else:
                        checkDataExist = False 
                    _LOG_.info(('AP_EXPRESS_RETRY', check_express))
                    
            except Exception as e:
                _LOG_.warning(('customer_take_express for AP_EXPRESS', e))
                if checkDataExist is not True:
                    _EXPRESS_.customer_take_express_signal.emit('Error')
        else:
            _LOG_.debug(('[DEBUG] not AP_EXPRESS parcel : ', str(validate_code)))
        
        if checkDataExist == True:
            #Checking data Is Extend, default false
            if checkIsExtend==True:
                _LOG_.warning(('[DEBUG] CHECK_IS_EXTEND ', getValidateCode['transactionRecord'], expresses))
                _LOG_.warning(('[DEBUG] EXPRESS ', expresses, dataExtend))
                pre_take_express(expresses, getValidateCode)
            else:
                _LOG_.warning(('[DEBUG] EXPRESS ', expresses, dataExtend))
                pre_take_express(expresses, dataExtend)
        elif checkDataExist is not True:
            _EXPRESS_.customer_take_express_signal.emit('Error')

           
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] customer_take_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.customer_take_express_signal.emit('Error')


def get_express_barcode_text(text_info):
    global _EXPRESS_
    global barcode_start
    if not barcode_start:
        return
    barcode_start = False
    if text_info == 'ERROR':
        return
    if text_info == '':
        _EXPRESS_.customer_take_express_signal.emit('NotInput')
        return
    if text_info.find('|') == -1:
        _EXPRESS_.customer_take_express_signal.emit('Error')
        return
    express_info = text_info.split('|')
    param = {'id': express_info[0].strip(),
             'validateCode': express_info[1].strip(),
             'status': 'IN_STORE'}
    expresses = ExpressDao.get_express_by_id_and_validate(param)
    pre_take_express(expresses)


def pre_take_express(expresses, dataExtend=""):
    if len(expresses) < 1:
        _EXPRESS_.customer_take_express_signal.emit('Error')
        return
    express = expresses[0]
    if express['overdueTime'] > ClientTools.now():
        take_express(express, False, dataExtend)
    else:
        pre_take_overdue_express(express)


def take_express(param, overdue=False, dataExtend=""):
    express = {'takeTime': ClientTools.now(),
               'status': 'CUSTOMER_TAKEN',
               'syncFlag': 0,
               'version': param['version'] + 1,
               'id': param['id'],
               'staffTakenUser_id': None}
    ExpressDao.take_express(express)
    mouth_param = box.service.BoxService.get_mouth(express)
    box.service.BoxService.free_mouth(mouth_param)
    box.service.BoxService.open_mouth(mouth_param['id'])
    try:
        express['expressNumber'] = param['expressNumber']
        express['takeUserPhoneNumber'] = param['takeUserPhoneNumber']
        express['overdueTime'] = param['overdueTime']
        express['storeTime'] = param['storeTime']
        express['validateCode'] = param['validateCode']
        express['box_id'] = param['box_id']
        express['logisticsCompany_id'] = param['logisticsCompany_id']
        express['mouth_id'] = param['mouth_id']
        express['operator_id'] = param['operator_id']
        express['storeUser_id'] = param['storeUser_id']
        express['isExtend'] = False

        if dataExtend != "":
            express['transactionRecord'] = dataExtend['transactionRecord']
            express['extendTimes'] = dataExtend['extendTimes']
            express['paymentMethod'] = dataExtend['paymentMethod']
            express['overdueTime'] = dataExtend['overdueTime']
            express['isExtend'] = dataExtend['isExtend']
            express['duration'] = dataExtend['duration']
            express['minutes_duration'] = dataExtend['minutes_duration']
            express['cost_overdue'] = dataExtend['cost_overdue']
            express['timestamp_duration'] = dataExtend['timestamp_duration']

    except Exception as e:
        _LOG_.warning(("Take_express additional params ERROR : ", e))
        
    if AP_EXPRESS is None:
        mouth_list = BoxDao.get_mouth_by_id({'id':mouth_param['id']})
        if len(mouth_list) != 1:
            _LOG_.debug(('mouth list error!', str(mouth_list)))
            return
        _mouth = mouth_list[0]        
        door = {
            "door_no": _mouth['number'],
            "phone_number": param['takeUserPhoneNumber']
        }
        _LOG_.debug(('door_number!', door))
        _EXPRESS_.customer_take_express_signal.emit('Success||undefined||'+ json.dumps(door))
    else:
        express_data = json.dumps(AP_EXPRESS)
        _EXPRESS_.customer_take_express_signal.emit('Success||'+express_data)
    if overdue:
        record_list = ExpressDao.get_record({'express_id': express['id']})
        if len(record_list) == 1:
            record = record_list[0]
            record_param = {'id': record['id'],
                            'createTime': record['createTime'],
                            'amount': record['amount']}
            express['transactionRecords'] = [record_param]
    _LOG_.warning(("[DATA_TAKEN_EXP] : ", express))
    dataExtend = json.dumps(express)
    paramExtend = {
        'dataExtend' : json.dumps(dataExtend),
        'validateCode' : express['validateCode']
    }
    ExpressDao.update_data_extend(paramExtend)
    result, status_code = HttpClient.post_message('express/customerTakeExpress', express)
    if status_code == 200 and result['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


overdue_express = None


def pre_take_overdue_express(express_param):
    global overdue_express
    global paid_amount
    global overdue_cost
    global AP_EXPRESS
    global AP_OVERDUE
    overdue_express = express_param
    _LOG_.info(("AP_EXPRESS_DEBUG : ", AP_EXPRESS))
    try:
        mouth = BoxDao.get_mouth_by_id({'id':overdue_express['mouth_id']})
        _LOG_.info(("OVERDUE_DATALOCKER_MOUTH : ", mouth))
        mouthTypeId = mouth[0]['mouthType_id']
        mouth_param = BoxDao.get_mouth_type_name({'id': mouthTypeId})
        dataLocker = {
            'name': mouth_param['name'],
            'deleteFlag': 0
        }
        mouth_param = box.service.BoxService.get_overdue_price(dataLocker)
        _LOG_.info(("OVERDUE_DATALOCKER : ", dataLocker))

        time_span = ClientTools.now() - express_param['overdueTime']
        day_span = math.ceil(time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
        overdue_cost = day_span * mouth_param['defaultOverduePrice']
        time_now = datetime.datetime.now().timestamp()
        times = int(express_param['overdueTime']) / 1000
        slice_time = slice(10)
        epoch_timeduration = time_now
        time_difference = math.trunc((time_now - int(times)) / 3600)
        minutes_difference = round((time_now - times) / 60) - 60 * time_difference
        day_span_temp = (time_span / 1000.0 / 60.0 / 60.0 / overdueTime)
        _LOG_.info(("OVERDUE_PRICE : ", mouth_param['defaultOverduePrice'], day_span))

        # record_param = {'express_id': overdue_express['id']}
        # record_list = ExpressDao.get_record(record_param)
        # if len(record_list) == 0:
        #     record_param = {'id': ClientTools.get_uuid(),
        #                     'paymentType': 'CASH',
        #                     'transactionType': 'PAYMENT_FOR_OVERDUE',
        #                     'amount': 0,
        #                     'express_id': overdue_express['id'],
        #                     'createTime': ClientTools.now(),
        #                     'mouth_id': overdue_express['mouth_id']}
        #     ExpressDao.save_record(record_param)
        #     paid_amount = 0
        # else:
        #     record = record_list[0]
        #     paid_amount = math.ceil(record['amount'] * -1)
        _LOG_.debug(('AP_EXPRESS!',AP_EXPRESS))

        if AP_EXPRESS is None:
            mouth_list = BoxDao.get_mouth_by_id({'id':overdue_express['mouth_id']})
            if len(mouth_list) != 1:
                _LOG_.debug(('mouth list error!', str(mouth_list)))
                return
            _mouth = mouth_list[0]        
            door = {
                "door_no": _mouth['number'],
                "phone_number": overdue_express['takeUserPhoneNumber']
            }
            data_json = {
                "cost_overdue":  str(overdue_cost),
                "extend_overdue": str(day_span),
                "parcel_duration": ( time_difference ),
                "timestamp_duration": ( epoch_timeduration ),
                "date_overdue": express_param['overdueTime'],
            }
            data_trx = {
                "door_no": _mouth['number'],
                "pin_code": overdue_express['validateCode'],
                "express": overdue_express['expressNumber']
            }
            data_over = {}
            data_over_list = []
            data_over['overdue_data'] = json.dumps(data_json)
            data_over['paymentType'] =  overdue_express['validateCode']
            data_over['transactionType'] = json.dumps(data_trx)

            _LOG_.debug(('door_number!', door))
            if overdue_express['expressNumber'][:4] != 'PDSL':
                if overdue_express['expressNumber'][:3] != 'PDS':
                    _EXPRESS_.customer_take_express_signal.emit('Overdue||'+ json.dumps(data_over) +'||' + json.dumps(door))
                else:
                    _EXPRESS_.customer_take_express_signal.emit('Overdue||Popsafe')
            else:
                # Get Details Mouth
                used_mouth = BoxDao.get_detail_mouth_by_id({"id": overdue_express['mouth_id']})
                overdue_express['lockerNo'] = str(used_mouth[0]["number"])
                overdue_express['lockerSize'] = used_mouth[0]["name"]
                used_locker = BoxDao.get_box_by_box_id({"id": overdue_express['box_id'], "deleteFlag": 0})
                overdue_express['lockerName'] = used_locker[0]["name"]  
                overdue_data = json.dumps(overdue_express)
                _LOG_.debug(("overdue_express : ", overdue_express))
                _EXPRESS_.customer_take_express_signal.emit('PopDeposit_Extend||'+str(overdue_data))
        else:
            data_json = {}
            data_json['cost_overdue'] = str(overdue_cost)
            data_json['extend_overdue'] = str(day_span)
            data_json['parcel_duration'] = ( time_difference )
            data_json['parcel_minutes_duration'] = ( minutes_difference )
            data_json['timestamp_duration'] = ( epoch_timeduration )
            data_json['date_overdue'] =  express_param['overdueTime']
            data_over = json.dumps(data_json)

            AP_EXPRESS['overdue_data'] = data_over
            overdue_data = json.dumps(AP_EXPRESS)
            AP_EXPRESS = None
            _EXPRESS_.customer_take_express_signal.emit('Overdue||'+overdue_data)
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] customer_take_express_signal : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))
        _EXPRESS_.customer_take_express_signal.emit('Error')
        _LOG_.warning((str(e), overdue_express))



paid_amount = 0
coin_machine_connect_flag = False


def take_overdue_express():
    pass


def get_overdue_cost():
    _EXPRESS_.overdue_cost_signal.emit(str(math.ceil(overdue_cost / 100)))


scanner_signal_connect_flag = False


def start_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if not scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(get_express_number_by_barcode)
        scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


express_id = ''
imported_express = None


def get_express_number_by_barcode(text):
    global imported_express
    global express_id
    if text == '' or text == 'ERROR':
        return
    _EXPRESS_.barcode_signal.emit(text)
    express_message, status_code = HttpClient.get_message('express/imported/' + text)
    if status_code == 200:
        if "takeUserPhoneNumber" in str(express_message):
            imported_express = express_message
            express_id = imported_express['id']
            _EXPRESS_.phone_number_signal.emit(imported_express['takeUserPhoneNumber'])
        else:
            _EXPRESS_.imported_express_result_signal.emit('no_imported')
            imported_express = None
            express_id = ''
    else:
        imported_express = None
        express_id = ''


def stop_get_express_number_by_barcode():
    global scanner_signal_connect_flag
    if scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.disconnect(get_express_number_by_barcode)
        scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()


def start_get_imported_express(text):
    get_express_number_by_text(text)


def get_express_number_by_text(text):
    global imported_express
    global express_id
    if text == '' or text == 'ERROR':
        return
    express_message, status_code = HttpClient.get_message('express/imported/' + text)
    if status_code == 200:
        if "takeUserPhoneNumber" in str(express_message):
            imported_express = express_message
            express_id = imported_express['id']
            _EXPRESS_.imported_express_result_signal.emit(imported_express['takeUserPhoneNumber'])
        else:
            _EXPRESS_.imported_express_result_signal.emit('no_imported')
            imported_express = None
            express_id = ''
    else:
        _EXPRESS_.imported_express_result_signal.emit('no_imported')
        imported_express = None
        express_id = ''


express_number = ''


def set_express_number(express_number__):
    global express_number
    express_number = express_number__


phone_number = ''


def set_phone_number(phone_number__):
    global phone_number
    phone_number = phone_number__


def start_store_express():
    ClientTools.get_global_pool().apply_async(store_express)


def store_express():
    global imported_express
    global express_id
    try:
        if len(express_number) == 0 or express_number == "":
            _EXPRESS_.store_express_signal.emit('Error')
            _LOG_.warning('[ERROR] store_express express_number is null')
            return
        mouth_result = box.service.BoxService.mouth
        _LOG_.debug(('mouth_result:', mouth_result))
        user_result = UserService.get_user()
        box_result = box.service.BoxService.get_box()
        if not box_result:
            _EXPRESS_.store_express_signal.emit('Error')
            _LOG_.warning('[ERROR] store_express box_result is null')
            return
        overdue_time = get_overdue_timestamp(box_result)
        # operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
        # if not operator_result:
        #     _EXPRESS_.store_express_signal.emit('Error')
        #     _LOG_.warning('[ERROR] store_express operator_result is null')
        #     return
        express_param = {'expressNumber': express_number,
                         'expressType': 'COURIER_STORE',
                         'overdueTime': overdue_time,
                         'status': 'IN_STORE',
                         'storeTime': ClientTools.now(),
                         'syncFlag': 0,
                         'takeUserPhoneNumber': phone_number,
                         'validateCode': random_validate(box_result['validateType']),
                         'version': 0,
                         'box_id': box_result['id'],
                         'logisticsCompany_id': user_result['company']['id'],
                         'mouth_id': mouth_result['id'],
                         'operator_id': box_result['operator_id'],
                         'storeUser_id': user_result['id']}
        if ClientTools.get_value('groupName', imported_express) is not None:
            express_param['groupName'] = imported_express['groupName']
        else:
            express_param['groupName'] = "UNDEFINED"
        if express_id == '':
            express_param['id'] = ClientTools.get_uuid()
        else:
            express_param['id'] = express_id
        # ========================================
        while True:
            _LOG_.info(('[ATTEMPT] store express data to DB', express_number))
            store_express_db = ExpressDao.save_express(express_param)
            if store_express_db is True:
                _LOG_.info(('[SUCCESS] store express data to DB', express_number))
                break
        # ========================================
        mouth_param = {'id': mouth_result['id'],
                       'express_id': express_param['id'],
                       'status': 'USED'}
        box.service.BoxService.use_mouth(mouth_param)
        express_param['box'] = {'id': express_param['box_id']}
        express_param.pop('box_id')
        express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
        express_param.pop('logisticsCompany_id')
        express_param['mouth'] = {'id': express_param['mouth_id']}
        express_param.pop('mouth_id')
        express_param['operator'] = {'id': express_param['operator_id']}
        express_param.pop('operator_id')
        express_param['storeUser'] = {'id': express_param['storeUser_id']}
        express_param.pop('storeUser_id')
        express_id = ''
        imported_express = None
        Camera.start_video_capture('courier_finish_drop_'+express_number)
        _LOG_.debug(('[DEBUG] system_capture', 'courier_finish_drop_'+express_number))
        _EXPRESS_.store_express_signal.emit('Success')
        message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
        if status_code == 200 and message['id'] == express_param['id']:
            ExpressDao.mark_sync_success(express_param)
    except Exception as e:
        _EXPRESS_.store_express_signal.emit('Error')
        _LOG_.warning(('[ERROR] store_express', str(e)))


def get_overdue_timestamp(box_result):
    try:
        start_time = datetime.datetime.now()
        overdue_flag = ClientTools.get_value('overdueFlag', imported_express, default_value=None)
        if overdue_flag is None:
            overdue_type = box_result['overdueType']
            if overdue_type == 'HOUR':
                overdue_time = box_result['freeHours']
            else:
                overdue_time = box_result['freeDays']
        else:
            overdue_type = overdue_flag.split(':')[0]
            overdue_time = overdue_flag.split(':')[1]
        if overdue_type == 'HOUR':
            end_time = start_time + datetime.timedelta(hours=int(overdue_time))
        else:
            end_time = (start_time + datetime.timedelta(days=int(overdue_time))).replace(hour=23, minute=0, second=0)
        return int(time.mktime(time.strptime(end_time.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')) * 1000)
    except Exception as e:
        _LOG_.debug(('get_overdue_timestamp', imported_express, str(e)))


def random_validate(validate_type, chars='ABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    while True:
        if validate_type == 'LETTER_AND_NUMBER_VALIDATE_CODE':
            validate_code = ''
            i = 0
            while i < 6:
                validate_code += random.choice(chars)
                i += 1
            if len(ExpressDao.get_express_by_validate({'validateCode': validate_code, 'status': 'IN_STORE'})) == 0:
                return validate_code
        else:
            return ''


customer_store_number = ''
customer_store_express = ''


def start_get_customer_store_express_info(text):
    global customer_store_number
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)


def get_customer_store_express_info():
    global customer_store_express
    message, status_code = HttpClient.get_message('express/customerExpress/' + customer_store_number)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            _EXPRESS_.customer_store_express_signal.emit('False')
            return
        if ClientTools.get_value("status", message) == "IMPORTED":
            customer_store_express = message
            _EXPRESS_.customer_store_express_signal.emit(str(json.dumps(customer_store_express)))
        else:
            _EXPRESS_.customer_store_express_signal.emit('False')
            return
    else:
        _EXPRESS_.customer_store_express_signal.emit('False')


customer_reject_number = ''
customer_reject_express = ''


def start_get_customer_reject_express_info(text):
    global customer_reject_number
    customer_reject_number = text
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_info)


def get_customer_reject_express_info():
    global customer_reject_express
    message, status_code = HttpClient.get_message('express/rejectExpress/' + customer_reject_number)
    if status_code == 200:
        customer_reject_express = message
        _EXPRESS_.customer_reject_express_signal.emit(str(json.dumps(customer_reject_express)))
    else:
        _EXPRESS_.customer_reject_express_signal.emit('False')


customer_scanner_signal_connect_flag = False


def start_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(customer_scan_qr_code)
        customer_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_customer_scan_qr_code():
    global customer_scanner_signal_connect_flag
    if not customer_scanner_signal_connect_flag:
        return
    customer_scanner_signal_connect_flag = False
    Scanner._SCANNER_.barcode_result.disconnect(customer_scan_qr_code)
    Scanner.start_stop_scanner()


def customer_scan_qr_code(text):
    global customer_scanner_signal_connect_flag
    global customer_store_number
    if text == '' or text == 'ERROR':
        customer_scanner_signal_connect_flag = False
        return
    _EXPRESS_.barcode_signal.emit(text)
    customer_store_number = text
    ClientTools.get_global_pool().apply_async(get_customer_store_express_info)
    customer_scanner_signal_connect_flag = False


def start_load_courier_overdue_express_count():
    ClientTools.get_global_pool().apply_async(load_courier_overdue_express_count)


def load_courier_overdue_express_count():
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'COURIER_STORE'}
    if __user['company']['level'] == 1:
        param.pop('logisticsCompany_id')
        count_list = ExpressDao.get_overdue_express_count_by_manager(param)
    else:
        count_list = ExpressDao.get_overdue_express_count_by_logistics_id(param)
    _EXPRESS_.overdue_express_count_signal.emit(count_list[0]['count'])

def start_operator_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(operator_load_overdue_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')

def operator_load_overdue_express(page):
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'COURIER_STORE'}
    if __user['company']['level'] == 1:
        param.pop('logisticsCompany_id')
        overdue_express_list_ = ExpressDao.get_overdue_express_by_operator(param)
    else:
        overdue_express_list_ = ExpressDao.get_overdue_express_by_logistics_id(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

    _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))


def start_courier_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(courier_load_overdue_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def courier_load_overdue_express(page):
    __user = UserService.get_user()
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'logisticsCompany_id': __user['company']['id'],
             'expressType': 'COURIER_STORE',
             'startLine': (int(page) - 1) * 5}
    if __user['company']['level'] == 1:
        param.pop('logisticsCompany_id')
        overdue_express_list_ = ExpressDao.get_overdue_express_by_manager(param)
    else:
        overdue_express_list_ = ExpressDao.get_overdue_express_by_logistics_id(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

    _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))


def start_load_manager_overdue_express_count():
    ClientTools.get_global_pool().apply_async(load_manager_overdue_express_count)


def load_manager_overdue_express_count():
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'expressType': 'COURIER_STORE'}
    count_list = ExpressDao.get_overdue_express_count_by_manager(param)
    _EXPRESS_.overdue_express_count_signal.emit(count_list[0]['count'])


def start_manager_load_overdue_express_list(page):
    ClientTools.get_global_pool().apply_async(manager_load_overdue_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def manager_load_overdue_express(page):
    param = {'status': 'IN_STORE',
             'overdueTime': ClientTools.now(),
             'expressType': 'COURIER_STORE',
             'startLine': (int(page) - 1) * 5}
    overdue_express_list_ = ExpressDao.get_overdue_express_by_manager(param)
    for overdue_express_ in overdue_express_list_:
        overdue_express_['mouth'] = box.service.BoxService.get_mouth(overdue_express_)

    _EXPRESS_.overdue_express_list_signal.emit(json.dumps(overdue_express_list_))


def start_staff_take_all_overdue_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_overdue_express)


def staff_take_all_overdue_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'overdueTime': ClientTools.now(),
                   'expressType': 'COURIER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_overdue_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN' or __user['company']['level'] == 1:
        __param = {'status': 'IN_STORE',
                   'overdueTime': ClientTools.now(),
                   'expressType': 'COURIER_STORE'}
        __express_list = ExpressDao.get_all_overdue_express_by_manager(__param)
    _LOG_.info('overdue_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_overdue_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_overdue_express(__user, __express)
        time.sleep(2)

    _EXPRESS_.staff_take_overdue_express_signal.emit('Success')


def start_staff_take_overdue_express_list(express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_overdue_express_list, (express_id_list,))


def staff_take_overdue_express_list(express_id_list):
    _LOG_.info(('[express_id_list_overdue_take]:', express_id_list))
    data_express_take = json.loads(express_id_list)
    __express_list = data_express_take['overdueTake']
    _LOG_.info(('[EXPRESS_LIST]', __express_list))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_overdue_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]
        _LOG_.info(('[USER_TAKE_OVERDUE]', __user))
        staff_take_overdue_express(__user, express)
        time.sleep(2)

    _EXPRESS_.staff_take_overdue_express_signal.emit('Success')


def staff_take_overdue_express(__user, express):
    
    try:
        express['takeTime'] = ClientTools.now()
        if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
            express['status'] = 'COURIER_TAKEN'
        if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
            express['status'] = 'OPERATOR_TAKEN'
        express['staffTakenUser_id'] = __user['id']
        express['version'] += 1
        express['syncFlag'] = 0
        ExpressDao.take_express(express)
        operator = json.dumps({
            "op_username": __user['loginName'],
            "op_name": __user['name']
        })
        data_operator = {
            'dataOperator': json.dumps(operator),
            'id': express['id']
        }
        ExpressDao.save_data_operator_taken(data_operator)
        _LOG_.info(('EXPRESS DAO : ', express))
        box.service.BoxService.free_mouth({'id': express['mouth_id']})
        box.service.BoxService.open_mouth(express['mouth_id'])
        express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
        express['dataOperator'] = json.dumps(operator)
        ClientTools.get_global_pool().apply_async(sync_staff_take_overdue_express, (express,))
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        _LOG_.warning(('[ERROR] staff_take_overdue_express : ','<errMessage>:', e, '<filename>:', fname, '<line>:', exc_tb.tb_lineno))


def sync_staff_take_overdue_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeOverdueExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def get_scan_not_sync_express_list():
    return ExpressDao.get_not_sync_express_list({'syncFlag': 0})


def mark_sync_success(express_):
    ExpressDao.mark_sync_success(express_)


calculate_customer_express_cost_flag = False


def stop_calculate_customer_express_cost():
    global calculate_customer_express_cost_flag
    if calculate_customer_express_cost_flag:
        calculate_customer_express_cost_flag = False


customer_express_weight = 0
customer_express_cost = 0


def calculate_customer_express_cost(weight):
    global customer_express_weight
    global customer_express_cost
    if not calculate_customer_express_cost_flag:
        return
    customer_express_weight = weight
    range_price_ = customer_store_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_express_cost = cost
    _EXPRESS_.customer_store_express_cost_signal.emit(json.dumps(cost_result_))


def start_pull_pre_pay_cash_for_customer_express(express_cost):
    global customer_express_cost
    customer_express_cost = express_cost


customer_paid_amount = 0
calculate_customer_reject_express_cost_flag = False


def stop_calculate_customer_reject_express_cost():
    global calculate_customer_reject_express_cost_flag
    if calculate_customer_reject_express_cost_flag:
        calculate_customer_reject_express_cost_flag = False


customer_reject_express_weight = 0
customer_reject_express_cost = 0


def calculate_customer_reject_express_cost(weight):
    global customer_reject_express_cost
    global customer_reject_express_weight
    if not calculate_customer_reject_express_cost_flag:
        return
    customer_reject_express_weight = weight
    range_price_ = customer_reject_express
    cost = 0
    cost += range_price_['firstPrice']
    continued_weight = weight - range_price_['firstHeavy']
    if continued_weight < 0:
        continued_weight = 0
    cost += math.ceil(continued_weight / range_price_['continuedHeavy']) * range_price_['continuedPrice']
    cost_result_ = {'heavy': weight, 'cost': cost}
    customer_reject_express_cost = cost
    _EXPRESS_.customer_store_express_cost_signal.emit(json.dumps(cost_result_))


def start_pull_pre_pay_cash_for_customer_reject_express(express_cost):
    global customer_reject_express_cost
    customer_reject_express_cost = express_cost


store_customer_express_flag = False


def start_store_customer_express():
    global store_customer_express_flag
    if not store_customer_express_flag:
        store_customer_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_express)


def store_customer_express():
    global store_customer_express_flag
    box_info = box.service.BoxService.get_box()
    customer_store_express['box_id'] = box_info['id']
    customer_store_express['logisticsCompany_id'] = customer_store_express['logisticsCompany']['id']
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': customer_store_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    customer_store_express['mouth'] = mouth_result
    customer_store_express['mouth_id'] = mouth_result['id']
    customer_store_express['operator_id'] = box_info['operator_id']
    customer_store_express['storeUser_id'] = "POPSEND"
    customer_store_express['weight'] = customer_express_weight
    customer_store_express['storeTime'] = ClientTools.now()
    customer_store_express['chargeType'] = customer_store_express['chargeType']
    ExpressDao.save_customer_express(customer_store_express)
    _EXPRESS_.store_customer_express_result_signal.emit('Success')
    record_list = ExpressDao.get_record({'express_id': customer_store_express['id']})
    if len(record_list) == 1:
        record = record_list[0]
        record_param = {'id': record['id'],
                        'createTime': record['createTime'],
                        'amount': record['amount']}
        customer_store_express['transactionRecords'] = [record_param]
    customer_store_express['box'] = {'id': customer_store_express['box_id']}
    _LOG_.debug(("customer_store_express : ", str(customer_store_express)))
    message, status_code = HttpClient.post_message('express/customerStoreExpress', customer_store_express)
    if status_code == 200 and message['id'] == customer_store_express['id']:
        ExpressDao.mark_sync_success(customer_store_express)
    store_customer_express_flag = False


store_customer_reject_express_flag = False


def start_store_customer_reject_express():
    global store_customer_reject_express_flag
    if not store_customer_reject_express_flag:
        store_customer_reject_express_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_express)


def store_customer_reject_express():
    global store_customer_reject_express_flag
    customer_reject_express['box_id'] = box.service.BoxService.get_box()['id']
    customer_reject_express['logisticsCompany_id'] = customer_reject_express['logisticsCompany']['id']
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': customer_reject_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    customer_reject_express['mouth'] = mouth_result
    customer_reject_express['mouth_id'] = mouth_result['id']
    customer_reject_express['operator_id'] = box.service.BoxService.get_box()['operator_id']
    customer_reject_express['storeUser_id'] = ClientTools.get_value('id', ClientTools.get_value('storeUser',
                                                                                                customer_reject_express,
                                                                                                {}))
    customer_reject_express['weight'] = customer_reject_express_weight
    customer_reject_express['endAddress'] = ClientTools.get_value('endAddress', customer_reject_express, None)
    customer_reject_express['startAddress'] = ClientTools.get_value('startAddress', customer_reject_express, None)
    customer_reject_express['recipientName'] = ClientTools.get_value('recipientName', customer_reject_express, None)
    customer_reject_express['recipientUserPhoneNumber'] = ClientTools.get_value('recipientUserPhoneNumber',
                                                                                customer_reject_express, None)
    customer_reject_express['storeTime'] = ClientTools.now()
    customer_reject_express['chargeType'] = customer_reject_express['chargeType']
    ExpressDao.save_customer_express(customer_reject_express)
    _EXPRESS_.store_customer_express_result_signal.emit('Success')
    record_list = ExpressDao.get_record({'express_id': customer_reject_express['id']})
    if len(record_list) == 1:
        record = record_list[0]
        record_param = {'id': record['id'],
                        'createTime': record['createTime'],
                        'amount': record['amount']}
        customer_reject_express['transactionRecords'] = [record_param]
    customer_reject_express['box'] = {'id': customer_reject_express['box_id']}
    message, status_code = HttpClient.post_message('express/customerRejectExpress', customer_reject_express)
    if status_code == 200 and message['id'] == customer_reject_express['id']:
        ExpressDao.mark_sync_success(customer_reject_express)
    store_customer_reject_express_flag = False


def start_get_customer_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_express_cost)


def get_customer_express_cost():
    _EXPRESS_.customer_express_cost_insert_coin_signal.emit(str(math.ceil(customer_express_cost / 100)))


def start_get_customer_reject_express_cost():
    ClientTools.get_global_pool().apply_async(get_customer_reject_express_cost)


def get_customer_reject_express_cost():
    _EXPRESS_.customer_express_cost_insert_coin_signal.emit(
        str(math.ceil(customer_reject_express_cost / 100)))


def get_scan_sync_express_transaction_record(param):
    return ExpressDao.get_record(param)


def start_load_customer_send_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_send_express_count)


def load_customer_send_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])
    elif __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])


def start_load_customer_reject_express_count():
    ClientTools.get_global_pool().apply_async(load_customer_reject_express_count)


def load_customer_reject_express_count():
    __user = UserService.get_user()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id']}
        count_list = ExpressDao.get_send_express_count_by_logistics_id(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT'}
        count_list = ExpressDao.get_send_express_count_by_operator(param)
        _EXPRESS_.send_express_count_signal.emit(count_list[0]['count'])


def get_send_amount(express__):
    return ExpressDao.get_mouth(express__)


def start_customer_load_send_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_send_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def customer_load_send_express(page):
    __user = UserService.get_user()
    _LOG_.debug(('customer_load_send_express __user is : ', __user))
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_STORE',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)

    _EXPRESS_.send_express_list_signal.emit(json.dumps(send_express_list_))


def start_customer_load_reject_express_list(page):
    ClientTools.get_global_pool().apply_async(customer_load_reject_express, (page,))
    _EXPRESS_.load_express_list_signal.emit('Success')


def customer_load_reject_express(page):
    __user = UserService.get_user()
    _LOG_.debug(('customer_load_reject_express __user is : ', __user))
    send_express_list_ = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'logisticsCompany_id': __user['company']['id'],
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_logistics_id(param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        param = {'status': 'IN_STORE',
                 'expressType': 'CUSTOMER_REJECT',
                 'startLine': (int(page) - 1) * 5}
        send_express_list_ = ExpressDao.get_send_express_by_operator(param)
    for send_express_ in send_express_list_:
        send_express_['mouth'] = box.service.BoxService.get_mouth(send_express_)
        send_express_['amount'] = get_send_amount(send_express_)
        send_express_['electronicCommerce'] = \
        CompanyDao.get_company_by_id({'id': send_express_['electronicCommerce_id']})[0]

    _EXPRESS_.reject_express_list_signal.emit(json.dumps(send_express_list_))


def start_staff_take_all_send_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_send_express)


def staff_take_all_send_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_STORE',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    _LOG_.info('send_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_send_express(__user, __express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def start_staff_take_send_express_list(send_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_send_express_list, (send_express_id_list,))


def staff_take_send_express_list(send_express_id_list):
    _LOG_.info(('express_id_list:', send_express_id_list))
    __express_list = json.loads(send_express_id_list)
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]
        staff_take_send_express(__user, express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def staff_take_send_express(__user, express):
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']
    express['version'] += 1
    express['syncFlag'] = 0
    ExpressDao.take_express(express)
    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_send_express, (express,))


def sync_staff_take_send_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserSendExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def start_staff_take_all_reject_express():
    ClientTools.get_global_pool().apply_async(staff_take_all_reject_express)


def staff_take_all_reject_express():
    __user = UserService.get_user()
    __express_list = []
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'logisticsCompany_id': __user['company']['id']}
        __express_list = ExpressDao.get_all_send_express_by_logistics_id(__param)
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        __param = {'status': 'IN_STORE',
                   'expressType': 'CUSTOMER_REJECT',
                   'overdueTime': ClientTools.now()}
        __express_list = ExpressDao.get_all_send_express_by_manager(__param)
    _LOG_.info('reject_express_count:' + str(len(__express_list)))
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    for __express in __express_list:
        staff_take_reject_express(__user, __express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def start_staff_take_reject_express_list(reject_express_id_list):
    ClientTools.get_global_pool().apply_async(staff_take_reject_express_list, (reject_express_id_list,))


def staff_take_reject_express_list(reject_express_id_list):
    _LOG_.info(('express_id_list REJECT :', reject_express_id_list))
    __express_list = json.loads(reject_express_id_list)
    if len(__express_list) == 0:
        _EXPRESS_.staff_take_send_express_signal.emit('None')
        return
    __user = UserService.get_user()
    for __express in __express_list:
        _LOG_.info(('__EXPRESS OKE : ', str(__express)))
        express_result_list = ExpressDao.get_express_by_id({'id': __express})
        express = express_result_list[0]

        _LOG_.info(('__EXPRESS OKE RESULT : ', str(express_result_list[0])))

        staff_take_reject_express(__user, express)

    _EXPRESS_.staff_take_send_express_signal.emit('Success')


def staff_take_reject_express(__user, express):
    _LOG_.info(('LIST USER :', str(__user)))
    _LOG_.info(('LIST EXPRESS :', str(express)))
    express['takeTime'] = ClientTools.now()
    if __user['role'] == 'LOGISTICS_COMPANY_USER' or __user['role'] == 'LOGISTICS_COMPANY_ADMIN':
        express['status'] = 'COURIER_TAKEN'
    if __user['role'] == 'OPERATOR_USER' or __user['role'] == 'OPERATOR_ADMIN':
        express['status'] = 'OPERATOR_TAKEN'
    express['staffTakenUser_id'] = __user['id']

    # _LOG_.info(('LIST EXPRESS staffTakenUser_id :', str(express['staffTakenUser_id'])))
 
    express['version'] += 1
    express['syncFlag'] = 0
    ExpressDao.take_express(express)

    _LOG_.info(('LIST EXPRESS mouth_id :', str(express['mouth_id'])))

    box.service.BoxService.free_mouth({'id': express['mouth_id']})
    box.service.BoxService.open_mouth(express['mouth_id'])
    express['staffTakenUser'] = {'id': express['staffTakenUser_id']}
    ClientTools.get_global_pool().apply_async(sync_staff_take_reject_express, (express,))


def sync_staff_take_reject_express(express):
    message, status_code = HttpClient.post_message('express/staffTakeUserRejectExpress', express)
    if status_code == 200 and message['id'] == express['id']:
        ExpressDao.mark_sync_success(express)


def start_service_pull_store_express(express_massage, timestamp=0):
    _LOG_.info(('Server_pull timestamp is ', timestamp))
    if ClientTools.now() - timestamp >= 30000:
        return
    service_pull_store_express(express_massage)


def service_pull_store_express(express_massage):
    _LOG_.info(('service_pull_store_express_info:', express_massage))
    param = {'id': express_massage['mouth']['box']['id'],
             'deleteFlag': 0}
    pull_box_info = BoxDao.get_box_by_box_id(param)
    if len(pull_box_info) != 1:
        # logger.debug('bad box_id')
        return False
    check_free_mouth_param = {'id': express_massage['mouth']['id'],
                              'status': 'ENABLE'}
    # logger.debug('start BoxDao')
    pull_mouth_result = BoxDao.get_free_mouth_by_id(check_free_mouth_param)
    if len(pull_mouth_result) != 1:
        # logger.debug('no free mouth')
        return False
    box.service.BoxService.pull_open_mouth(express_massage['mouth']['id'])
    # logger.debug('opened')
    express_param = {'id': express_massage['id'],
                     'expressNumber': express_massage['expressNumber'],
                     'expressType': express_massage['expressType'],
                     'overdueTime': express_massage['overdueTime'],
                     'status': express_massage['status'],
                     'storeTime': express_massage['storeTime'],
                     'syncFlag': 1, 'takeUserPhoneNumber': express_massage['takeUserPhoneNumber'],
                     'validateCode': express_massage['validateCode'],
                     'version': 0, 'box_id': express_massage['mouth']['box']['id'],
                     'logisticsCompany_id': express_massage['logisticsCompany']['id'],
                     'mouth_id': express_massage['mouth']['id'],
                     'operator_id': pull_box_info[0]['operator_id'],
                     'storeUser_id': express_massage['storeUser']['id'],
                     'groupName': ClientTools.get_value('groupName', express_massage)}
    _LOG_.debug(('express_param is :', express_param))
    ExpressDao.save_express(express_param)
    # logger.debug('save express end')
    mouth_param = {'id': express_massage['mouth']['id'],
                   'express_id': express_massage['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    # logger.debug('use_mouth end')
    return True


def start_reset_express(express_message):
    ClientTools.get_global_pool().apply_async(reset_express, (express_message,))


def reset_express(express_message):
    _LOG_.info(('reset_express_info:', express_message))
    param = {'id': express_message['box']['id'],
             'deleteFlag': 0}
    pull_box_info = BoxDao.get_box_by_box_id(param)
    if len(pull_box_info) != 1:
        reset_express_post_message(express_message['id'], 'ERROR BOX_ID', 'ERROR')
        return
    express_info_list = ExpressDao.get_express_by_id({'id': express_message['express']['id']})
    if len(express_info_list) != 1:
        reset_express_post_message(express_message['id'], 'NO SUCH EXPRESS', 'ERROR')
        return
    express_info = express_info_list[0]
    if express_message['express']['overdueTime'] <= express_message['express']['storeTime']:
        reset_express_post_message(express_message['id'], 'OVERDUE TIME ERROR', 'ERROR')
        return
    express_param = {'id': express_message['express']['id'],
                     'overdueTime': express_message['express']['overdueTime'],
                     'status': express_message['express']['status'],
                     'syncFlag': 1,
                     'takeUserPhoneNumber': express_message['express']['takeUserPhoneNumber'],
                     'validateCode': express_message['express']['validateCode'],
                     'version': express_info['version'] + 1}
    if express_info['status'] == 'IN_STORE':
        # logger.debug('the express in box')
        express_param['lastModifiedTime'] = ClientTools.now()
        ExpressDao.reset_express(express_param)
        reset_express_post_message(express_message['id'], 'IN_STORE EXPRESS RESET DONE', 'SUCCESS')
        return
    if express_info['status'] == 'CUSTOMER_TAKEN':
        # logger.debug('the express taken by customer')
        mouth_info_list = BoxDao.get_mouth_by_id({'id': express_message['express']['mouth']['id']})
        if len(mouth_info_list) != 1:
            reset_express_post_message(express_message['id'], 'MOUTH_ID ERROR', 'ERROR')
            return
        mouth_info = mouth_info_list[0]
        if mouth_info['status'] != 'ENABLE':
            reset_express_post_message(express_message['id'], 'MOUTH_STATUS ERROR', 'ERROR')
            return
        if mouth_info['status'] == 'ENABLE':
            mouth_param = {'status': 'USED',
                           'id': mouth_info['id'],
                           'express_id': express_message['express']['id']}
            BoxDao.use_mouth(mouth_param)
            express_param['lastModifiedTime'] = ClientTools.now()
            ExpressDao.reset_express(express_param)
            reset_express_post_message(express_message['id'], 'CUSTOMER_TAKEN EXPRESS DONE', 'SUCCESS')
            return
    if express_info['status'] == 'OPERATOR_TAKEN' or express_info['status'] == 'COURIER_TAKEN':
        # logger.debug('the express taken by staff')
        mouth_info_list = BoxDao.get_mouth_by_id({'id': express_message['express']['mouth']['id']})
        if len(mouth_info_list) != 1:
            reset_express_post_message(express_message['id'], 'MOUTH_ID ERROR', 'ERROR')
            return
        mouth_info = mouth_info_list[0]
        if mouth_info['status'] != 'ENABLE':
            reset_express_post_message(express_message['id'], 'MOUTH_STATUS ERROR', 'ERROR')
            return
        if mouth_info['status'] == 'ENABLE':
            mouth_param = {'status': 'USED',
                           'id': mouth_info['id'],
                           'express_id': express_message['express']['id']}
            BoxDao.use_mouth(mouth_param)
            express_param['lastModifiedTime'] = ClientTools.now()
            ExpressDao.reset_express(express_param)
            reset_express_post_message(express_message['id'], 'STAFF_TAKEN EXPRESS DONE', 'SUCCESS')
            return
    reset_express_post_message(express_message['id'], 'EXPRESS STATUS ERROR', 'ERROR')


def reset_express_post_message(task_id, result, reset_status):
    reset_express_result = {'id': task_id,
                            'result': result,
                            'statusType': reset_status}
    HttpClient.post_message('task/finish', reset_express_result)
    # logger.debug("reset_express result : ", str(reset_express_result))


reject_merchant_name = ''


def start_customer_reject_select_merchant(merchant_name):
    global reject_merchant_name
    reject_merchant_name = merchant_name


electronic_commerce_reject_number = ''
electronic_reject_express = ''


def start_customer_reject_for_electronic_commerce(barcode):
    global electronic_commerce_reject_number
    electronic_commerce_reject_number = barcode
    ClientTools.get_global_pool().apply_async(customer_reject_for_electronic_commerce)


def customer_reject_for_electronic_commerce():
    global electronic_reject_express
    message, status_code = HttpClient.get_message('express/reject/checkRule/' +
                                                  electronic_commerce_reject_number + '?type=' + reject_merchant_name)
    if status_code == 200:
        if ClientTools.get_value("statusCode", message) == 404:
            _EXPRESS_.customer_reject_express_signal.emit('False')
        else:
            electronic_reject_express = message
            electronic_reject_express['chargeType'] = 'NOT_CHARGE'
            electronic_reject_express['package_id'] = electronic_commerce_reject_number
            _EXPRESS_.customer_reject_express_signal.emit(str(json.dumps(electronic_reject_express)))
    else:
        _EXPRESS_.customer_reject_express_signal.emit('False')


def start_get_electronic_commerce_reject_express():
    ClientTools.get_global_pool().apply_async(get_electronic_commerce_reject_express)


def get_electronic_commerce_reject_express():
    global electronic_reject_express
    box_info = box.service.BoxService.get_box()
    electronic_reject_express['phone_number'] = phone_number
    electronic_reject_express['box_name'] = box_info['name']
    _EXPRESS_.reject_express_signal.emit(str(json.dumps(electronic_reject_express)))


store_customer_reject_for_electronic_commerce_flag = False


def start_store_customer_reject_for_electronic_commerce():
    global store_customer_reject_for_electronic_commerce_flag
    if not store_customer_reject_for_electronic_commerce_flag:
        store_customer_reject_for_electronic_commerce_flag = True
        ClientTools.get_global_pool().apply_async(store_customer_reject_for_ecommerce)


def store_customer_reject_for_ecommerce():
    global store_customer_reject_for_electronic_commerce_flag
    box_info = box.service.BoxService.get_box()
    reject_express = dict()
    if ClientTools.get_value('id', electronic_reject_express) is not None:
        reject_express['id'] = electronic_reject_express['id']
    else:
        reject_express['id'] = ClientTools.get_uuid()
    reject_express['box_id'] = box_info['id']
    reject_express['logisticsCompany_id'] = electronic_reject_express['logisticsCompany']['id']
    reject_express['groupName'] = ClientTools.get_value('groupName', electronic_reject_express,
                                                        reject_merchant_name)
    mouth_result = box.service.BoxService.mouth
    mouth_param = {'id': mouth_result['id'],
                   'express_id': reject_express['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    reject_express['mouth'] = mouth_result
    reject_express['mouth_id'] = mouth_result['id']
    reject_express['operator_id'] = box_info['operator_id']
    reject_express['storeUser_id'] = 'C_' + reject_express['id']
    reject_express['endAddress'] = reject_express['groupName'] + "_WAREHOUSE"
    reject_express['startAddress'] = 'PopBox @ ' + box_info['name']
    reject_express['recipientName'] = reject_express['groupName'] + "_CUSTOMER"
    reject_express['weight'] = 0
    if ClientTools.get_value('storeUserPhoneNumber', electronic_reject_express) is not None:
        reject_express['storeUserPhoneNumber'] = electronic_reject_express['storeUserPhoneNumber']
    else:
        reject_express['storeUserPhoneNumber'] = phone_number
    reject_express['storeTime'] = ClientTools.now()
    reject_express['chargeType'] = electronic_reject_express['chargeType']
    reject_express['customerStoreNumber'] = electronic_commerce_reject_number
    reject_express['expressType'] = 'CUSTOMER_REJECT'
    reject_express['electronicCommerce_id'] = electronic_reject_express['electronicCommerce']['id']
    ExpressDao.save_customer_reject_express(reject_express)
    company_list = CompanyDao.get_company_by_id({'id': reject_express['electronicCommerce_id']})
    electronic_commerce = {'id': electronic_reject_express['electronicCommerce']['id'],
                           'companyType': 'ELECTRONIC_COMMERCE',
                           'name': electronic_reject_express['electronicCommerce']['name'],
                           'deleteFlag': 0,
                           'parentCompany_id': ""}
    if len(company_list) == 0:
        CompanyDao.insert_company(electronic_commerce)
    else:
        CompanyDao.update_company(electronic_commerce)
    _EXPRESS_.store_customer_express_result_signal.emit('Success')
    reject_express['box'] = {'id': reject_express['box_id']}
    reject_express['logisticsCompany'] = electronic_reject_express['logisticsCompany']
    reject_express['electronicCommerce'] = electronic_reject_express['electronicCommerce']
    _LOG_.debug("reject_express : " + str(reject_express))
    message, status_code = HttpClient.post_message('express/rejectExpressNotImported', reject_express)
    if status_code == 200 and message['id'] == reject_express['id']:
        ExpressDao.mark_sync_success(reject_express)
    store_customer_reject_for_electronic_commerce_flag = False


def start_get_product_file():
    ClientTools.get_global_pool().apply_async(get_product_file)


def get_product_file():
    box_info = box.service.BoxService.get_box()
    _EXPRESS_.product_file_signal.emit(box_info['orderNo'])


other_service_scanner_signal_connect_flag = False


def start_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if not other_service_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.connect(get_express_info)
        other_service_scanner_signal_connect_flag = True
    Scanner.start_get_text_info()


def stop_get_express_info_by_barcode():
    global other_service_scanner_signal_connect_flag
    if other_service_scanner_signal_connect_flag:
        Scanner._SCANNER_.barcode_result.disconnect(get_express_info)
        other_service_scanner_signal_connect_flag = False
    Scanner.start_stop_scanner()


global_express_info = ''


def start_get_express_info(express_text):
    ClientTools.get_global_pool().apply_async(get_express_info, (express_text,))


def get_express_info(text):
    global pakpobox_order_number
    global global_express_info
    if text == '':
        return
    global_express_info = ''
    pakpobox_info = dict()
    pakpobox_info['token'] = Configurator.get_value('popbox', 'Token')
    start_url = Configurator.get_value('popbox', 'serveraddress')
    pakpobox_order_number = text
    pakpobox_info['order_number'] = text
    express_message, status_code = HttpClient.pakpobox_get_message(start_url, pakpobox_info)
    if status_code == 200:
        if express_message['response']['message'] == 'AVAILABLE':
            if len(express_message['data'][0]) != 0:
                _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                global_express_info = express_message['data'][0]
                QP3000S.price = express_message['data'][0]['order_amount']
                _EXPRESS_.start_get_express_info_result_signal.emit('success')
            else:
                _EXPRESS_.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'NOT AVAILABLE':
            _EXPRESS_.start_get_express_info_result_signal.emit('Error')
        elif express_message['response']['message'] == 'PAID':
            if len(express_message['data'][0]) != 0:
                _LOG_.debug(('get_express_info express_message: ', express_message['data']))
                global_express_info = express_message['data'][0]
                _EXPRESS_.start_get_express_info_result_signal.emit('PAID')
            else:
                _EXPRESS_.start_get_express_info_result_signal.emit('Error')
    else:
        _EXPRESS_.start_get_express_info_result_signal.emit('Error')


def start_get_pakpobox_express_info():
    ClientTools.get_global_pool().apply_async(get_pakpobox_express_info)


def get_pakpobox_express_info():
    _EXPRESS_.start_get_pakpobox_express_info_signal.emit(json.dumps(global_express_info))


def start_get_customer_info_by_card():
    ClientTools.get_global_pool().apply_async(get_customer_info_by_card)


def get_customer_info_by_card():
    global card_info
    _LOG_.debug('start_get_customer_info_by_card')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_get_customer_info_by_card flag: ', flag))
    if flag:
        card_info = QP3000S.balanceInfo()
        _LOG_.debug(('start_get_customer_info_by_card customer_card_amount: ', card_info))
    else:
        card_info = '0'
    _EXPRESS_.start_get_customer_info_by_card_signal.emit(int(card_info))


def start_payment_by_card():
    ClientTools.get_global_pool().apply_async(payment_by_card)


def payment_by_card():
    global time_stamp
    payment_result_info = dict()
    _LOG_.debug('start_payment_by_card')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_get_card_info flag: ', flag))
    if flag:
        payment_info_flag = QP3000S.purcDeb()
        _LOG_.debug(('start_get_card_info : ', payment_info_flag))
        if payment_info_flag == '0000':
            payment_info = QP3000S.getReport()
            _LOG_.debug(('start_get_card_info getReport result : ', payment_info))
            payment_result_info['last_balance'] = payment_info[32:40].lstrip('0')
            payment_result_info['card_no'] = payment_info[4:20]
            time_stamp = int(time.time())
            payment_result_info['show_date'] = time_stamp * 1000
            payment_result_info['terminal_id'] = Configurator.get_value('popbox', 'terminalID')
            box_info = box.service.BoxService.get_box()
            payment_result_info['locker'] = box_info['name']
            _EXPRESS_.start_payment_by_card_signal.emit(json.dumps(payment_result_info))
        else:
            _EXPRESS_.start_payment_by_card_signal.emit('ERROR')
    else:
        _EXPRESS_.start_payment_by_card_signal.emit('ERROR')


def start_get_cod_status():
    ClientTools.get_global_pool().apply_async(get_cod_status)


def get_cod_status():
    cod_status = Configurator.get_value('COP', 'status')
    _EXPRESS_.start_get_cod_status_signal.emit(cod_status)


def start_finish_payment_operate():
    ClientTools.get_global_pool().apply_async(finish_payment_operate)


def finish_payment_operate():
    pakpobox_finish_info = dict()
    box_result = box.service.BoxService.get_box()
    _LOG_.debug('start_finish_payment_operate')
    flag = QP3000S.init_serial(x=1)
    _LOG_.debug(('start_finish_payment_operate flag: ', flag))
    if flag:
        settlement = QP3000S.settlement()
        _LOG_.debug(('start_finish_payment_operate settlement: ', settlement))
        pakpobox_finish_info['settle_code'] = settlement
        pakpobox_finish_info['order_number'] = pakpobox_order_number
        pakpobox_finish_info['token'] = Configurator.get_value('popbox', 'Token')
        pakpobox_finish_info['order_amount'] = int(global_express_info['order_amount'])
        time_array = time.localtime(time_stamp)
        other_style_time = time.strftime('%Y-%m-%d %H:%M:%S', time_array)
        pakpobox_finish_info['settle_timestamp'] = other_style_time
        pakpobox_finish_info['settle_place'] = box_result['name']
        _LOG_.debug(('start_finish_payment_operate info: ', pakpobox_finish_info))
        start_retry_push_customer_info(pakpobox_finish_info)


def start_retry_push_customer_info(customer_info):
    ClientTools.get_global_pool().apply_async(retry_push_customer_info, (customer_info,))


def retry_push_customer_info(customer_info):
    end_url = Configurator.get_value('popbox', 'endserveraddress')
    while True:
        express_message, status_code = HttpClient.pakpobox_get_message(end_url, customer_info)
        if status_code == 200 and express_message['response']['message'] == 'SUCCESS':
            return
        time.sleep(2)


def start_deposit_express():
    ClientTools.get_global_pool().apply_async(store_deposit_express)


popdeposit_express = customer_store_express


def store_deposit_express():
    mouth_result = box.service.BoxService.mouth
    _LOG_.debug(('mouth_result:', mouth_result))
    box_result = box.service.BoxService.get_box()
    if not box_result:
        _EXPRESS_.store_express_signal.emit('Error')
        return
    overdue_time = get_overdue_timestamp(box_result)
    operator_result = CompanyService.get_company_by_id(box_result['operator_id'])
    if not operator_result:
        _EXPRESS_.store_express_signal.emit('Error')
        return
    express_param = {'expressNumber': popdeposit_express['customerStoreNumber'],
                     'expressType': 'COURIER_STORE',
                     'overdueTime': overdue_time,
                     'status': 'IN_STORE',
                     'storeTime': ClientTools.now(),
                     'syncFlag': 0,
                     'takeUserPhoneNumber': popdeposit_express['takeUserPhoneNumber'],
                     'validateCode': random_validate(box_result['validateType']),
                     'version': 1,
                     'box_id': box_result['id'],
                     'logisticsCompany_id': popdeposit_express['logisticsCompany']['id'],
                     'mouth_id': mouth_result['id'],
                     'operator_id': box_result['operator_id'],
                     'storeUser_id': '402880825dbcd4c3015de54d98c5518e',
                     'groupName': 'POPDEPOSIT',
                     'id': popdeposit_express['id']}
    ExpressDao.save_express(express_param)
    mouth_param = {'id': express_param['mouth_id'],
                   'express_id': express_param['id'],
                   'status': 'USED'}
    box.service.BoxService.use_mouth(mouth_param)
    express_param['box'] = {'id': express_param['box_id']}
    express_param.pop('box_id')
    express_param['logisticsCompany'] = {'id': express_param['logisticsCompany_id']}
    express_param.pop('logisticsCompany_id')
    express_param['mouth'] = {'id': express_param['mouth_id']}
    express_param.pop('mouth_id')
    express_param['operator'] = {'id': express_param['operator_id']}
    express_param.pop('operator_id')
    express_param['storeUser'] = {'id': express_param['storeUser_id']}
    express_param.pop('storeUser_id')
    _EXPRESS_.store_express_signal.emit('Success')
    logging.info('POPDEPOSIT express stored : ' + str(json.dumps(express_param)))
    message, status_code = HttpClient.post_message('express/staffStoreExpress', express_param)
    if status_code == 200 and message['id'] == express_param['id']:
        ExpressDao.mark_sync_success(express_param)

# def start_load_user_local(user_info):
#     ClientTools.get_global_pool().apply_async(load_user_local_count, (user_info,))

# def load_user_local_count(user_info):
#     user_list = user_info
#     param = {'username': user_list['username'],
#              'userpassword': user_list['password']}
#     count_list = ExpressDao.local_user_get(param)
#     _EXPRESS_.local_user_get_signal.emit(count_list[0]['count'])


def is_json(myjson):
  try:
    json_object = json.loads(myjson)
    try:
        int(json_object)
        return False
    except Exception as e:
        return True
  except ValueError as e:
    return False
  return True