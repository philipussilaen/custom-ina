import logging
from multiprocessing.dummy import Pool
import uuid
import Configurator
import random
import socket
import struct
import sys
import time
import datetime
import win32api
import win32file

__author__ = 'wahyudi@popbox.asia'
_LOG_ = logging.getLogger()
pool = Pool(processes=16)


def get_global_pool():
    return pool


def get_port_value(device_name, default_baud_rate, default_port, default_timeout=1):
    if Configurator.get_value(device_name, 'baudrate'):
        baudrate = int(Configurator.get_value(device_name, 'baudrate'))
    else:
        baudrate = default_baud_rate
    if Configurator.get_value(device_name, 'port'):
        port = Configurator.get_value(device_name, 'port')
    else:
        port = default_port
    if Configurator.get_value(device_name, 'timeout'):
        timeout = int(Configurator.get_value(device_name, 'timeout'))
    else:
        timeout = default_timeout
    __result = {'baudrate': baudrate, 'port': port, 'timeout': timeout}
    _LOG_.debug((device_name, __result))
    return __result


def now():
    return int(time.time()) * 1000


def today():
    now_time = time.time()
    midnight = now_time - now_time % 86400 + time.timezone
    return int(midnight) * 1000


def today_time():
    now_time = time.time()
    midnight = now_time - now_time % 86400 + time.timezone
    return int(midnight)


def time_str(f='%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.now().strftime(f)


def get_uuid():
    return str(uuid.uuid1().hex)


def get_value(key, map_, default_value=None):
    if map_ is None:
        return default_value
    if key in map_.keys():
        return map_[key]
    return default_value


def get_random_chars(length=3, chars='ABCDEFGHJKMNPQRSTUVWXYZ'):
    random_ = ''
    i = 0
    while i < length:
        random_ += random.choice(chars)
        i += 1
    return random_


ntp_servers = ['ntp.iitb.ac.in', 'time.nist.gov', 'time.windows.com', 'pool.ntp.org']


def sync_ntp(addr='time.nist.gov'):
    # http://code.activestate.com/recipes/117211-simple-very-sntp-client/
    TIME1970 = 2208988800      # Thanks to F.Lundh
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    data = '\x1b' + 47 * '\0'
    try:
        # Timing out the connection after 5 seconds, if no response received
        client.settimeout(5.0)
        client.sendto(bytes(data, "utf-8"), (addr, 123))
        data, address = client.recvfrom(1024)
        if data:
            epoch_time = struct.unpack('!12I', data)[10]
            epoch_time -= TIME1970
            return epoch_time
    except socket.timeout:
        return None


def reset_system_time_date():
    for server in ntp_servers:
        epoch_time = sync_ntp(server)
        if epoch_time is not None:
            utcTime = datetime.datetime.utcfromtimestamp(epoch_time)
            win32api.SetSystemTime(utcTime.year,
                                   utcTime.month,
                                   utcTime.weekday(),
                                   utcTime.day,
                                   utcTime.hour,
                                   utcTime.minute,
                                   utcTime.second,
                                   0)
            localTime = datetime.datetime.fromtimestamp(epoch_time)
            print("Time updated to: " + localTime.strftime("%Y-%m-%d %H:%M") + " from " + server)
            _LOG_.info("Time updated to: " + localTime.strftime("%Y-%m-%d %H:%M") + " from " + server)
            break
        else:
            print("Could not find time from " + server)
            _LOG_.debug("Could not find time from " + server)


BUFFER = 1024


def handle_file(mode="r/w", param=None, path=None):
    if path is None:
        _LOG_.warning('Missing Path file to handle')
        return -1, 'Missing Path File'
    # path = os.path.join(win32api.GetTempPath(), path)
    # print(path)
    try:
        if param is not None and mode == "w":
            param = param.replace('|', '\r\n')
            _handle = win32file.CreateFile(
                path,
                win32file.GENERIC_WRITE,
                win32file.FILE_SHARE_READ |
                win32file.FILE_SHARE_WRITE,
                None,
                win32file.OPEN_ALWAYS,
                0,
                None)
            try:
                response, data = win32file.WriteFile(_handle, param.encode(), None)
                # LOGGER.debug(('[DEBUG] writing to file with handle : ', response, data))
                return response, data
            except Exception as e:
                _LOG_.warning((path, e))
                return -1, str(e)[:50]
            finally:
                _handle.close()
                # os.unlink(path)

        if mode == "r":
            _handle = win32file.CreateFile(
                path,
                win32file.GENERIC_READ,
                win32file.FILE_SHARE_READ |
                win32file.FILE_SHARE_WRITE,
                None,
                win32file.OPEN_ALWAYS,
                0,
                None)
            try:
                response, data = win32file.ReadFile(_handle, BUFFER, None)
                # LOGGER.debug(('[DEBUG] reading to file with handle : ', response, data))
                return response, data.decode('utf-8')
            except Exception as e:
                _LOG_.warning((path, e))
                return -1, str(e)[:50]
            finally:
                _handle.close()
                # os.unlink(path)
    except Exception as e:
        _LOG_.warning(e)
        return -1, str(e)[:50]
