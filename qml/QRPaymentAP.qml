import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    width: 1024
    height: 768
    property int timer_value: 120
    property var press: "0"
    property var products: ""
    property var provider: ""
    property var global_trans: ""
    property url app_url: ""
    property bool small_logo_vis: false
    property var prod_name: "---"
    property var amount: "999000"
    property bool paymentSuccess: false
    property var prod_trans_id: "undefined"
    property var prod_valid_time: "1970-12-12 00:00:00"
    property var prod_pay_id: ""
    property var phone_number: ""
    property var product_class: ""
    property var paymentResult: undefined
    property var popsafe_param: undefined
    property var duration: undefined
    property bool testingMode: false
    property bool isExtending: false
    property var extend_pincode: undefined
    property var door_size: (testingMode==true) ? 'M' : 'L'
    property bool process_signal: true
    property var tratransactionRecord: ""
    property var paymentMethod: ""
    property var paymentAmount: ""
    property var paymentParam: ""
    property var lockerSize: ""
    property var imageLangkah: undefined
    property var extendData: undefined
    property bool paymentCancel : false


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value;
            my_timer.restart();
            press = "0";
            loadingPopUp.open();
            main_page.enabled = false;
            process_signal = true;
            define_provider(provider);

            if (isExtending == true) {
                step_progress.source = imageLangkah
            }

        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
        root.check_trans_global_result.connect(trans_global_result);
        root.choose_mouth_result.connect(select_door_result);
        root.apexpress_result.connect(next_process);
        root.update_apexpress_result.connect(process_update);
        root.customer_take_express_result.connect(process_result);
    }

    Component.onDestruction: {
        root.check_trans_global_result.disconnect(trans_global_result);
        root.choose_mouth_result.disconnect(select_door_result);
        root.apexpress_result.disconnect(next_process);
        root.update_apexpress_result.disconnect(process_update);
        root.customer_take_express_result.disconnect(process_result);
    }

    function process_result(text){
        console.log('process_result : ' + text)
        if (text=='Error'||text=='NotInput'){
            failure_notif.titleText = qsTr('Parcel Tidak Ditemukan');
            failure_notif.notifText = qsTr('Mohon Maaf, Silakan Masukkan Kode Pin Yang Benar.');
            failure_notif.open();
            return;
        }
        var status = text.split('||')[0];
        var info = text.split('||')[1];
        var detail = JSON.parse(info);
        var d = JSON.parse(detail.transactionType)
//        if (status=='Overdue'){
//            my_stack_view.push(select_payment_ap, {phone_number: detail.phone_number, isExtending: true, extendData: info});
//        }
        if (status=='Success'){
            my_stack_view.push(take_parcel_ap, {door_no: d.door_no});
            return;
        }
    }

    function process_update(u){
        console.log('process_update from trial ', u);
        if (u=='Success'){
            // DATA EXTEND FOR RECORD TO POPSAFE
            var dataExtend = JSON.stringify({
                "extendPincode": extend_pincode,
                "transactionRecord": "",
                "paymentMethod": "",
                "duration": "",
                "extendTimes": "",
                "overdueTime": "",
                "cost_overdue": "",
                "isExtend": false
            })

            if (isExtending == true) {
                var tmpExtend = JSON.parse(extendData);
                var tmpdataExtend =  JSON.parse(tmpExtend.overdue_data);
                var tmpTrx = JSON.parse(global_trans)
                // DATA EXTEND FOR RECORD TO POPSAFE
                dataExtend = JSON.stringify({
                    "extendPincode": extend_pincode,
                    "transactionRecord": tmpTrx.transaction_id,
                    "paymentMethod": provider,
                    "duration": tmpdataExtend.parcel_duration,
                    "extendTimes": tmpdataExtend.extend_overdue,
                    "overdueTime": tmpdataExtend.date_overdue,
                    "cost_overdue": tmpdataExtend.cost_overdue,
                    "minutes_duration": tmpdataExtend.parcel_minutes_duration,
                    "timestamp_duration": tmpdataExtend.timestamp_duration,
                    "isExtend": isExtending
                })
            }            
            
            console.log(" data_extend: " + dataExtend)
            
            slot_handler.customer_take_express(dataExtend+'||AP_EXPRESS');
            console.log("PEMBERITAHUAN extend pin code == "+ dataExtend)
            slot_handler.start_video_capture("take_apexpress_" + extend_pincode);
            return;
        }
    }

    function next_process(t){
        console.log('next_process', t);
        loadingPopUp.close();
        if (t=='ERROR'){
            my_timer.stop()
            notif_text.contentNotif = qsTr('Terjadi Kesalahan, Silakan Hubungi Layanan Pelanggan.');
            notif_text.successMode = false;
            notif_text.open();
            return
        }
        var info = JSON.parse(t);
        // bni-yap
        var param = {
            'paymentType': 'mid-snap',
            'duration': duration,
            'amount': amount,
            'paymentStatus': info.result,
            'pin_code': info.pin_code,
            'date': info.date,
            'door_no': info.door_no,
            'result': paymentResult,
            'phone_number': phone_number,
            'express': info.express
        };

        my_stack_view.push(open_door_ap, {summary: JSON.stringify(param), popsafe_param: popsafe_param, isPopDeposit: true});
        return
    }

    function select_door_result(r){

        console.log("process_signal_qr_payment: ", process_signal)

        if (!process_signal) return
        console.log('select_door_result_qr', r);
        switch (r){
        case 'NotMouth':
            notif_text.successMode = false;
            notif_text.contentNotif = qsTr('Mohon Maaf, Tidak Ada Loker Yang Tersedia Saat Ini.');
            notif_text.open();
            return;
        case 'Success':
            var param_popsafe = JSON.parse(popsafe_param)
            
            slot_handler.start_store_apexpress(phone_number, duration, param_popsafe.transactionRecord, param_popsafe.paymentMethod, param_popsafe.paymentAmount, lockerSize, paymentParam);

            return;
        default:
            return;
        }
        process_signal = false;
    }

    function trans_global_result(result){
        console.log("trans_global_result is ", result)
        if(result=="") return;
        else if(result=="ERROR"){
            loadingPopUp.close()
            dimm_connection.visible=true;
            my_timer.stop()
            my_failed_timer.stop()
            //yap_status_notif.visible = false
            
            return
        }
        else{
            var result_yap = JSON.parse(result);
            var result_trans = result_yap.status;
        }    
        if(result_trans=="PAID"){
            paymentSuccess = true;
            my_failed_timer.stop()
            // release_product(product_class);
            loadingPopUp.popupText = qsTr('Memproses...');
            if (isExtending){
                if(paymentCancel==true){
                    loadingPopUp.close();
                    dimm_act.visible = true;
                    notif_yes_no.visible = true;
                    confirm_yap.enabled = false;
                } else {
                    console.log(" dataOverdue_qrpayment: " + extendData)
                    slot_handler.start_update_apexpress(extend_pincode, result);
                }
            } else {
                if(paymentCancel==true){
                    loadingPopUp.close();
                    dimm_act.visible = true;
                    notif_yes_no.visible = true;
                    confirm_yap.enabled = false;
                } else {
                    slot_handler.start_choose_mouth_size(door_size,"staff_store_express");
                }
            }
            return
        }    
        else {
            if(paymentCancel==true){
                loadingPopUp.close();
                dimm_act.visible = true;
                notif_yes_no.visible = true;
                confirm_yap.enabled = false;
                paymentCancel = false
                press = '0'
            }else{
                loadingPopUp.close();
                main_page.enabled = false;
                yap_status_notif.visible = true;
                my_timer.stop()
                my_failed_timer.start()
                dimm_act.visible=true;
                confirm_yap.visible = false;
                press = '0'
            }
        }
    }

    function release_product(pc){
        switch(pc){
       
        case "popsafe-order":
            if (popsafe_param != undefined){
                var lockerSize = JSON.parse(popsafe_param).lockerSize
                slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
            }
            break
        case "popsafe-extend":
            if (popsafe_param != undefined){
                var pp = JSON.parse(products)
                var cc = JSON.parse(customers)
                var param = JSON.stringify({"expressNumber": pp.sku, "userSession": cc.sessionId, "trxRemarks": provider+"|"+prod_trans_id+"|"+amount})
                slot_handler.start_extend_express(param)
                slot_handler.start_video_capture("popsafe-extend-"+pp.sku+'-'+pp.pin)
                slot_handler.customer_take_express(pp.pin)
            }
            break;
        default:
            return
        }
    }

    function general_data_qr(obj){
        var product = JSON.parse(obj)
        qr_image.source = product.qr_url
        prod_name = product.name
        amount = product.total_amount
        main_page.enabled = true
    }

    function global_data_qr(objA, objB){
        console.log(objA, objB);
        var product = JSON.parse(objA)
        prod_name = "(" + product.name + ")"
        amount = product.total_amount
        var yap = JSON.parse(objB)
        prod_trans_id = yap.transaction_id //LKR
        prod_pay_id = yap.payment_id
        prod_valid_time = yap.expired_datetime
        qr_image.source = yap.url
        main_page.enabled = true
    }
   //TODO Create more text info for yap_transaction
    /*        {
            "payment_id": "BNI-YAP180224105220YCRKL",
            "transaction_id": "LKR1802242252HUG",
            "status": "CREATED",
            "url": "http://paymentdev.popbox.asia/img/payment/yap/BNI-YAP180224105220YCRKL.png",
            "expired_datetime": "2018-02-25 22:52:20"
        }*/

    function define_provider(provider){
        console.log('define provider : ' + provider)

        if (provider=="") return;
        switch(provider){
            case 'DIMO-PAYBYQR':
                general_data_qr(products)
                app_url = "img/payment/paybyqr-small.png"
                small_logo_vis = true
                break
            case 'BNI-YAP': case 'bni-yap':
                global_data_qr(products, global_trans)
                app_url = "img/payment/yap-small.png"
                break
            case 'MID-GOPAY':
                global_data_qr(products, global_trans)
                app_url = "img/payment/gopay-small.png"
                break
              case 'TCASH':
                global_data_qr(products, global_trans)
                app_url = "img/payment/tcash-small.png"
                break
            case 'OTTO-QR':
                global_data_qr(products, global_trans)
                app_url = "img/payment/ottopay-small.png"
                break

            default:
                app_url = "img/otherImages/checked_white.png"
                break
        }        
    }

    function insert_flg(a){
        if(a != undefined){
            var newstrA=""
            newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
            return newstrA
        }else{
            return a
        }
    }

    Rectangle{
        id: main_page
        width: parent.width
        height: parent.height - 90
        color: "#ffffff"
        anchors.top: parent.top
        anchors.topMargin: 90
        Image {
            id: step_progress
            width: 500
            height: 51
            anchors.top: parent.top
            anchors.topMargin: 25
            anchors.horizontalCenter: parent.horizontalCenter
            source: "img/apservice/langkah/2.png"
        }

        Text {
            id: main_command_text //
            width: 500
            height: 100
            color: "#323232"
            text: qsTr("Bayar dengan Pindai QR")
            anchors.left: parent.left
            anchors.leftMargin: 50
            anchors.top: parent.top
            anchors.topMargin: 110
            font.bold: true
            font.family: 'Microsoft YaHei'
            font.pixelSize: 50

        }

        Column{
            id: details_text
            anchors.verticalCenterOffset: 10
            anchors.left: parent.left
            anchors.leftMargin: 44
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10
            RowTextAP{
                id: order_text
                labelText: qsTr('Order')
                contentText: prod_trans_id
                colorContent: '#323232'
                contentBold: true
            }
            RowTextAP{
                id: jumlah_text
                labelText: qsTr('Jumlah')
                //contentText: 'Rp'+FUNC.insert_dot(amount)
                contentText: "Rp " + insert_flg(amount)
                colorContent: '#009BE1'
                contentBold: true
            }
            RowTextAP{
                id: bayar_text
                labelText: qsTr('Bayar')
            }
            RowTextAP{
                id: take_time_
                labelText: qsTr('Waktu')
                contentText: abc.counter + qsTr(' Detik')
                colorContent: '#009BE1'
                contentBold: true
            }

        }

        Image{
            id: qr_app_logo
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 260
            anchors.left: parent.left
            anchors.leftMargin: 200
            width: 100
            height: 80
            fillMode: Image.PreserveAspectFit
            source: app_url
        }

        Rectangle{
            id: rec_qr
            //x: 362
            color: "white"
            radius: 20
            anchors.top: parent.top
            anchors.topMargin: 200
            anchors.right: parent.right
            anchors.rightMargin: 70
            width: 350
            height: 350

            Image{
                id: qr_image
                scale: 0.95
                opacity: 1
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
                onStatusChanged: if(qr_image.status == Image.Ready) loadingPopUp.close()
            }
        }

        Text{
            id:prod_item_name
           // x: 362
            width: 300
            text: (provider=="BNI-YAP") ? prod_pay_id + "\n" + prod_name : prod_name
            //text: prod_trans_id
            anchors.top: parent.top
            anchors.topMargin: 550
            anchors.right: parent.right
            anchors.rightMargin: 90
            color:"black"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:18
            font.bold: true
        }

        Image {
            id: images_logos
            x: 32
            y: 554
            width: 961
            height: 101
            source: "img/payment/small_logos.png"
            visible: small_logo_vis
        }

        NotifButtonAP{
            id: confirm_yap
            x: 300
            anchors.top: parent.top
            anchors.topMargin: 560
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('SUDAH BAYAR')
            visible: !small_logo_vis

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press!="0") return
                    press = "1"
                    my_timer.stop()
                    loadingPopUp.popupText = qsTr('Memeriksa Pembayaran...');
                    loadingPopUp.open();
                    slot_handler.start_check_trans_global()
//                    console.log('confirm_button is pressed..!')
                }
            }
        }

        NotifButtonAP{
            id: batal_yap
            x: 40
            anchors.top: parent.top
            anchors.topMargin: 560
            modeReverse: true
            buttonText: qsTr('BATALKAN')
            visible: !small_logo_vis
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press!="0") return
                    press = "1"
                    loadingPopUp.popupText = qsTr('Memeriksa Pembayaran...');
                    loadingPopUp.open();
                    paymentCancel = true
                    slot_handler.start_check_trans_global()
                    // dimm_act.visible = true;
                    // notif_yes_no.visible = true;
                    // confirm_yap.enabled = false;
                }
            }
        }

    }
    Rectangle{
        id:dimm_act
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"
    }

    Rectangle{
        id:notif_yes_no
        width: 650
        height: 400
        visible: false
        color: "white"
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text{
            id:text_yes_no
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 0
            text: (paymentCancel==true) ? qsTr("Pembayaran Sukses") : qsTr("Batalkan Transaksi")
            anchors.top: parent.top
            anchors.topMargin: 50
            font.bold: true
            color:"#009BE1"
            font.family:"Microsoft YaHei"
            font.pixelSize:43
        }

        Text{
            id:isi_yes_no
            x: 93
            width: 850
            height: 50
            text: (paymentCancel==true) ? qsTr("Transaksi kamu tidak dapat dibatalkan,\nkarena status pembayaran sudah sukses.") : qsTr("Apakah Anda yakin ingin membatalkan\ntransaksi?")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 200
            font.family:"Microsoft YaHei"
            color:"#4A4A4A"
            textFormat: Text.PlainText
            font.pixelSize: 26
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        NotifButtonAP{
            id: tidak_yes_no
            x: 50
            anchors.top: parent.top
            anchors.topMargin: 280
            visible : (paymentCancel==true) ? false : true
            buttonColor: 'BLUE'
            modeReverse: true
            buttonText: qsTr('TIDAK')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    main_page.enabled = true;
                    press = '0';
                    notif_yes_no.visible = false;
                    dimm_act.visible = false;
                    confirm_yap.enabled = true;
                }
            }
        }

        NotifButtonAP{
            id: oke_yes_no
            x: 350
            visible: (paymentCancel==true) ? false : true
            anchors.top: parent.top
            anchors.topMargin: 280
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('YA')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(qr_image.source!=undefined){
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    } else {
                        my_timer.stop()
                        my_stack_view.pop()
                        press = '0'
                    }
                }
            }
        }

        NotifButtonAP{
            id: payment_cancel
            x: 200
            visible: (paymentCancel==true) ? true : false
            anchors.top: parent.top
            anchors.topMargin: 280
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('LANJUT')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    notif_yes_no.visible = false;
                    dimm_act.visible = false;
                    confirm_yap.enabled = true;
                    paymentCancel = false
                    loadingPopUp.open();
                    slot_handler.start_check_trans_global()
                }
            }
        }

    }

    Rectangle{
        id:yap_status_notif
        width: 650
        height: 500
        visible:false
        color: "white"
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text{
            id:prod_item_trans_id
            //x: 200
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 0
            //width: 300
            //text: prod_trans_id
            text: (paymentSuccess==true) ? qsTr("Selamat, Pembayaran Anda berhasil.") : qsTr("Pembayaran Gagal")
            anchors.top: parent.top
            anchors.topMargin: 10
            font.bold: true
            color:"#009BE1"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:43
        }

        Text{
            id:text_yap_notif
            x: 93
            width: 850
            height: 50
            text: (paymentSuccess==true) ? qsTr("Selamat, Pembayaran Anda berhasil.") : qsTr("Maaf untuk transaksi Anda Gagal.\nsilakan ulangi pembayaran")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 160
            font.family:"Microsoft YaHei"
            color:"#4A4A4A"
            textFormat: Text.PlainText
            font.pixelSize: 26
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: yap_img_notif
            x: 410
            width: 215
            height: 215
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 220
            fillMode: Image.PreserveAspectFit
            source: (paymentSuccess==true) ? "img/apservice/icon/sukses_biru.png"  : "img/payment/illustration-failed.png"
        }

        NotifButtonAP{
            id: yap_notif_retry
            x: 365
            anchors.top: parent.top
            anchors.topMargin: 400
            anchors.horizontalCenter: parent.horizontalCenter
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('COBA BAYAR LAGI')

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    abc.counter = timer_value;
                    my_timer.start()
                    my_failed_timer.stop()
                    main_page.enabled = true;
                    press = '0';
                    yap_status_notif.visible = false;
                    dimm_act.visible = false;
                    confirm_yap.visible = true;
                    
                }
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                take_time_.contentText = abc.counter + qsTr(' Detik')
                if(abc.counter <= 0){
                    loadingPopUp.popupText = qsTr('Memeriksa Pembayaran...');
                    loadingPopUp.open();
                    //paymentCancel = true
                    slot_handler.start_check_trans_global()
                    my_timer.stop()
                    //my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }
    
    Rectangle{
        id: timer_failed
        x:50
        y:50
        QtObject{
            id:cba
            property int counter
            Component.onCompleted:{
                cba.counter = timer_value
            }
        }

        Timer{
            id:my_failed_timer
            interval:1000
            repeat:true
            running:false
            triggeredOnStart:false
            onTriggered:{
                cba.counter -= 1
                if(cba.counter <= 0){
                    my_failed_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
        successMode: false
    }

    FailurePaymentAP{
        id: failure_notif
        titleText: qsTr('Pembayaran Gagal')
        notifText: qsTr('Mohon Maaf, Transaksi Anda gagal. Silakan Ulangi Pembayaran.')
    }

    LoadingPopUp{
        id: loadingPopUp
        z: 99
    }

    Rectangle{
        id:dimm_connection
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"

        Rectangle{
            id: rec
            width: 600
            height: 460
            //border.color: "black"
            //color: "transparent"
            color:"white"
            radius: 22
            anchors.top: parent.top
            anchors.topMargin: 150
            anchors.left: parent.left
            anchors.leftMargin: 218

            Image{
                id: no_wifi
                width: 180
                height: 180
                anchors.top: parent.top
                anchors.topMargin: 50
                anchors.horizontalCenter: parent.horizontalCenter
                source: "img/apservice/no-wifi.png"

            }

            Text {
                color: "#A6B1B3"
                text: qsTr("Connection Issue")
                anchors.horizontalCenter: parent.horizontalCenter

                // anchors.left: parent.left
                // anchors.leftMargin:60
                anchors.top: parent.top
                anchors.topMargin: 250
                font.family: 'Microsoft YaHei'
                font.pixelSize: 50
                font.bold: true
            }

            Rectangle {
                id: btn_cobalagi
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 110
                //visible: true
                Image {
                    width: 200
                    height: 60
                    source: "img/courier08/08ground.1.png"
                    anchors.horizontalCenter: parent.horizontalCenter

                    Text{
                        anchors.horizontalCenter : parent.horizontalCenter
                        anchors.verticalCenter : parent.verticalCenter
                        font.family: 'Microsoft YaHei'
                        font.pixelSize: 20
                        color: "#ffffff"
                        font.bold: true
                        text: qsTr("COBA LAGI")
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            my_failed_timer.start()
                             cba.counter = timer_value;
                            yap_status_notif.visible = false
                            dimm_connection.visible = false
                            loadingPopUp.popupText = qsTr('Memeriksa Pembayaran...');
                            loadingPopUp.open();
                            slot_handler.start_check_trans_global()
                        }
                    }
                }
            }
        } 
    }
}                    