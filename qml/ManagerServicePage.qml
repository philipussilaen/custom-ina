import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property var name:""
    property var phone_number:""
    property var company_name: ""
    property int timer_value: 60
    property var overdue_num:""

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('name user : ' + name )
            if (name=="POPDEV"){
                phone_number = "-"
                company_name = "PopBox Developer"
            } else {
                slot_handler.get_user_info()
            }
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_load_manager_overdue_express_count()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        root.user_info_result.connect(handle_result)
        root.overdue_express_count_result.connect(overdue_count)
    }

    Component.onDestruction: {
        root.user_info_result.disconnect(handle_result)
        root.overdue_express_count_result.disconnect(overdue_count)
    }

    function overdue_count(text){
        overdue_num.text = text
    }

    function handle_result(text){
        var result = JSON.parse(text)
        name = result.name
        phone_number = result.phoneNumber
        company_name = result.company_name
    }

    ManagerServiceButton{
        id:manager_service_button
        x: 162
        y: 354
        show_icon:"img/manage25/3.png"
        show_source:"img/bottondown/1.png"
        show_text:qsTr("sys-info")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(box_manage_page)
            }
            onEntered:{
                manager_service_button.show_icon = "img/manage25/4.png"
                manager_service_button.show_text_color = "white"
            }
            onExited:{
                manager_service_button.show_icon = "img/manage25/3.png"
                manager_service_button.show_source = "img/bottondown/1.png"
                manager_service_button.show_text_color = "#BF2E26"
            }
        }
    }

    ManagerServiceButton{
        id:manager_service_button1
        x: 642
        y: 464
        show_icon:"img/manage25/9.png"
        show_source:"img/bottondown/1.png"
        show_text:qsTr("box")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent

            onClicked:{
                my_stack_view.push(manager_cabinet_view)
            }
            onEntered:{
                manager_service_button1.show_icon = "img/manage25/10.png"
                manager_service_button1.show_text_color = "white"
            }
            onExited:{
                manager_service_button1.show_icon = "img/manage25/9.png"
                manager_service_button1.show_source = "img/bottondown/1.png"
                manager_service_button1.show_text_color = "#BF2E26"
            }
        }
    }

    ManagerServiceButton{
        id:manager_service_button2
        x: 402
        y: 464
        show_icon:"img/manage25/7.png"
        show_source:"img/bottondown/1.png"
        show_text:qsTr("unusual")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked:{
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                manager_service_button2.show_icon = "img/manage25/8.png"
                manager_service_button2.show_text_color = "white"
            }
            onExited:{
                manager_service_button2.show_icon = "img/manage25/7.png"
                manager_service_button2.show_source = "img/bottondown/1.png"
                manager_service_button2.show_text_color = "#BF2E26"
            }
        }
    }

    ManagerServiceButton{
        id:manager_service_button3
        x: 402
        y: 354
        show_icon:"img/manage25/5.png"
        show_source:"img/bottondown/1.png"
        show_text:qsTr("overdue")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked:{
                my_stack_view.push(manage_take_overdue_page)
            }
            onEntered:{
                manager_service_button3.show_icon = "img/manage25/6.png"
                manager_service_button3.show_text_color = "white"
            }
            onExited:{
                manager_service_button3.show_icon = "img/manage25/5.png"
                manager_service_button3.show_source = "img/bottondown/1.png"
                manager_service_button3.show_text_color = "#BF2E26"
            }
            Text {
                id: overdue_num
                x: 183
                y: 0
                color: "#BF2E26"
                text: qsTr("10")
                anchors.right: parent.right
                anchors.rightMargin: 8
                styleColor: "#BF2E26"
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.bold: true
                font.pixelSize: 24
            }
        }
    }

    Text {
        id: text1
        x: 165
        y: 272
        width: 100
        height: 50
        text: qsTr("address:")
        color:"#FFFFFF"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: "Microsoft YaHei"
        font.pixelSize: 30
    }

    Text {
        color:"#FFFFFF"
        id: text2
        x: 165
        y: 221
        width: 100
        height: 50
        text: qsTr("number:")
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 30
    }

    Text {
        color:"#FFFFFF"
        id: text3
        x: 165
        y: 165
        width: 100
        height: 50
        text: qsTr("name:")
        verticalAlignment: Text.AlignVCenter
        font.family: "Microsoft YaHei"
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 30
    }

    Text {
        color:"#FFFFFF"
        id: text4
        x: 300
        y: 272
        width: 428
        height: 50
        text: company_name
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 30
    }

    Text {
        color:"#FFFFFF"
        id: text5
        x: 300
        y: 221
        width: 428
        height: 50
        text: phone_number
        verticalAlignment: Text.AlignVCenter
        font.family: "Microsoft YaHei"
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 30
    }

    Text {
        color:"#FFFFFF"
        id: text6
        x: 300
        y: 165
        width: 428
        height: 50
        text: name
        font.family: "Microsoft YaHei"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 30
    }

    QuitButton {
        id:pick_up_Button1
        x: 874
        y: 630
        show_text: qsTr("exit")

        MouseArea {
            anchors.leftMargin: 4
            anchors.bottomMargin: -2
            anchors.rightMargin: -4
            anchors.fill: parent
            anchors.topMargin: 2
            onClicked:{
                manager_take_send_express_button.enabled = false
                pick_up_Button2.enabled = false
                pick_up_Button1.enabled = false
                manager_service_button3.enabled = false
                manager_service_button2.enabled = false
                manager_service_button1.enabled = false
                manager_service_button.enabled = false
                check_quit.open()
            }
        }
    }

    PickUpButton {
        id:pick_up_Button2
        x: 162
        y: 630
        show_text: qsTr("return")

        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked:{
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    ManagerServiceButton {
        id: manager_take_send_express_button
        x: 162
        y: 464
        show_text: qsTr("take_send")
        show_icon: "img/manage25/1.png"
        show_source:"img/bottondown/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked:{
                my_stack_view.push(take_send_express_page)
            }
            onEntered:{
                manager_take_send_express_button.show_icon = "img/manage25/2.png"
                manager_take_send_express_button.show_text_color = "white"
            }
            onExited:{
                manager_take_send_express_button.show_icon = "img/manage25/1.png"
                manager_take_send_express_button.show_source = "img/bottondown/1.png"
                manager_take_send_express_button.show_text_color = "#BF2E26"
            }
        }
    }

    ManagerServiceButton {
        id: manager_take_reject_express_button
        x: 642
        y: 354
        show_text: qsTr("take_reject")
        show_icon: "img/manage25/11.png"
        show_source:"img/bottondown/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked:{
                my_stack_view.push(take_reject_express)
            }
            onEntered:{
                manager_take_reject_express_button.show_icon = "img/manage25/12.png"
                manager_take_reject_express_button.show_text_color = "white"
            }
            onExited:{
                manager_take_reject_express_button.show_icon = "img/manage25/11.png"
                manager_take_reject_express_button.show_source = "img/bottondown/1.png"
                manager_take_reject_express_button.show_text_color = "#BF2E26"
            }
        }
    }

    HideWindow{
        id:check_quit
        //visible:true

        Image {
            id: img_lock_secret
            x: 434
            y: 214
            width: 200
            height: 200
            source: "img/otherImages/lock_opened.png"
        }

        Text {
            x: 0
            y:446
            width: 1024
            height: 60
            text: qsTr("sure to quit")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:sure_button
            x:200
            y:560
            show_text:qsTr("sure")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_explorer()
                    Qt.quit()
                }
            }
        }

        OverTimeButton{
            id:back_button
            x:550
            y:560
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    manager_take_send_express_button.enabled = true
                    pick_up_Button2.enabled = true
                    pick_up_Button1.enabled = true
                    manager_service_button3.enabled = true
                    manager_service_button2.enabled = true
                    manager_service_button1.enabled = true
                    manager_service_button.enabled = true
                    check_quit.close()
                }
            }
        }
    }
}
