import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:true_input_phone
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var pref_login_user:""
    property var expressNo:""

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('prefix_user_account : ' + pref_login_user)
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            if(show_text == "000"){
                notif_one.visible = false
                notif_one.enabled = false
                notif_two.visible = true
                notif_two.enabled = true
            }else{
                notif_one.visible = true
                notif_one.enabled = true
                notif_two.visible = false
                notif_two.enabled = false
            }
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        width: 50
        height: 50
        color: "#00000000"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Text{
            id:countShow_test
            x: 10
            y: 10
            anchors.centerIn:parent
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:20
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id:opacity_bground
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#472f2f"
        opacity: 0.4
    }

    Rectangle{
        id: notif_one
        x: 130
        y: 220
        width: 790
        height: 440
        visible: false
        enabled: false

        Image {
            id: bground_one
            x: -5
            y: -2
            width: 800
            height: 450
            source: "img/courier13/layerground1.png"

            Text {
                id: please_check_one_1
                x: 0
                y: 40
                width: 800
                height: 60
                color: "#FFFFFF"
                font.family:"Microsoft YaHei"
                text: qsTr("Please check the customer mobile phone number")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 30
                textFormat: Text.AutoText
            }

            Text {
                id: please_check_one_2
                x: 0
                y: 153
                width: 800
                height: 60
                color: "#FFFFFF"
                text: qsTr("(customer verification code and QR CODE will be sent)")
                wrapMode: Text.WordWrap
                font.italic: true
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 17
                font.family:"Microsoft YaHei"
                textFormat: Text.AutoText
            }

            Text {
                id: phone_num_one
                y: 190
                width: 800
                height: 60
                color: "#FFFFFF"
                font.family:"Microsoft YaHei"
                text: show_text
                horizontalAlignment: Text.AlignHCenter
                font.pointSize:45
            }

        }

        OverTimeButton{
            id:back_button_notif_one
            x:90
            y:300
            show_text:qsTr("cancel")
            show_x:15
            show_image:"img/05/back.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop()
                }
                onEntered:{
                    back_button_notif_one.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    back_button_notif_one.show_source = "img/05/button.png"
                }
            }
        }

        OverTimeButton{
            id:ok_button_notif_one
            x:430
            y:300
            show_text:qsTr("ok")
            show_x:205
            show_image:"img/05/ok.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_video_capture("courier_drop_" + expressNo)
                    slot_handler.set_phone_number(show_text)
                    my_stack_view.push(courier_select_box_view, {pref_login_user:pref_login_user})
                }
                onEntered:{
                    ok_button_notif_one.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    ok_button_notif_one.show_source = "img/05/button.png"
                }
            }
        }
    }

    Rectangle {
        id: notif_two
        x: 122
        y: 219
        width: 790
        height: 440
        visible: false
        enabled: false

        Image {
            id: bground_two
            x: -5
            y: -2
            width: 800
            height: 450

            Text {
                id: please_check_two_1
                x: 0
                y: 74
                width: 800
                height: 120
                color: "#ffffff"
                text: qsTr("Notification will be sent immediately to the administrator number.")
                wrapMode: Text.WordWrap
                font.pointSize: 30
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.AutoText
                font.family: "Microsoft YaHei"
            }

            Text {
                id: please_check_two_2
                x: 0
                y: 200
                width: 800
                height: 60
                color: "#ffffff"
                text: qsTr("Please Press OK to continue the process.")
                font.italic: true
                font.pointSize: 20
                horizontalAlignment: Text.AlignHCenter
                textFormat: Text.AutoText
                font.family: "Microsoft YaHei"
            }
            source: "img/courier13/layerground1.png"
        }

        OverTimeButton {
            id: back_button_notif_two
            x: 90
            y: 300
            show_image: "img/05/back.png"
            show_text: qsTr("cancel")
            show_x: 15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop()
                }
                onEntered:{
                    back_button_notif_two.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    back_button_notif_two.show_source = "img/05/button.png"
                }
            }
        }

        OverTimeButton {
            id: ok_button_notif_two
            x: 430
            y: 300
            show_image: "img/05/ok.png"
            show_text: qsTr("ok")
            show_x: 205

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.set_phone_number(show_text)
                    my_stack_view.push(courier_select_box_view, {pref_login_user:pref_login_user})
                }
                onEntered:{
                    ok_button_notif_two.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    ok_button_notif_two.show_source = "img/05/button.png"
                }
            }
        }
    }
}
