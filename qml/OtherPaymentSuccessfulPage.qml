import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_payment_successful_page
    width: 1024
    height: 768
    property var amount:""
    property var last_balance:""
    property var card:""
    property var show_data:""
    property var locker:""
    property var terminal_id:""
    property int timer_value: 60
    property var payment_result:""
    property var order_number:""
    property var new_amount: "-"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            show_payment_result(payment_result)
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    console.log('settle order : ' + order_number)
                    slot_handler.start_push_data_settlement(order_number)
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    function show_payment_result(payment_result){
        console.log("settlement_result : " + payment_result)
        if(payment_result == ""){
            return
        }

        var obj = JSON.parse(payment_result)
        terminal_id = obj.terminal_id
        locker = obj.locker
        last_balance = insert_flg(obj.last_balance + "")
        card = obj.card_no
        new_amount = insert_flg(amount)

        var now_date = new Date(obj.show_date)
        var time_Y = add_zero(now_date.getFullYear());
        var mouth = (now_date.getMonth()+1 < 10 ? +(now_date.getMonth()+1) : now_date.getMonth()+1)
        switch (mouth){
            case 1: var time_M = "January"
                break;
            case 2: var time_M = "February"
                break;
            case 3: var time_M = "March"
                break;
            case 4: var time_M = "April"
                break;
            case 5: var time_M = "May"
                break;
            case 6: var time_M = "June"
                break;
            case 7: var time_M = "July"
                break;
            case 8: var time_M = "August"
                break ;
            case 9: var time_M = "September"
                break;
            case 10: var time_M = "October"
                break;
            case 11: var time_M = "November"
                break;
            case 12: var time_M = "December"
                break;
        }
        var time_D = add_zero(now_date.getDate());
        var time_h = add_zero(now_date.getHours());
        var time_m = add_zero(now_date.getMinutes());
        var time_s = add_zero(now_date.getSeconds());
        show_data = time_D+" "+time_M+" "+time_Y+" "+time_h+":"+time_m+":"+time_s
    }

    function add_zero(temp){
        if(temp<10) return "0"+temp;
        else return temp;
    }

    function change_mouth(mouth){
        switch (mouth){
            case "1": print("January")
                break;
            case "2": print("February")
                break;
            case "3": print("March")
                break;
            case "4": print("April")
                break;
            case "5": print("May")
                break;
            case "6": print("June")
                break;
            case "7": print("July")
                break;
            case "8": print("August")
                break ;
            case "9": print("September")
                break;
            case "10": print("October")
                break;
            case "11": print("November")
                break;
            case "12": print("December")
                break;
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    FullWidthReminderText{
       id:text
       y:140
       remind_text:qsTr("Order Details")
       remind_text_size:"35"
    }

    FullWidthReminderText{
       id:text1
       y:540
       remind_text:qsTr("The sms notification will be sent to you to pickup your parcel")
       remind_text_size:"30"
    }

    DoorButton{
        id:back_button
        y:671
        x:373
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log('settle order : ' + order_number)
                slot_handler.start_push_data_settlement(order_number)
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    Row{
        x:220
        y:220
        spacing:30
        Column{
            spacing:10
            Text {
                text: qsTr("Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Last Balance")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Card")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Data")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Locker")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Terminal ID")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
        }
        Row{
            spacing:10
            Column{
                spacing:10
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:10
                Text {
                    text: amount
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: last_balance
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: card
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: show_data
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: locker
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: terminal_id
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
        }
    }

    FullWidthReminderText {
        id: text2
        x: 0
        y: 593
        remind_text: qsTr("Please do not forget to take your prepaid card")
        remind_text_size: "30"
    }

}
