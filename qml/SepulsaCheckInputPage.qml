import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:sepulsa_check_input

    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var show_text_count:0

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            if(sepulsa_check_input.show_text!=""){
                sepulsa_check_input.show_text=""
                show_text_count = 0
            }
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{
        id:main_page
         FullWidthReminderText{
            id:main_h1_text
            x: 0
            y:153
            remind_text:qsTr("Enter Transaction Record Number")
            remind_text_size:"35"
        }

         Rectangle{
             id: rec_input_text
            x:162
            y:227
            width:700
            height:60
            color:"transparent"

            Image{
                id: frame_img
                anchors.fill: parent
                source: "img/courier11/input1.png"
            }

            TextEdit{
                id: input_text
                anchors.fill: parent
                x:10
                font.family:"Microsoft YaHei"
                text:show_text
                horizontalAlignment: Text.AlignHCenter
                color:"#FFFFFF"
                font.pixelSize:40
            }
        }

        FullKeyboard{
            id:touch_keyboard
            x:29
            y:360
            property var count:show_text_count
            property var validate_code:""

            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(show_validate_code)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            function on_function_button_clicked(str){
                if(str == "ok"){
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    if(sepulsa_check_input.show_text == "" || sepulsa_check_input.show_text.length < 8){
                        main_page.enabled = false
                        error_tips.open()
                    }
                    else{
                        console.log("checking for trx_id : ", show_text)
                        count = 0
                        loading.open()
                        slot_handler.sepulsa_trx_check(show_text)
                        touch_keyboard.enabled = false
                    }
                }

                if(str=="delete"){
                    if(count>=30){
                        count=29
                    }
                }
            }

            function show_validate_code(str){
                if (str == "" && count > 0){
                    if(count>=30){
                        count=30
                    }
                    count--
                    sepulsa_check_input.show_text=sepulsa_check_input.show_text.substring(0,count);
                }
                if (str != "" && count < 30){
                    count++
                }
                if (count>=30){
                    str=""
                }
                else{
                    sepulsa_check_input.show_text += str
                }
                abc.counter = timer_value
                my_timer.restart()
            }
        }
    }

    Component.onCompleted: {
        root.sepulsa_trx_check_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.sepulsa_trx_check_result.disconnect(handle_text)
    }

    function handle_text(result){
        loading.close()
        touch_keyboard.enabled = true
        if(result == "" || result == "ERROR" || result == "Invalid Transaction" || result == "[]"){
            loading.close()
            main_page.enabled = false
            error_tips.open()
        }else{
            my_stack_view.push(sepulsa_check_info_view, {detail_result: result, trx_id: show_text})
        }
    }

    LoadingView{
        id:loading
    }

        HideWindow{
        id:error_tips
        //visible: true

        Text {
            id: oops_text
            x: 113
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: details_error_text
            x: 108
            y:301
            width: 809
            height: 250
            text: qsTr("The transaction number cannot be verified. Please retry and enter the correct transaction number.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }


        OverTimeButton{
            id:ok_back
            x:374
            y:597
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                anchors.fill: parent
                onClicked: {
                    if(sepulsa_check_input.show_text!=""){
                        sepulsa_check_input.show_text=""
                        show_text_count = 0
                    }
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    error_tips.close()
                    main_page.enabled = true
                    touch_keyboard.enabled = true
                }
                onEntered:{
                    ok_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
