import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property int timer_value: 60
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }


    }

    Text{
        id:title_page
        x:0
        y:144
        width: 1024
        height: 60
        text:qsTr("Oops, Failed Authentication!")
        font.family:"Microsoft YaHei"
        horizontalAlignment: Text.AlignHCenter
        color:"#FFFFFF"
        font.pixelSize:35
    }

    Image {
        id: img_error_psw
        x: 387
        y: 224
        width: 250
        height: 250
        source: "img/otherImages/user_warning.png"
    }

    Text {
        id: suggest_text
        x: 0
        y: 504
        width: 1024
        height: 60
        color:"#FFFFFF"
        font.family:"Microsoft YaHei"
        text: qsTr("Please re-enter your account name and password, thank you!")
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
    }

    BackButtonMenu{
        id:psw_error_button
        x:419
        y:586
        show_text:qsTr("back")

        MouseArea {

            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                psw_error_button.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                psw_error_button.show_source = "img/button/7.png"
            }
        }
    }
}
