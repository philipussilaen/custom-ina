import QtQuick 2.4
import QtQuick.Controls 1.2


Rectangle{
    id: rectangle1

    width:247
    height:56
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_x
    property var show_source:"img/05/button2.png"

    Image{
        width:247
        height:56
        source:show_source
    }
    Rectangle{
        id: rectangle2
        width:247
        height:56
        color:"transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        Image{
            x:show_x
            y:13
            width:28
            height:28
            source:show_image
        }
        Text{
            text:show_text
            anchors.horizontalCenterOffset: 10
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color:"red"
            font.family:"Microsoft YaHei"
            font.pixelSize:25
        }
    }
}
