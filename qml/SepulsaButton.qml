import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_rec
    width:150
    height:200
    color:"transparent"

    property var show_text: "995.000"
    property var show_init_price: "1.000.000"
    property var button_type: "PULSA"
    property var button_desc: "Telkomsel 1000000 - Diskon 5.000"
    property var show_image: "img/sepulsa/logo/telkomsel.png"
    property var show_text_color: "red"
    property var show_bg_color: "white"
    property var isPromo: "true"

    Rectangle{
        id: rec_bground
        anchors.fill: parent
        radius: 10
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }

            GradientStop {
                position: 1
                color: "#d4d0d0"
            }
        }
        border.color: "#e2dddd"
        border.width: 0
    }

    Image{
        id: img
        rotation: -45
        visible: false //Disabling Watermark
        scale: 1
        anchors.fill: parent_rec
        source: show_image
        opacity: 0.5
        fillMode: Image.PreserveAspectFit
    }

    Rectangle{
        id: foreground_text_button_type
        x:25
        width: parent_rec.width
        height: 40
        color: (button_type=="PULSA") ? "gray" : "black"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        Text{
            id: text_button_type
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: (button_type=="PULSA") ? "black" : "white"
            font.pixelSize: 25
            font.family: "Microsoft YaHei"
            font.bold: true
            text: button_type
        }
    }

    Text{
        id: text_nominal
        x:0
        y:70
        width: parent_rec.width
        height: 60
        color: show_text_color
        text: show_text
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
        font.family: "Microsoft YaHei"
    }

    Text{
        id: text_init_price
        x:0
        y:53
        width: parent_rec.width
        height: 17
        visible: (isPromo=="true" && show_text!=show_init_price) ? true : false
        color: show_text_color
        text: show_init_price
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 15
        font.family: "Microsoft YaHei"
        font.italic: true
        font.strikeout: true
    }

    Text{
        id: text_button_desc
        x: 22
        y: 137
        width: parent_rec.width
        height: 55
        color: show_text_color
        text: button_desc
        font.italic: true
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        wrapMode: Text.WordWrap
        textFormat: Text.PlainText
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: "Microsoft YaHei"
        anchors.horizontalCenterOffset: 0
        horizontalAlignment: Text.AlignHCenter
    }

    Image{
        id: badge_promo
        visible: (isPromo=="true") ? true : false
        x: -2
        y: -3
        width: 80
        height: 80
        fillMode: Image.PreserveAspectFit
        source: "img/sepulsa/promo_badge.png"
    }
}
