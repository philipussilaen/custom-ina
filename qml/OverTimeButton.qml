import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle1
    width:279
    height:64
    property bool global_vis: true

    visible: global_vis

    property var show_text:""
    property var show_image:""
    property var show_x
    property var show_source:"img/button/button1.png"
    property var text_color: "red"
    property var bg_color: "transparent"
    property int text_size: 30

    color:bg_color

    Image{
        width:279
        height:64
        source:show_source
    }

    Text{
        text:show_text
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color:text_color
        font.family:"Microsoft YaHei"
        font.pixelSize:text_size
    }
}
