import QtQuick 2.4
import QtQuick.Controls 1.2
import "base_function.js" as FUNC

Background{
    id:simcard_service_page
    property int timer_value: 60
    property var press: "0"
    property var sim_data: undefined
    property var total_product:  1
    property var operator_name: undefined
    property var product_type: undefined
    property int count: 0
    property var filter_text: ''
    property bool keyboardVis: false
    width: 1024
    height: 768
//    color: 'darkred'

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            parse_sim_data(sim_data)
            loading_view.close()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }    

    Component.onCompleted: {
        root.get_sim_card_result.connect(parse_sim_data);
    }

    Component.onDestruction: {
        root.get_sim_card_result.disconnect(parse_sim_data);        
    }

    function parse_sim_data(objD){
//        console.log('sim_data', objD)
        loading_view.close()
        press = "0"

        if (objD==undefined) return
        gridview.model = undefined;
        buttonItemSection_listModel.clear()

        if (objD=='NO_DATA'){
            gridview.model = buttonItemSection_listModel;
            return
        }

        var d = JSON.parse(objD)
        total_product = d.total
        product_type = d.type
        operator_name = d.operator
//        page_title.remind_text = qsTr("Choose SIM Card Below :") + operator_name.toUpperCase()
        page_title.remind_text = qsTr("Choose SIM Card Below :")

        var sims = d.data
        for (var i in sims){
            if (sims[i].status == 'new'){
                buttonItemSection_listModel.append(sims[i])
            }
        }

        gridview.model = buttonItemSection_listModel;
        console.log('Total SIM Card : ' + buttonItemSection_listModel.count);
    }


    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    FullWidthReminderText{
        id:page_title
        y:148
        visible: false
        remind_text_size:"27"
        remind_text: qsTr("Choose SIM Card Below :")
    }

    Rectangle{
        id: input_zone
        x: 137
        y: 130
        width: 600
        height: 50
        color: "white"
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        radius: 30

        Image{
            scale: 0.25
            anchors.top: parent.top
            anchors.topMargin: -35
            anchors.right: parent.right
            anchors.rightMargin: -30
            source:"img/ppob/global_search.png"
        }

        TextEdit{
            id:text_input2
            text:filter_text
//            text: '0812'
            anchors.topMargin: 6
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: parent.left
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"darkred"
            font.pixelSize:30
//            cursorVisible: true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    keyboardVis = true;
                }
            }
        }
    }

    Text{
        id: reset_text_button
        x: 830
        y: 147
        text: 'RESET'
        color: 'yellow'
        font.family: 'Microsoft YaHei'
        font.underline: true
//        font.bold: true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return
                press = '1'
                filter_text = '';
                count = 0;
                loading_view.open();
                slot_handler.start_filter_sim_data('RESET');
            }
        }
    }

    Item{
        id: button_item_view
        x: 0
        y: 200
        width: 1024
        height: 568
        focus: true
        visible: !keyboardVis

        ScrollBarPop{
            id: vertical_sbar
            flickable: gridview
            x: 0
            y: 266
            height: button_item_view.height
            color: "white"
            anchors.rightMargin: 35
        }

        GridView{
            id: gridview
            x: 0
            flickableDirection: Flickable.HorizontalAndVerticalFlick
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.topMargin: 0
            anchors.leftMargin: 60
            anchors.fill: parent
            focus: true
            clip: true
            flickDeceleration: 750
            maximumFlickVelocity: 1500
            layoutDirection: Qt.LeftToRight
            flow: GridView.FlowLeftToRight
            boundsBehavior: Flickable.StopAtBounds
            snapMode: GridView.SnapToRow
            anchors.margins: 20
            delegate: delegate_item_view
            //model: groceryItem_listModel
            cellWidth: 220
            cellHeight: 220
        }

        ListModel {
            id: buttonItemSection_listModel
            dynamicRoles: true
        }

        Component{
            id: delegate_item_view
            SIMCardViewNew{
                operator: operator_name
                sim_type: product_type.toUpperCase()
                init_price: FUNC.insert_dot(publish_price.toString())
                new_price: FUNC.insert_dot(final_price.toString())
                product_desc: description
                phone_no: number

                MouseArea{
                    anchors.fill: parent
                    enabled: !keyboardVis
                    onClicked: {
                        if (press!='0') return
                        press = '1'
                        console.log(name + ' SIM Card is selected !')
                        var products = {"sku": product_id, "name": description, "number": number, "qty": '1', "total_amount": final_price.toString(), "qr_url": ''}
                        my_stack_view.push(customer_input_details, {products: JSON.stringify(products), usageOf:'ppob-starter-pack'})

                    }
                }
            }
        }

    }

    Text {
        id: total_product_text
        x: 881
        y: 733
        visible: false
        width: 100
        text: total_product.toString() + ' SIM'
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        color: 'white'
        font.bold: false
        font.family: 'Microsoft YaHei'
        font.pixelSize: 15
    }

//    Rectangle{
//        id: base_number_keyboard
//        x: 0
//        y: 200
//        visible: keyboardVis
//        color: 'white'
//        radius: 0
//        opacity: 0.7
//        width: 1024
//        height: 568
//    }

    NumKeyboard{
        id:number_keyboard
        visible: keyboardVis
        x:378
        y:374
        functionPad: false

        Component.onCompleted: {
            number_keyboard.letter_button_clicked.connect(show_validate_code);
            number_keyboard.function_button_clicked.connect(function_button_action);
        }

        NumButton{
            id: reset_button
            x: 0
            y: 270
            defaultColor: 'orange'
            img_source: ''
            show_text: qsTr('DEL')
            textColor: 'white'
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    count--
                    filter_text = filter_text.substring(0,filter_text.length-1);
//                    filter_text = '';
//                    loading_view.open();
//                    slot_handler.start_filter_sim_data(parent.show_text);
                }
            }
        }

        NumButton{
            id: find_button
            x: 180
            y: 270
            defaultColor: 'green'
            img_source: ''
            show_text: qsTr('FIND')
            textColor: 'white'
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return
                    press = '1'
                    count = 0;
                    loading_view.open();
                    keyboardVis = false;
                    slot_handler.start_filter_sim_data(simcard_service_page.filter_text);
                }
            }
        }

        function function_button_action(str){
//            if (str == "BACK" || str == "BATAL"){
//                my_timer.stop()
//                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 4) return true }))
//            }
//            if (str == "DELETE" || str == "HAPUS"){
//                show_validate_code("")
//            }
        }

        function show_validate_code(str){
//            if (str == "" && count > 0){
//                if(count>=15){
//                    count=15
//                }
//                count--
//                simcard_service_page.filter_text = simcard_service_page.filter_text.substring(0,count);
//            }
//            if(count==0){
//                simcard_service_page.filter_text = ""
//            }
            if (str != "" && count < 15){
                count++;
            }
            if (count > 15){
                str="";
            }
            filter_text += str;
            abc.counter = timer_value
            my_timer.restart()
        }
    }

    LoadingView{
        id: loading_view
    }
}
