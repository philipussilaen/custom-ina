import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width: 400
    height: 150
    radius: 25
    property alias main_text: main_text.text
    property alias desc_text: desc_text.text
    property var button_img: ""
    color: "white"

    Image{
        id: img_showing
        width:100
        height:100
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.verticalCenter: parent.verticalCenter
        source:button_img
    }

    Text {
        id: main_text
        width: 250
        font.family: "Microsoft YaHei"
        font.pixelSize: 20
        color: "white"
        font.bold: true
        text: "Main Button"
        anchors.verticalCenterOffset: -20
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Text {
        id: desc_text
        width: 250
        height: 50
        font.family: "Microsoft YaHei"
        font.pixelSize: 15
        color: "white"
        text: "Main Button Desc"
        anchors.verticalCenterOffset: 20
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10

    }


}
