import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:courier_select_box
    width: 1024
    height: 768
    property var boxSize_status:"FFFFF"
    property int timer_value: 60
    property var press: "0"
    property var store_type: "store"
    property var pref_login_user:""
    property variant pref_laundry : ["LOT", "PIC", "TYK", "KNK"]

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('prefix_user_account : ' + pref_login_user)
            console.log('store_type : ' + store_type)
            updateStatus()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            handle_button_user(pref_login_user)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.choose_mouth_result.connect(handle_text)
        root.mouth_status_result.connect(handle_mouth_status)
        root.free_mouth_result.connect(show_free_mouth_num)
        slot_handler.start_get_mouth_status()
        slot_handler.start_get_free_mouth_mun()
    }

    Component.onDestruction: {
        root.choose_mouth_result.disconnect(handle_text)
        root.mouth_status_result.disconnect(handle_mouth_status)
        root.free_mouth_result.disconnect(show_free_mouth_num)
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    if(store_type == "store"){
                        my_timer.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                    if(store_type == "change"){
                        my_timer.stop()
                        slot_handler.start_store_express()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")
        visible: (store_type=="store") ? true : false

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                select_service_back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                select_service_back_button.show_source = "img/button/7.png"
            }
        }
    }

    FullWidthReminderText{
        id:please_select_service
        y:150
        remind_text:qsTr("please select service option")
        remind_text_size:35
    }

        CourierSelectBoxSizeButton{
            id:mini_box
            x: 127
            y: 214
            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size1.png"

            MouseArea {
                id: mouseArea1
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_choose_mouth_size("MINI","staff_store_express")
                }
                onEntered:{
                mini_box.show_image = "img/courier14/size6.png"
                }
                onExited:{
                mini_box.show_source = "img/courier14/14ground.png"
                mini_box.show_image = "img/courier14/size1.png"
                }
            }
            Text {
                    id: mini_num
                    x: 126
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
            }
        }

        CourierSelectBoxSizeButton{
            id:small_box
            x: 387
            y: 214
            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size2.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_choose_mouth_size("S","staff_store_express")
                }
                onEntered:{
                    small_box.show_image = "img/courier14/size7.png"
                }
                onExited:{
                    small_box.show_source = "img/courier14/14ground.png"
                    small_box.show_image = "img/courier14/size2.png"
                }

                Text {
                    id: small_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:middle_box
            x: 647
            y: 214

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size3.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_choose_mouth_size("M","staff_store_express")
                }
                onEntered:{
                    middle_box.show_image = "img/courier14/size8.png"
                }
                onExited:{
                    middle_box.show_source = "img/courier14/14ground.png"
                    middle_box.show_image = "img/courier14/size3.png"
                }

                Text {
                    id: mid_num
                    x: 126
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:big_box
            x: 127
            y: 424

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size4.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_choose_mouth_size("L","staff_store_express")
                }
                onEntered:{
                    big_box.show_image = "img/courier14/size9.png"
                }
                onExited:{
                    big_box.show_source = "img/courier14/14ground.png"
                    big_box.show_image = "img/courier14/size4.png"
                }

                Text {
                    id: big_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:extra_big_box
            x: 387
            y: 424

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size5.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_choose_mouth_size("XL","staff_store_express")
                }
                onEntered:{
                    extra_big_box.show_image = "img/courier14/size10.png"
                }
                onExited:{
                    extra_big_box.show_source = "img/courier14/14ground.png"
                    extra_big_box.show_image = "img/courier14/size5.png"
                }

                Text {
                    id: extra_big_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

/* -> Disabling Back Button
    OverTimeButton{
        id:select_box_back_button
        x:670
        y:425
        show_text:qsTr("back to menu")
        show_x:15
        show_image:"img/05/back.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
            }
            onEntered:{
                select_box_back_button.show_source = "img/05/pushdown.png"
            }
            onExited:{
                select_box_back_button.show_source = "img/05/button.png"
            }
        }
    }
*/
    function updateStatus(){
        if( boxSize_status.substring(4,5) == "T"){
            mini_box.show_source = "img/courier14/14ground.png"
            mini_box.show_image = "img/courier14/size1.png"
            mini_box.enabled = true
        }
        else{
            mini_box.show_source = "img/courier14/gray1.png"
            mini_box.enabled = false
        }
        if( boxSize_status.substring(3,4) == "T"){
            small_box.show_source = "img/courier14/14ground.png"
            small_box.show_image = "img/courier14/size2.png"
            small_box.enabled = true
        }
        else{
            small_box.show_source = "img/courier14/gray1.png"
            small_box.enabled = false
        }
        if( boxSize_status.substring(2,3) == "T"){
            middle_box.show_source = "img/courier14/14ground.png"
            middle_box.show_image = "img/courier14/size3.png"
            middle_box.enabled = true
        }
        else{
            middle_box.show_source = "img/courier14/gray1.png"
            middle_box.enabled = false
        }

        if( boxSize_status.substring(1,2) == "T"){
            big_box.show_source = "img/courier14/14ground.png"
            big_box.show_image = "img/courier14/size4.png"
            big_box.enabled = true
        }
        else{
            big_box.show_source = "img/courier14/gray1.png"
            big_box.enabled = false
        }

        if( boxSize_status.substring(0,1) == "T"){
            extra_big_box.show_source = "img/courier14/14ground.png"
            extra_big_box.show_image = "img/courier14/size5.png"
            extra_big_box.enabled = true
        }

        else{
            extra_big_box.show_source = "img/courier14/gray1.png"
            extra_big_box.enabled = false
        }
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        for(var i in obj){
                    if(i == "XL"){
                        extra_big_num.text = obj[i]
                    }
                    if(i == "L"){
                        big_num.text = obj[i]
                    }
                    if(i == "M"){
                        mid_num.text = obj[i]
                    }
                    if(i == "S"){
                        small_num.text = obj[i]
                    }
                    if(i == "MINI"){
                        mini_num.text = obj[i]
                    }
            }
    }

    function handle_text(text){
        if(text=='Success'){
            if (store_type=="store"){
                my_stack_view.push(door_open_view,{store_type:store_type,containerqml:courier_select_box});
            } else if (store_type=="change"){
                my_stack_view.push(door_open_view,{store_type:store_type,containerqml:courier_select_box, change_door_status: false});
            }
        }
        if(text=='NotMouth'){
            select_service_back_button.enabled = false
            mini_box.enabled = false
            small_box.enabled = false
            middle_box.enabled = false
            big_box.enabled = false
            extra_big_box.enabled = false
            not_mouth.open()
        }
        if(text=='NotBalance'){
            select_service_back_button.enabled = false
            mini_box.enabled = false
            small_box.enabled = false
            middle_box.enabled = false
            big_box.enabled = false
            extra_big_box.enabled = false
            not_balance.open()
        }
    }

    function handle_mouth_status(text){
        boxSize_status = text
        updateStatus()
    }

    function handle_button_user(pref_login_user){
        if(pref_laundry.indexOf(pref_login_user) > -1){
            mini_box.enabled = false
            mini_box.visible = false
            small_box.enabled = false
            small_box.visible = false
            middle_box.x = 127
            middle_box.y = 280
            big_box.x = 387
            big_box.y = 280
            extra_big_box.x = 647
            extra_big_box.y = 280
        }else{
            return
        }
    }

    function clickedfunc(temp){
        store_type = temp
    }

    HideWindow{
        id:not_mouth
        Text {
            y:350
            width: 1024
            height: 60
            text: qsTr("not_mouth")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:not_mouth_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15


            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    select_service_back_button.enabled = true
                    slot_handler.start_get_mouth_status()
                    not_mouth.close()
                }
                onEntered:{
                    not_mouth_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    not_mouth_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:not_balance
        Text {
            y:350
            width: 1024
            height: 60
            text: qsTr("not_balance")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:not_balance_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    select_service_back_button.enabled = true
                    slot_handler.start_get_mouth_status()
                    not_balance.close()
                }
                onEntered:{
                    not_balance_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    not_balance_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
