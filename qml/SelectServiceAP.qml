import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    mainMode: true
    property var press: '0'
    property variant pic_source: []
    property variant qml_pic: []
    property var path: '/advertisement/apservice/'
    property var img_path: '..' + path
    property var doorSize: 'L'
    property bool availableUse: false
    property var locker_name: "---"
    property var minTapCount: 7
    property var count: 0


    property int num_pic: 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_file_dir(path);
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_idle_mode();
            slot_handler.start_get_locker_name()
            timer_clock.start();
            timer_startup.start();
            press = '0';
            availableUse = false;
            minTapCount = 7

        }
        if(Stack.status==Stack.Deactivating){
            timer_clock.stop();
            slider_timer.stop();
            timer_startup.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_file_dir_result.connect(init_images);
        root.free_mouth_result.connect(show_free_mouth_num);
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.start_get_file_dir_result.disconnect(init_images);
        root.free_mouth_result.disconnect(show_free_mouth_num);
        root.start_get_locker_name_result.disconnect(show_locker_name)
    }

    function show_free_mouth_num(doors){
        var obj = JSON.parse(doors);
        for(var i in obj){
            if(i == doorSize){
                // print('available compartment', doorSize, ':', obj[i]);
                if (obj[i] > 0) {
                    availableUse = true;
                    availableMouth.text = obj[i];
                     //print('ready to use :', availableUse);
                }
            }
        }
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name_text.text = (exampleMode==true) ? "SIMULATION" :text
    }

    function init_images(result){
//        console.log(result);
        if (result=='') return;
        var i = JSON.parse(result);
        qml_pic = i.output;
        slider.sourceComponent = slider.Null;
        pic_source = img_path + qml_pic[0];
        slider.sourceComponent = component;
        slider_timer.start();
        loading_image.visible = false;
    }


    Timer{
        id:slider_timer
        interval:10000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1;
                if(num_pic == qml_pic.length){
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[0];
                    slider.sourceComponent = component;
                    num_pic = 0;
                }else{
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[num_pic];
                    slider.sourceComponent = component;
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                source: pic_source
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
            }
        }
    }

    Loader {
        id: slider
        x: 0
        y: 90
        width: parent.width
        height: 400
    }

    AnimatedImage{
        id: loading_image
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'img/apservice/loading.gif'
        fillMode: Image.PreserveAspectFit
    }

    MainButtonAP{
        id: titip
        x: 24
        y: 510
        width: 480
        height: 200
        Image {
            id: titip_logo
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "img/courier08/btn_titip.png"
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (availableUse){
                    if (press!='0') return;
                    press = '1';
                    //console.log('TITIP Button is Pressed');
                    my_stack_view.push(input_phone_ap, {usageOf: 'popsafe-order'});
                    slot_handler.stop_idle_mode();
                } else {
                    main_rec_dimm.visible = true;
                    ambil.enabled = false;
                }
            }
        }
    }

    MainButtonAP{
        id: ambil
        x: 521
        y: 510
        width: 480
        height: 200
        Image {
            id: ambil_logo
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "img/courier08/btn_ambil.png"
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return
                press = '1';
                //console.log('AMBIL Button is Pressed');
                my_stack_view.push(input_pin_ap);
                slot_handler.stop_idle_mode();
            }
        }
    }

    Rectangle{
        id: status_avail
        width: 300
        height: 35
        color: "transparent"
        anchors.left: parent.left
        anchors.leftMargin: 375
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10

        Row {
            id: avail_doors_view
            x: 0
            y: 0   
            spacing: 5

            Text {
                id: mouth_title
                text: qsTr("KETERSEDIAAN LOKER: ")
                font.family:"Microsoft YaHei"
                font.bold: true
                color:"#ef494d"
                font.pixelSize: 20
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            Row {
                spacing: 5
                Text {
                    id: availableMouth
                    text: qsTr("0")
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    color:"#ef494d"
                    font.pixelSize: 20
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter
                }
            }
          
        }
    }

    Rectangle{
        id: courir_rec
        y: 100
        width: 62
        height: 62
        color: "transparent"
        anchors.right: parent.right
        anchors.rightMargin: 20
        Image {
            id: admin_logo
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "img/apservice/icon/icon-accessadmin.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(background_login_view,{"identity":"LOGISTICS_COMPANY_USER"})
                // slot_handler.start_post_activity("StoreParcel")
            }
        }
    }

    Rectangle{
        id: logo_bmleft
        x: 24
        y: 716
        width: 100
        height: 40
        color: "#00000000"
        Image {
            id: logo_pop
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "img/courier08/popbox_red.png"
        }
    }

    Rectangle{
        id: logo_bmright
        x: 760
        y: 716
        width: 240
        height: 40
        color: "#00000000"
        Image {
            id: logo_aps
            visible: (exampleMode==true) ? false : true 
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "img/courier08/aps.png"
        }
    }

    Text {
        id: timeText
        width: 150
        height: 35
        text: new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        font.bold: true
        anchors.top: parent.top
        anchors.topMargin: 12
        anchors.right: parent.right
        anchors.rightMargin: 20
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize: 35
        color:"#ffffff"
    }

    Text {
        id: dateText
        height: 25
        text: new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat)
        anchors.top: parent.top
        anchors.topMargin: 58
        anchors.right: parent.right
        anchors.rightMargin: 20
        font.italic: false
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize: 20
        color:"#ffffff"
    }

    Text {
        id: locker_name_text
        height: 25
        text: (exampleMode==true) ? "SIMULATION" : "DPS"
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 440
        font.italic: false
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.capitalization: Font.AllUppercase
        font.pixelSize: 30
        color:"#ffffff"
        MouseArea {
            x: 0
            y: 0
            width: 50
            height: 25
            onClicked: {
                count ++;
                if(count == minTapCount){
                    // my_timer.stop()
                    console.log("clicked before 7: " + count)
                    count = 0
                    my_stack_view.push(background_login_view,{"identity":"OPERATOR_USER"})
                }
            }
        }
    }


    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:
        {
            timeText.text = new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss");
            press = '0';
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:
        {
            slot_handler.start_get_file_dir(path);
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_idle_mode();
            slot_handler.start_get_locker_name()
            press = '0';
        }
    }

    Rectangle{
        id:main_rec_dimm
        visible:false
        width: 1024
        height: 768
        color: "#AA4A4A4A"

        Rectangle{
            id:sub_rec_dimm
            width: 650
            height: 500
            color: "white"
            radius: 22
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Text{
                id:subject_text
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: 0
                text: qsTr("Loker Penuh")
                anchors.top: parent.top
                anchors.topMargin: 10
                font.bold: true
                color:"#009BE1"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize:43
            }

            Text{
                id:body_text
                x: 93
                width: 850
                height: 50
                text: qsTr("Maaf tidak ada loker tersedia saat ini,\nSilakan coba beberapa saat lagi")
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 160
                font.family:"Microsoft YaHei"
                color:"#4A4A4A"
                textFormat: Text.PlainText
                font.pixelSize: 26
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            Image {
                id: img_notif
                x: 410
                width: 215
                height: 215
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 220
                fillMode: Image.PreserveAspectFit
                source: "img/apservice/img/failed.png"
            }

            NotifButtonAP{
                id: button_notif
                anchors.top: parent.top
                anchors.topMargin: 400
                anchors.horizontalCenter: parent.horizontalCenter
                buttonColor: 'BLUE'
                modeReverse: false
                buttonText: qsTr('OK')

                MouseArea {
                    //visible: !isExtending
                    anchors.fill: parent
                    onClicked: {
                        main_rec_dimm.visible=false;
                        ambil.enabled = true;
                    }
                }
            }
        }
    }

}
