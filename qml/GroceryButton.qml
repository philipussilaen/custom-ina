import QtQuick 2.4
import QtQuick.Controls 1.2


Rectangle{
    width:279
    height:64
    color:"transparent"
    property var show_text:""
    property var show_source:"img/05/red_1.png"

    Image{
        width:279
        height:64
        source:show_source
    }

    Text{
        text:show_text
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color:"white"
        font.family:"Microsoft YaHei"
        font.pixelSize:30
    }

}
