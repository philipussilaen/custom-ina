import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: main_rectangle
    width:225
    height:225
    color:"white"
    radius: 20
    property var show_text:"eMoney"
    property var show_image:"img/apservice/logo/emoney.png"
    property var paymentBase: 'qr'
    property bool buttonActive: true
    property var colorTheme: 'BLUE' //'BLUE'/'GREEN'/
    border.color: get_color(buttonActive, colorTheme)
    border.width: 3

    Image{
        scale: 0.8
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
        source:show_image
    }

    Image {
        id: badge
        width: 40
        height: 40
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        source: get_badge(paymentBase, buttonActive)
    }

    Text{
        text:show_text
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family:"Microsoft YaHei"
        color:get_color(buttonActive, colorTheme)
        font.pixelSize:20
    }

    function get_color(a, t){
        if (a==true){
            if (t=='BLUE'){
                return "#009BE1";
            } else {
                return "#65B82E";
            }
        } else {
            return 'gray';
        }
    }

    function get_badge(p, a){
        if (p=='card'){
            if (a==true){
                return "img/apservice/icon/kartu_biru.png";
            } else {
                return "img/apservice/icon/kartu_grey.png";
            }
        } else {
            if (a==true){
                return "img/apservice/icon/qr_biru.png";
            } else {
                return "img/apservice/icon/qr_grey.png";
            }
        }
    }

}
