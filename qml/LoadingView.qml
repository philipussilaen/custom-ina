import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:floating_window
    visible: false

    Rectangle{
        id: rectangle1
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#4A4A4A4A"
        Rectangle{
            id:sub_rec_dimm
            width: 650
            height: 500
            color: "white"
            radius: 22
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            Text{
                id:subject_text
                //x: 200
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: 0
                //width: 300
                //text: prod_trans_id
                text: qsTr("Tunggu ya :)")
                anchors.top: parent.top
                anchors.topMargin: 30
                font.bold: true
                color:"#009BE1"
                //verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.pixelSize:43
            }

            Text{
                id:body_text
                x: 93
                width: 850
                height: 50
                text: qsTr("Sedang mengecek metode\npembayaran yang tersedia")
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 80
                font.family:"Microsoft YaHei"
                color:"#4A4A4A"
                textFormat: Text.PlainText
                font.pixelSize: 26
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

        }
    }

    AnimatedImage  {
        id: loading_image
        x: 285
        y: 351
        height: 215
        width: 303
        anchors.verticalCenter: rectangle1.verticalCenter
        anchors.horizontalCenter: rectangle1.horizontalCenter
        source: "img/loading/loading_pembayaran.gif"
    }
    // Text {
    //     id: loading_text
    //     x: 451
    //     y: 500
    //     color: "#ffffff"
    //     text: qsTr("Preparing...")
    //     font.family:"Microsoft YaHei"
    //     anchors.horizontalCenter: rectangle1.horizontalCenter
    //     font.pixelSize: 20
    // }

    function open(){
        floating_window.visible = true
    }
    function close(){
        floating_window.visible = false
    }
}
