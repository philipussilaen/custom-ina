import QtQuick 2.4
import QtQuick.Controls 1.2


Rectangle{
    id: rectangle1

    width:279
    height:64
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_x
    property var show_source:"img/button/7.png"

    Image{
        width:279
        height:64
        source:show_source
    }

    Text{
        text:show_text
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color:"red"
        font.family:"Microsoft YaHei"
        font.pixelSize:30
    }

}
