import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:245
    height:235
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_text_color:"#ffffff"


    Image{
        x:0
        y:0
        width:245
        height:235
        source:show_image
    }


    Rectangle{
        x:0
        y:0
        width:245
        height:235
        color:"transparent"


        Text{
            text:show_text
            anchors.verticalCenterOffset: 91
            anchors.horizontalCenterOffset: 0
            font.family:"Microsoft YaHei"
            color:show_text_color
            font.pixelSize:25
            anchors.centerIn: parent;
        }
    }


}
