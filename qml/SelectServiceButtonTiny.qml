import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:187
    height:91
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_source:""


    Image{
        x:0
        y:0
        width:187
        height:91
        source:show_source
    }


    Image{
        x:0
        y:0
        width:187
        height:91
        source:show_image
    }
}
