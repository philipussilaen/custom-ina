import QtQuick 2.4
import QtQuick.Controls 1.3

Rectangle{
    id: rectangle
    property alias labelText: label.text
    property alias contentText: content.text
    property var colorContent: 'gray'
    property var colorLabel: '#323232'
    property int fontSize: 30
    property bool contentBold: false
    property int separatorPoint: 125
    property int textMargin: 150
    width: 500
    height: 50
    color: 'white'

    GroupBox{
        id: groupBox
        anchors.fill: parent
        flat: true

        Text {
            id: label
            text: qsTr("No HP")
            font.family: 'Microsoft YaHei'
            font.bold: true
            font.pixelSize: fontSize
            color: colorLabel
        }

        Text {
            id: separator
            text: ":"
            anchors.left: parent.left
            anchors.leftMargin: separatorPoint
            font.pixelSize: fontSize
        }

        Text{
            id: content
            anchors.left: parent.left
            anchors.leftMargin: textMargin
            // text: "085710157057"
            text: ""
            font.family: 'Microsoft YaHei'
            font.pixelSize: fontSize
            color: colorContent
            font.bold: contentBold
        }
    }

}
