import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:reject_sure_phone_number
    width: 1024
    height: 768
    property var show_text:""
    property int timer_value: 60
    property var press: "0"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        x: 0
        y: 0
        width: 1024
        height: 768
        color: "#472f2f"
        opacity: 0.4
    }

    Rectangle{
        x: 130
        y: 220
        width: 790
        height: 440

        Image {
            id: image1
            x: -5
            y: -2
            width: 800
            height: 450
            source: "img/courier13/layerground1.png"

            Text {
                id: text1
                x: 0
                y: 40
                width: 800
                height: 60
                color: "#FFFFFF"
                font.family:"Microsoft YaHei"
                text: qsTr("Please Verify Phone Number")
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 30
                textFormat: Text.AutoText
            }

            Text {
                x: 8
                y: 156
                width: 800
                height: 60
                color: "#FFFFFF"
                font.family:"Microsoft YaHei"
                text: show_text
                horizontalAlignment: Text.AlignHCenter
                font.pointSize:35
            }

        }

        OverTimeButton{
            id:sure_back_button
            x:110
            y:300
            show_text:qsTr("back")
            show_x:15
            show_image:"img/05/back.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop()
                }
                onEntered:{
                    sure_back_button.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    sure_back_button.show_source = "img/05/button.png"
                }
            }
        }

        OverTimeButton{
            id:sure_ok_button
            x:402
            y:300
            show_text:qsTr("continue")
            show_x:205
            show_image:"img/05/ok.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.set_phone_number(show_text)
                    my_stack_view.push(reject_express_info)
                }
                onEntered:{
                    sure_ok_button.show_source = "img/05/pushdown.png"
                }
                onExited:{
                    sure_ok_button.show_source = "img/05/button.png"
                }
            }
        }
    }
}
