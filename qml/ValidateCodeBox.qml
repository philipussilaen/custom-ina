import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    property var show_text:""

    width:54
    height:54
    color:"transparent"    

    Image{
        width:54
        height:54
        source:"img/item/2.png"
    }

    Text{
        text:show_text
        color:"#BF2D26"
        font.family:"Microsoft YaHei"
        font.pixelSize:40
        anchors.centerIn: parent;
    }

}
