import QtQuick 2.4
import QtQuick.Controls 1.2
import "button_payment.js" as BUTTON

Background{
    id:ppob_service_page_section
    property int timer_value: 60
    property var press: "0"
    property var _parent_id_: '0'
    property var _tab_: ''
    property var emoney_reader: undefined
    property var data_buttons: BUTTON.button_list

    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value;
            my_timer.restart();
            press = "0";
            parse_button(_parent_id_);
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    Component.onCompleted: {
        root.get_sim_card_result.connect(next_process);
    }

    Component.onDestruction: {
        root.get_sim_card_result.disconnect(next_process);
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

//    function cod_result(result){
//        console.log("cod_result is ", result)
//        if(result==""){
//            emoney_reader = "disabled"
//        }else{
//            emoney_reader = result
//        }
//    }

    function next_process(result){
        console.log(result)
        if (result==undefined || result=='ERROR'){
            error_notif_text.text = qsTr("Apologize currently this machine is not connected to the system, Please retry later")
            error_notif.open()
        } else if(result=='NO_DATA') {
            error_notif_text.text = qsTr("Apologize currently there is no product available this time, Please retry later")
            error_notif.open()
        } else {
            my_stack_view.push(simcard_ppob_view, {sim_data: result})
        }
    }

    function get_image(id, name){
        switch (id){
        case 9:
            return "img/otherservice/cop_white.png";
        case 16:
            return "img/button/popsend_white.png";
        default:
            return "img/ppob/"+ name +".png";
        }
    }

    function parse_button(parent_id_){
        buttonItemSection_listModel.clear()
        gridview.model = undefined;
        var buttons = data_buttons;
//        console.log('buttons', JSON.stringify(buttons));
        for (var i in buttons){
            var button_name = buttons[i].name;
            var button_id = buttons[i].id;
            var button_parent = buttons[i].parent;
            var button_tab = buttons[i].tab;
            if (buttons[i].status==1 && buttons[i].parent==parent_id_){
                buttonItemSection_listModel.append({'_name_' : button_name,
                                             '_id_' : button_id,
                                             '_parent_id_': button_parent,
                                             '_tab_': button_tab
                                            });
            }
        }
        gridview.model = buttonItemSection_listModel;
        console.log('Total Buttons : ' + buttonItemSection_listModel.count + ' in ' + parent_id_);
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Text {
        id: title_class
        x: 0
        y: 125
        anchors.horizontalCenter: parent.horizontalCenter
        text: _tab_
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color: 'white'
        font.bold: true
//        font.italic: true
        font.pixelSize: 25
        font.family:"Microsoft YaHei"
    }

    Item{
        id: button_item_view
        x: 8
        y: 201
        width: 1008
        height: 559
        focus: true

        GridView{
            id: gridview
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.topMargin: 0
            anchors.leftMargin: 31
            anchors.fill: parent
            focus: true
            clip: true
            flickDeceleration: 750
            maximumFlickVelocity: 1500
            layoutDirection: Qt.LeftToRight
            flow: GridView.FlowLeftToRight
            boundsBehavior: Flickable.StopAtBounds
            snapMode: GridView.SnapToRow
            anchors.margins: 20
            delegate: delegate_item_view
            //model: groceryItem_listModel
            cellWidth: 225
            cellHeight: 215
        }

        ListModel {
            id: buttonItemSection_listModel
            dynamicRoles: true
        }

        Component{
            id: delegate_item_view
            PPOBServiceButton {
                width: 245
                height: 235
                scale: 0.8
                show_text_color: "#ab312e"
                show_text: _name_
                show_image: get_image(_id_, _name_)
                sim_operator: get_sim_logo(_id_)

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press != "0"){
                            return
                        }
                        press = "1"
                        my_timer.stop()
                        define_process(_id_)
                    }
                }
            }
        }
    }

    function get_sim_logo(id){
        if (id == 6 || id == 7) return 'telkomsel'
        return
    }

    function define_process(id){
        switch(id){
        case 2: case 9:
            my_stack_view.push(other_input_memory_page);
            break;
        case 6:
            slot_handler.start_get_sim_product('telkomsel', 'data');
            break;
        case 7:
            slot_handler.start_get_sim_product('telkomsel', 'regular');
            break;
        case 15:
            my_stack_view.push(sepulsa_input_phone_view);
            break;
        case 16:
            my_stack_view.push(courier_input_phone_view, {usageOf:"popsend_topup"});
            break;
        case 27:
            if (emoney_reader == 'enabled'){
                my_stack_view.push(emoney_check_balance);
            } else {
                my_stack_view.push(on_develop_view);
            }
            break;
        default:
            my_stack_view.push(on_develop_view);
            break;
        }
    }


    HideWindow{
        id:error_notif
//        visible: true

        Image {
            id: img_network_error
            x: 409
            y: 219
            width: 228
            height: 230
            source: "img/otherImages/error_notif.png"
        }

        Text {
            id: error_notif_text
            x: 107
            y:464
            width: 827
            height: 118
//            text: qsTr("Apologize currently the machine is not connected to the system, Please retry later")
            font.italic: false
            font.pixelSize: 30
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            x:384
            y:609
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


}
