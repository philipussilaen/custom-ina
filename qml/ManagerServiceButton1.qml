import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:250
    height:180
    color:"transparent"
    property var show_text:"TEST"
    property var show_source: "img/bottondown/1.png"
    property var show_icon:"img/courier08/08pass1.png"


    Image{
        anchors.fill: parent
        source:show_source
    }

    Image{
        x: 80
        y: 9
        width: 90
        height: 90
        scale: 0.8
        fillMode: Image.PreserveAspectFit
        source: show_icon
    }

    Text {
        x: 0
        y: 90
        width: 250
        height: 75
        color:"red"
        text:show_text
        textFormat: Text.PlainText
        font.family: "Courier"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
    }
}
