import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:other_service_emoney
    property int timer_value: 30
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:other_service_emoney_title
        x: 0
        y:135
        border.width: 0
        remind_text:qsTr("How To Do Payment")
        remind_text_size:"45"
    }

    Image {
        id: step1
        x: 0
        y: 173
        width: 350
        height: 350
        source: "img/otherservice/cara-emoney.png"
    }

    Image {
        id: step2
        x: 337
        y: 173
        width: 350
        height: 350
        source: "img/otherservice/cara-emoney1.png"
    }

    Image {
        id: step3
        x: 668
        y: 173
        width: 350
        height: 350
        source: "img/otherservice/cara-emoney2.png"
    }


    Text {
        id: textstep1
        x: 37
        y: 523
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Scan or enter your order number.")
        textFormat: Text.AutoText
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        font.pixelSize: 24
        font.family:"Microsoft YaHei"
    }

    Text {
        id: textstep2
        x: 374
        y: 519
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Put your pre-paid card into reader and confirm payment.")
        horizontalAlignment: Text.AlignHCenter
        font.family:"Microsoft YaHei"
        font.pixelSize: 24
        wrapMode: Text.WordWrap
    }

    Text {
        id: textstep3
        x: 705
        y: 519
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Do not forget to take back your pre-paid card.")
        horizontalAlignment: Text.AlignHCenter
        font.family:"Microsoft YaHei"
        font.pixelSize: 24
        wrapMode: Text.WordWrap
    }

    Image{
        id:next_button
        x: 400
        y: 664
        width:225
        height:60
        source:"img/button/1.png"
        Text{
            text:qsTr("Go To Payment")
            font.family:"Microsoft YaHei"
            color:"red"
            font.pixelSize:28
            anchors.centerIn: parent;
        }
        MouseArea {
            y: 0
            width: 225
            height: 60
            enabled: true
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            onClicked: {                
                my_stack_view.push(other_input_memory_page)                
            }
            onEntered:{
                next_button.source = "img/button/DeleteButton.png"
            }
            onExited:{
                next_button.source = "img/button/1.png"
            }
        }
    }

    
    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

}
