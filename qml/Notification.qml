import QtQuick 2.4

Rectangle{
    id: main_rectangle
    color: "#828282"
    property alias contentNotif: notif_text.text
    property bool successMode: true
    property bool redImage: false
    width: 1024
    height: 768
    visible: false


    Rectangle{
        id: sub_rectangle
        color: "white"
        radius: 20
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        width: 500
        height: 400
        Image{
            id: close_button
            width: 50
            height: 50
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            source: 'img/apservice/close.png'
            MouseArea{
                anchors.fill: parent;
                onClicked: close();
            }
        }

        Text{
            id: notif_text
            x: 86
            y: 179
            width: 450
            height: 200
            font.family: 'Microfot YaHei'
            font.pixelSize: 30
            color: 'darkgrey'
            text: "Dear Customer......"
            anchors.verticalCenterOffset: 80
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }

        Image{
            id: notif_img
            visible: !redImage
            anchors.verticalCenterOffset: -80
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source: (successMode==true) ? "img/apservice/icon/sukses_hijau.png" : "img/apservice/icon/tidakvalid_hijau.png"
        }

        Image{
            id: notif_img_red
            visible: redImage
            anchors.verticalCenterOffset: -80
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source: (successMode==true) ? "img/payment/success_icon.png" : "img/payment/failed-icon.png"
        }

    }


    function open(){
        main_rectangle.visible = true;
    }

    function close(){
        main_rectangle.visible = false;
    }

}





