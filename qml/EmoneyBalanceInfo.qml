import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: emoney_balance_info

    property var balance
    property int timer_value: 30
    property var press:"0"
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            ok_button.enabled = true
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Text{
            id:show_time
            x:60
            y:50
            font.family:"Microsoft YaHei"
            color:"#f8f400"
            textFormat: Text.PlainText
            font.pointSize:33
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                show_time.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:title_text
        x: 0
        y:165
        remind_text:qsTr("Balance Information")
        remind_text_size:"45"
    }

    DoorButton{
        id:ok_button
        y:616
        x:373
        show_text:qsTr("OK")
        show_image:"img/bottondown/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                ok_button.show_image = "img/bottondown/down_1.png"
            }
            onExited:{
                ok_button.show_image = "img/bottondown/1.png"
            }
        }
    }

    Rectangle{
        x: 54
        y: 244
        width: 900
        height: 350
        color: "#ffffff"
        opacity: 0.85
        radius: 10
        border.width: 0

        Image{
            id: money
            x:-45
            y:-28
            width:390
            height:387
            scale: 0.8
            sourceSize.height: 387
            sourceSize.width: 390
            source: "img/otherservice/money_coin.png"
        }
        Text {
            id: title_balance
            x:207
            y:78
            width: 565
            height: 80
            text: qsTr("Your pre-paid balance is ")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:24
            wrapMode: Text.WordWrap
        }
        Text {
            id: balance_text
            x:207
            y:164
            width: 565
            height: 60
            text: balance
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:35
            wrapMode: Text.WordWrap
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {      
    }

}
