import hashlib
import json
from PyQt5.QtCore import QObject, pyqtSignal
import ClientTools
import box.service.BoxService
import express.repository.ExpressDao as ExpressDao
import base64
import Configurator
from network import HttpClient
import logging

__author__ = 'wahyudi@popbox.asia'


class UserSignalHandler(QObject):
    user_login_signal = pyqtSignal(str)
    user_info_signal = pyqtSignal(str)
    courier_get_user_signal = pyqtSignal(str)
    user_local_signal = pyqtSignal(str)


_USER_ = UserSignalHandler()
user_type = ''
user = ''
server = Configurator.get_or_set_value('ClientInfo', 'prox^prefix', 'pr0x')


def background_login_start(username, password, identity):
    global user_type
    user_type = identity
    ClientTools.get_global_pool().apply_async(login, (username, password))


def get_user():
    global user
    return user


def select_wallet_for_box():
    user['wallet'] = {'balance': 0}
    __box_result = box.service.BoxService.get_box()
    if __box_result is False:
        return
    if 'currencyUnit' not in __box_result.keys():
        return
    __box_currency_unit = __box_result['currencyUnit']
    __user_wallets = user['wallets']
    for __wallet in __user_wallets:
        if __wallet['currencyUnit'] != __box_currency_unit:
            continue
        user['wallet'] = __wallet


last_username = ''
last_password = ''


def login(username, password):
    global user, last_password, last_username
    pass_64 = base64.b64encode(bytes(password, 'utf-8'))
    pass_64 = str(pass_64)[2:-1] 
    param = {'username': username, 'userpassword': pass_64}
    count_list = ExpressDao.local_user_get(param)
    count_list = count_list[0]
    count_user = count_list['count']
    if(count_user == 1):
        __msg = {'name': count_list['displayName'],
                        'phoneNumber': count_list['phone'],
                        'loginName': count_list['userName'],
                        'company': {'name': count_list['company'],'id': count_list['id_company'],'level': int(count_list['userLevel'])},
                        'id' : count_list['id_user'],
                        'type' : count_list['usr_type'],
                        'role': count_list['role']}
        _USER_.user_login_signal.emit('Success')  
        user = __msg
    else:
        if server not in Configurator.get_value('ClientInfo', 'serveraddress'):
            password_and_salt_ = password + '_SALT_PAKPOBOX'
        else:
            password_and_salt_ = password + '_POPBOX_NEWLOCKER_SALT'
        md_ = hashlib.sha256(password_and_salt_.encode(encoding='utf-8'))
        if not md_:
            _USER_.user_login_signal.emit('Failure')
            return
        __user = {'loginName': username, 'password': str(md_.hexdigest())}
        logging.info("Param User Login : "+ str(__user))
        __message, status_code = HttpClient.post_message('user/clientLogin', __user)
        if status_code == -1:
            _USER_.user_login_signal.emit('Failure')
            return
        # if status_code == -1:
            
        #         _USER_.user_login_signal.emit('Failure')
        #         return
        if status_code != 200:
            # _USER_.user_login_signal.emit('Failure')
            user = __msg
            return
        if ClientTools.get_value('statusCode', __message) == 401:
            _USER_.user_login_signal.emit('Failure')
            return
        user = __message
        last_username = username
        last_password = password
        # print("login_result : " + str(user))
        # select_wallet_for_box()
        HttpClient.set_user_token(__message['token'])
        if user_type == 'LOGISTICS_COMPANY_USER':
            if user_type == __message['role'] or 'LOGISTICS_COMPANY_ADMIN' == __message['role']:
                _USER_.user_login_signal.emit('Success')
            else:
                _USER_.user_login_signal.emit('NoPermission')
        if user_type == 'OPERATOR_USER':
            if user_type == __message['role'] or 'OPERATOR_ADMIN' == __message['role']:
                _USER_.user_login_signal.emit('Success')
            else:
                _USER_.user_login_signal.emit('NoPermission')


def start_retry_login():
    ClientTools.get_global_pool().apply_async(retry_login)


def retry_login():
    if last_username != '' and last_password != '':
        login(last_username, last_password)
    else:
        return


def get_user_info():
    info = {'name': user['name'], 'phoneNumber': user['phoneNumber'], 'namelog': user['loginName'], 'company_name': user['company']['name']}
    _USER_.user_info_signal.emit(json.dumps(info))


def courier_get_user():
    _USER_.courier_get_user_signal.emit(json.dumps(user))


def start_get_card_info(card_id, identity):
    ClientTools.get_global_pool().apply_async(login_by_card, (card_id, identity))


def login_by_card(card_id, identity):
    global user
    global user_type
    user_type = identity
    __user = {'value': card_id}
    __message, status_code = HttpClient.post_message('user/cardLogin', __user)
    if status_code == -1:
        _USER_.user_login_signal.emit('NetworkError')
        return
    if status_code != 200:
        _USER_.user_login_signal.emit('Failure')
        return
    user = __message
    # select_wallet_for_box()
    HttpClient.set_user_token(__message['token'])
    if user_type == 'LOGISTICS_COMPANY_USER':
        if user_type == __message['role'] or 'LOGISTICS_COMPANY_ADMIN' == __message['role']:
            _USER_.user_login_signal.emit('Success')
        else:
            _USER_.user_login_signal.emit('NoPermission')
    if user_type == 'OPERATOR_USER':
        if user_type == __message['role'] or 'OPERATOR_ADMIN' == __message['role']:
            _USER_.user_login_signal.emit('Success')
        else:
            _USER_.user_login_signal.emit('NoPermission')
